package model;

import java.awt.Point;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import model.Units.UFO;
import model.Units.Unit;
import model.Units.UnitCollection;
import model.Units.UnitTaskCollection;
import model.Units.Tasks.UnitTask;
import model.constructs.Bed;
import model.constructs.BedCollection;
import model.resource.Food;
import model.resource.Metal;
import model.resource.Wood;
import view.GameGUI;
import view.GameView;
import view.lighting.LightEmitter;
import view.lighting.LightingEngine;

public class SaveGameSystem {

	/*
	 *
	 * Save and Load Game
	 */
	public static void saveGame(GameView view) {
		// Write to disk with FileOutputStream
		FileOutputStream f_out = null;
		try {
			f_out = new FileOutputStream("save.dat");
		} catch (FileNotFoundException e3) {
			e3.printStackTrace();
		}

		// Write object with ObjectOutputStream
		ObjectOutputStream obj_out = null;
		if (f_out == null)
			return;
		try {
			obj_out = new ObjectOutputStream(f_out);
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		if (obj_out == null)
			return;
		// Write object out to disk
		try {
			obj_out.writeObject(GameMap.getMapTilesForSaving());
			obj_out.writeObject(view.unitsToDraw);
			obj_out.writeObject(view.camera);
			obj_out.writeObject(WoodCollection.getInstance().getWoodForSaving());
			obj_out.writeObject(FoodCollection.getInstance().getFoodForSaving());
			obj_out.writeObject(MetalCollection.getInstance().getMetalForSaving());

			obj_out.writeObject(UnitTaskCollection.getInstance().getGatheringTasksForSaving());
			obj_out.writeObject(UnitTaskCollection.getInstance().getBuildingTasksForSaving());
			obj_out.writeObject(UnitTaskCollection.getInstance().getOtherTasksForSaving());

			obj_out.writeObject(UnitTaskCollection.getInstance().getGathering());
			obj_out.writeObject(UnitTaskCollection.getInstance().getBuilding());

			obj_out.writeObject(UnitTaskCollection.getInstance().getCurrentUnitsGathering());
			obj_out.writeObject(UnitTaskCollection.getInstance().getCurrentUnitsBuilding());

			obj_out.writeObject(BedCollection.getInstance().saveBeds());
			obj_out.writeObject(view.enemies);
			obj_out.writeObject(new Float(view.sunPhase));
			obj_out.writeObject(LightingEngine.lightEmitters);
			obj_out.writeObject(GameMap.getInstance().getSafeTilesForSaving());

		} catch (IOException e1) {
			e1.printStackTrace();
		}
		GameGUI.debugPanel.post("Game Saved");
	}

	@SuppressWarnings("unchecked")
	public static boolean loadGame(GameView view) {
		GameView.gameOver = false;
		UnitCollection.getInstance().clear();
		// Read from disk using FileInputStream
		FileInputStream f_in = null;
		try {
			f_in = new FileInputStream("save.dat");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		if (f_in == null)
			return false;

		// Read object using ObjectInputStream
		ObjectInputStream obj_in = null;
		try {
			obj_in = new ObjectInputStream(f_in);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (obj_in == null)
			return false;

		// Read an object
		Object obj = null;
		try {
			// Load map tiles
			obj = obj_in.readObject();
			GameMap.getInstance().loadMapTiles((TerrainTile[][]) obj);
			// Load units
			obj = obj_in.readObject();
			view.unitsToDraw = (LinkedList<Unit>) obj;
			for (Unit unit : view.unitsToDraw) {
				unit.load();
				UnitCollection.getInstance().add(unit);
				GameGUI.log("unit loaded: " + unit.getName());
			}
			// Load camera position
			obj = obj_in.readObject();
			view.camera = (Point) obj;
			// load wood collection
			obj = obj_in.readObject();
			WoodCollection.getInstance().loadWood((LinkedList<Wood>) obj);
			// load food collection
			obj = obj_in.readObject();
			FoodCollection.getInstance().loadFood((LinkedList<Food>) obj);
			// load metal collection
			obj = obj_in.readObject();
			MetalCollection.getInstance().loadMetal((LinkedList<Metal>) obj);

			// load task lists
			obj = obj_in.readObject();
			UnitTaskCollection.getInstance().loadGathering((Queue<UnitTask>) obj);
			obj = obj_in.readObject();
			UnitTaskCollection.getInstance().loadBuilding((Queue<UnitTask>) obj);
			obj = obj_in.readObject();
			UnitTaskCollection.getInstance().loadOther((Queue<UnitTask>) obj);

			// load gathering/building unit ints
			obj = obj_in.readObject();
			UnitTaskCollection.getInstance().setUnitsToGather((int) obj);
			obj = obj_in.readObject();
			UnitTaskCollection.getInstance().setUnitsToBuild((int) obj);
			obj = obj_in.readObject();
			UnitTaskCollection.getInstance().setCurrentUnitsGathering((int) obj);
			obj = obj_in.readObject();
			UnitTaskCollection.getInstance().setCurrentUnitsBuilding((int) obj);

			// load bed info
			obj = obj_in.readObject();
			BedCollection.getInstance().loadBeds((LinkedList<Bed>) obj);

			// Load ufos
			obj = obj_in.readObject();
			view.enemies = (LinkedList<UFO>) obj;

			obj = obj_in.readObject();
			view.sunPhase = (Float) obj;

			obj = obj_in.readObject();
			LightingEngine.lightEmitters = (List<LightEmitter>) obj;

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		GameGUI.debugPanel.post("Game Loaded");

		GameGUI.miniMap.updateMiniMapTexture();

		return true;
	}
}
