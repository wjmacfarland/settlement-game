package model.resource;

import java.awt.Point;
import java.util.Random;

public class Apple extends Food {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean hasAppleSeed;
	private AppleSeed[] seeds = new AppleSeed[3]; // Apple's container of
													// appleseeds
	private AppleSeed appleseed = new AppleSeed(gfx); // AppleSeed object called
														// appleseed

	Random randomGenerator = new Random();

	public Apple() {
		// Set the graphic
		super(new Point(3, 63));

		this.foodAmount = 15; // or how filling the apple will be to units
								// hunger
		int temp = randomGenerator.nextInt(10); // each apple has a 30% chance
												// of having seeds.
		if (temp > 6) {
			for (int x = 0; x < temp - 6; x++) { // apples will only have either
													// 1, 2, or 3 appleseeds.
				this.seeds[x] = appleseed;
			}
		}
	}

	@Override
	public String toString() {
		String toReturn = "" + foodAmount;
		return toReturn;
	}
}
