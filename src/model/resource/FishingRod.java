package model.resource;

import java.awt.Point;

public class FishingRod extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4297313823116762077L;
	
	static final Point GFX_ROD = new Point(13, 11);
	private int durability; // amount of times axe/item can be used

	public FishingRod() {
		super(GFX_ROD);
		this.durability = 100;
	}

	public void use() {
		if (this.usable()) {
			this.durability -= 1; // everytime a unit of wood is
									// gathered/harvested, decrease duribility
		}
	}

	public boolean usable() // if the item in the unit inventory isnt broken
	{
		if (this.durability > 0) {
			return true;
		}
		return false;
	}

	public boolean lowDurability() // if condition is low, so unit can make new
									// axe to remake breaking one
	{
		if (durability <= 25) {
			return true;
		}
		return false;
	}
}
