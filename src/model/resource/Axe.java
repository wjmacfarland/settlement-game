package model.resource;

import java.awt.Point;

public class Axe extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1325487096610303663L;
	
	static final Point gfx_AXE = new Point(2, 100);
	private int durability; // amount of times axe/item can be used

	public Axe() {
		super(gfx_AXE);
		this.durability = 100;
	}

	public void use() {
		if (this.usable()) {
			this.durability -= 1; // everytime a unit of wood is
									// gathered/harvested, decrease duribility
		}
	}

	public boolean usable() // if the item in the unit inventory isnt broken
	{
		if (this.durability > 0) {
			return true;
		}
		return false;
	}

	public boolean lowDurability() // if condition is low, so unit can make new
									// axe to remake breaking one
	{
		if (durability <= 25) {
			return true;
		}
		return false;
	}
}
