package model.resource;

import java.awt.Point;

public class Wood extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = -747362553343310970L;
	
	static final Point gfx_WOOD = new Point(0, 123);

	public Wood() {
		super(gfx_WOOD);
	}
}
