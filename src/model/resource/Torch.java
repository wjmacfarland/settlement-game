package model.resource;

import java.awt.Point;

import model.Units.Unit;
import view.lighting.BasicLightRadiusStrategy;
import view.lighting.LightEmitter;

public class Torch extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4297313823116762077L;
	
	static final Point GFX_TORCH = new Point(11, 82);
	private int durability; // amount of times axe/item can be used
	LightEmitter emitter = null;

	public Torch(Unit unit) {
		super(GFX_TORCH);
		this.durability = 200;
		unit.setLightEmitter(new LightEmitter(unit.getLocation(), 6, BasicLightRadiusStrategy.CIRCLE));
	}

	public void use() {
		if (this.usable()) {
			this.durability -= 1; // everytime a unit of wood is
									// gathered/harvested, decrease duribility
		}
	}

	public boolean usable() // if the item in the unit inventory isnt broken
	{
		if (this.durability > 0) {
			return true;
		}
		return false;
	}

	public boolean lowDurability() // if condition is low, so unit can make new
									// axe to remake breaking one
	{
		if (durability <= 25) {
			return true;
		}
		return false;
	}
}
