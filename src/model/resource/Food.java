package model.resource;

import java.awt.Point;

public abstract class Food extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3493793295323644014L;

	public Food(Point gfx) {
		super(gfx);
	}

	protected int foodAmount;

	public int getFoodAmount() {
		return foodAmount;
	}

	public void thisFoodHasBeenEaten() {
		foodAmount = 0;
	}

	@Override
	public String toString() {
		String toReturn = "" + foodAmount;
		return toReturn;
	}

}
