package model.resource;

import java.awt.Point;

public class Metal extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Metal() {
		super(new Point(3, 5));
	}
}
