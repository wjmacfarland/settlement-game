package model.resource;

import java.awt.Point;

public class Water extends Food {

	/**
	 * 
	 */
	private static final long serialVersionUID = 301088868394796340L;

	public Water() {
		super(new Point(60, 1));
		this.foodAmount = 15;
	}

}
