package model.resource;

import java.awt.Point;
import java.io.Serializable;

public abstract class Item implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7517853953288853917L;

	public Point gfx;

	public Item(Point gfx) {
		this.gfx = gfx;
	}
}
