package model;

import helpers.MersenneTwister;
import view.GameView;

public class RandomTerrainCreator {

    public static int mapSize;
    public static int radius;

    public static int numTreeSeeds;
    public static int treeGrowthValue;

    public static int numMountainSeeds;
    public static int mountainGrowthValue;

    public static int numLakeSeeds;
    public static int lakeGrowthValue;

    private char[][] map;
    private static final char PLAINS = ' ';
    private static final char MOUNTAIN = 'M';
    private static final char TREE = 'T';
    private static final char HILL = 'H';
    private static final char SPACE = 'S';
    private static final char LAND = 'X';
    private static final char WATER = 'W';
    private static final char SAND = 'Q'; // Q because 'S' = space and what tile begins with Q
    private static final char APPLE = 'A';
    MersenneTwister randomGenerator = GameView.RANDOM;

    /*
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Welcome to the Random Terrain Creator!");
        System.out.println("This will be a demonstration and explanation of how the random terrain generator works.");
        System.out.println("For the world, a size is needed to map on.");
        System.out.println("The number entered will determine the dimension of the square limit of the map.");
        System.out.println("Any positive number will do. The smaller the less there can be in the world.");
        System.out.println("Though exceeding 175-200, the world may not fit all in the console, depending on the amount of lines your console will allow at once.");
        System.out.println("A recommended size is 150.");
        System.out.println();
        System.out.println("Please enter any integer of your choice.");

        mapSize = Integer.parseInt(keyboard.next());

        for (int x = 0; x < 5; x++) {
            System.out.println();
        }

        System.out.println("Now enter the radius of the world you want.");
        System.out.println("The world will be a circular plane of terrain floating in space.");
        System.out.println("This feature is added in case we want to determine the size of the starting world.");
        System.out.println("In the future, we can add other random planets or worlds that can be visited on the map.");
        System.out.println("A safe radius for an example would be 10 less than half the map size.");
        System.out.println();
        System.out.println("Please enter any integer of your choice.");

        radius = Integer.parseInt(keyboard.next());

        for (int x = 0; x < 5; x++) {
            System.out.println();
        }

        System.out.println("The basis of the planet has been established.");
        System.out.println("Next will be the amount of tree seeds you want planted.");
        System.out.println("Based on the number you input, that many parent trees will be planted randomly through out the planet.");
        System.out.println("Only one tree will be planted, then randomly select another tile, and plant another.");
        System.out.println("This means that the trees will most likely be spread out.");
        System.out.println("BUT there is another factor in determining the trees in the world.");
        System.out.println("After you input the number of tree seeds, you'll be asked to enter the number of growth iterations.");
        System.out.println("Growth iterations allow for the trees to spread randomly from the seed, causing a more random world.");
        System.out.println("A low amount of seeds and a high iteration value will create a world with a few, yet large forests.");
		//System.out.println("A recommended amount of trees is a little less than your entered radius.");
        //System.out.println("A recommended amount of growth iterations is half the tree seeds entered.");
        System.out.println();
        System.out.println("Please enter any integer of your choice for tree seeds.");

        numTreeSeeds = Integer.parseInt(keyboard.next());

        System.out.println();
        System.out.println("Please enter any integer of your choice for tree growth rotations.");

        treeGrowthValue = Integer.parseInt(keyboard.next());

        for (int x = 0; x < 5; x++) {
            System.out.println();
        }

        System.out.println("Mountains are also a tile that can be generated in this world.");
        System.out.println("Mountains cannot be moved on, so having too many mountains will cause extra difficulty.");
        System.out.println("Like trees, mountains will have a initial seed value, and a growth value, but mountains have a harder time expanding.");
        System.out.println();
        System.out.println("Please enter any integer of your choice for mountain seeds.");

        numMountainSeeds = Integer.parseInt(keyboard.next());

        System.out.println();
        System.out.println("Please enter any integer of your choice for mountain growth rotations.");

        mountainGrowthValue = Integer.parseInt(keyboard.next());

        for (int x = 0; x < 5; x++) {
            System.out.println();
        }

        System.out.println("Water tiles work the same way as tree and mountain tiles.");
        System.out.println("I am not sure about how we will work in water as a resource, but water.");
        System.out.println();
        System.out.println("Please enter any integer of your choice for water/lake seeds.");

        numLakeSeeds = Integer.parseInt(keyboard.next());

        System.out.println();
        System.out.println("Please enter any integer of your choice for water/lake growth rotations.");

        lakeGrowthValue = Integer.parseInt(keyboard.next());

        for (int x = 0; x < 25; x++) {
            System.out.println();
        }

        System.out.println("Enter any key to generate your world in the console.");
        keyboard.next();

        RandomTerrainCreator temp = new RandomTerrainCreator(mapSize, radius, numTreeSeeds, treeGrowthValue, numMountainSeeds, mountainGrowthValue, numLakeSeeds, lakeGrowthValue,  numSandSeeds, int sandGrowthValue);
        System.out.println(temp.toString());
    }
*/
    public char[][] getMap() {
        return map;
    }

    public RandomTerrainCreator(int mapSize, int radius, int numTreeSeeds, int treeGrowthValue,
    		int numMountainSeeds, int mountainGrowthValue, int numLakeSeeds, int lakeGrowthValue,
    		int numSandSeeds, int sandGrowthValue) {
        //Random randomGenerator = new Random();
        int[] center = {mapSize / 2 + 1, mapSize / 2 + 1};	// center of 51x51 is 25, 25
        int length = radius;							// radius of planet 
        int random;

        map = new char[mapSize][mapSize];

        // to establish edge of planet
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[0].length; y++) {
                int[] calc = {Math.abs(x - center[0]), Math.abs(y - center[1])};
                int temp = (calc[0] * calc[0]) + (calc[1] * calc[1]);	//a^2 + b^2 = c^2
                if (Math.sqrt(temp) >= length) {
                    map[x][y] = SPACE;
                } else if (x == center[0] && y == center[1]) {
                    map[x][y] = 'C';
                } else {
                    map[x][y] = PLAINS;
                }
            }
        }
        
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[0].length; y++) {
                int[] calc = {Math.abs(x - center[0]), Math.abs(y - center[1])};
                int temp = (calc[0] * calc[0]) + (calc[1] * calc[1]);    //a^2 + b^2 = c^2
                if (Math.sqrt(temp) <= 8) { //given 8 just because good radius
                    map[x][y] = 'C';
                }
            }
        }

		// depending on what seed wants priority first then switch these next functions around
        // this automatically adds trees to the map anywhere. 
        if (numTreeSeeds != 0) // if numTreeSeeds == 0, then dont plant trees
        {
            int count = 0;
            while (count < numTreeSeeds) {
                int random1 = randomGenerator.nextInt(mapSize);
                int random2 = randomGenerator.nextInt(mapSize);

                if (map[random1][random2] == PLAINS) {
                    map[random1][random2] = TREE;
                    count++;
                }
            }
        }
		// tree seeds planted, add tree growth
        // by increasing the limit, more repetition of a tree spreading 
        for (int x = 0; x < treeGrowthValue; x++) {
            treeGrowth();
        }
        
        // makeApple() makes some trees Apples
        makeApple();

        if (numMountainSeeds != 0) // if numTreeSeeds == 0, then dont plant trees
        {
            int count = 0;
            while (count < numMountainSeeds) {
                int random1 = randomGenerator.nextInt(mapSize);
                int random2 = randomGenerator.nextInt(mapSize);

                if (map[random1][random2] == PLAINS) {
                    map[random1][random2] = MOUNTAIN;
                    count++;
                }
            }
        }

        for (int x = 0; x < mountainGrowthValue; x++) {
            mountainGrowth();
        }

        if (numLakeSeeds != 0) // if numLakeSeeds == 0, then dont plant Lakes
        {
            int count = 0;
            while (count < numLakeSeeds) {
                int random1 = randomGenerator.nextInt(mapSize);
                int random2 = randomGenerator.nextInt(mapSize);

                if (map[random1][random2] == PLAINS) {
                    map[random1][random2] = WATER;
                    count++;
                }
            }
        }

        for (int x = 0; x < lakeGrowthValue; x++) {
            lakeGrowth();
        }
        
        if (numSandSeeds != 0) // if numSandSeeds == 0, then dont plant Sand
        {
            int count = 0;
            while (count < numSandSeeds) {
                int random1 = randomGenerator.nextInt(mapSize);
                int random2 = randomGenerator.nextInt(mapSize);

                if (map[random1][random2] == PLAINS) {
                    map[random1][random2] = SAND;
                    count++;
                }
            }
        }
        
        for (int x = 0; x < sandGrowthValue; x++) {
            sandGrowth();
        }
        
    }

    //-----------------
    public void treeGrowth() {
        int random;
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[0].length; y++) {
                if (map[x][y] == TREE) {
                    random = randomGenerator.nextInt(10);		// by increasing the range of the random number, less chance of growth
                    if (map[x - 1][y] == PLAINS && random == 1) // 10% chance 
                    {
                        map[x - 1][y] = TREE;
                    }
                    if (map[x + 1][y] == PLAINS && random == 2) {
                        map[x + 1][y] = TREE;
                    }
                    if (map[x][y - 1] == PLAINS && random == 3) {
                        map[x][y - 1] = TREE;
                    }
                    if (map[x][y + 1] == PLAINS && random == 4) {
                        map[x][y + 1] = TREE;
                    }
                }
            }
        }
    }

    public void mountainGrowth() {
        int random;
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[0].length; y++) {
                if (map[x][y] == MOUNTAIN) {
                    random = randomGenerator.nextInt(25);		// by increasing the range of the random number, less chance of growth
                    if (map[x - 1][y] == PLAINS && random == 1) //  
                    {
                        map[x - 1][y] = MOUNTAIN;
                    }
                    if (map[x + 1][y] == PLAINS && random == 2) {
                        map[x + 1][y] = MOUNTAIN;
                    }
                    if (map[x][y - 1] == PLAINS && random == 3) {
                        map[x][y - 1] = MOUNTAIN;
                    }
                    if (map[x][y + 1] == PLAINS && random == 4) {
                        map[x][y + 1] = MOUNTAIN;
                    }
                }
            }
        }
    }

    public void lakeGrowth() {
        int random;
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[0].length; y++) {
                if (map[x][y] == WATER) {
                    random = randomGenerator.nextInt(10);		// by increasing the range of the random number, less chance of growth
                    if (map[x - 1][y] == PLAINS && random == 1) // 10% chance 
                    {
                        map[x - 1][y] = WATER;
                    }
                    if (map[x + 1][y] == PLAINS && random == 2) {
                        map[x + 1][y] = WATER;
                    }
                    if (map[x][y - 1] == PLAINS && random == 3) {
                        map[x][y - 1] = WATER;
                    }
                    if (map[x][y + 1] == PLAINS && random == 4) {
                        map[x][y + 1] = WATER;
                    }
                }
            }
        }
    }
    
    public void sandGrowth() {
    	int random;
    	 for (int x = 0; x < map.length; x++) {
             for (int y = 0; y < map[0].length; y++) {
                 if (map[x][y] == SAND) {
                     random = randomGenerator.nextInt(10);		// by increasing the range of the random number, less chance of growth
                     if (map[x - 1][y] == PLAINS && random == 1) // 5% chance 
                     {
                         map[x - 1][y] = SAND;
                     }
                     if (map[x + 1][y] == PLAINS && random == 2) {
                         map[x + 1][y] = SAND;
                     }
                     if (map[x][y - 1] == PLAINS && random == 3) {
                         map[x][y - 1] = SAND;
                     }
                     if (map[x][y + 1] == PLAINS && random == 4) {
                         map[x][y + 1] = SAND;
                     }
                 }
             }
         }
    }
    
    public void makeApple()
    {
    	int random;
    	for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[0].length; y++) {
            	if (map[x][y] == TREE) {
            		random = randomGenerator.nextInt(8);// 33% chance of tree having apples
            		if(random == 1) {
            			map[x][y] = APPLE;
            		}
            			
            	}
            }
    	}
    	
    }

    public boolean isFull() {
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[0].length; y++) {
                if (map[x][y] == PLAINS) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
	public String toString() {
        String toReturn = "";
        for (int x = 0; x < map.length; x++) {
            toReturn = toReturn + "| ";
            for (int y = 0; y < map[0].length; y++) {
                toReturn = toReturn + " " + map[x][y] + " ";
            }

            toReturn = toReturn + " | \n";
        }

        return toReturn;
    }
}
