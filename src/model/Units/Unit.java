package model.Units;

import helpers.NameGenerator;

import java.awt.Point;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import view.GameGUI;
import view.GameView;
import view.Drawable;
import view.lighting.LightEmitter;
import view.panels.UnitPanel;
import model.FoodCollection;
import model.GameMap;
import model.Pathfinder;
import model.TerrainTile;
import model.Units.Tasks.DepositFoodTask;
import model.Units.Tasks.DepositMetalTask;
import model.Units.Tasks.DepositWoodTask;
import model.Units.Tasks.UnitTask;
import model.constructs.Bed;
import model.constructs.BedCollection;
import model.goals.DepositFoodGoal;
import model.goals.FindBedGoal;
import model.goals.FindWarmthGoal;
import model.resource.Apple;
import model.resource.Axe;
import model.resource.FishingRod;
import model.resource.Food;
import model.resource.Item;
import model.resource.Metal;
import model.resource.Torch;
import model.resource.Wood;
import model.Node;

public class Unit implements Drawable, Comparable<Unit>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static enum UnitState {
		Wandering("Wandering", 0),
		Gathering("Gathering", 1),
		Building("Building", 2),
		Sleeping("Sleeping", 1),
		Tired("Tired", 1),
		Eating("Eating", 1),
		Cold("Cold", 1);

		String stateName;
		int doingTask;

		UnitState(String stateName, int doing) {
			this.stateName = stateName;
			this.doingTask = doing;
		}
	}

	public static enum InventoryItem {
		Axe,
		Wood,
		Apple,
		Torch,
		FishingRod,
		Metal,
		Food;
	}

	/*
	 * 
	 * Instance Variables
	 */

	private List<Item> inventory;

	public transient UnitPanel unitPanel;

	protected String name;

	protected UnitState state;

	private Bed bed;
	private boolean doneEatingFood = false;

	/*
	 * 
	 * Location variables
	 */
	protected Point location;
	protected int xOffset;
	protected int yOffset;
	protected Point spawnLocation;

	/*
	 * 
	 * Need variables
	 */
	private float sustenance;
	private float fatigue;
	private float warmth;

	protected LightEmitter lightEmitter;

	protected boolean isDead = false;
	protected boolean isHungry = false;
	protected boolean taskComplete = false;
	protected boolean gotBedPath = false;
	private boolean gotFoodPath = false;
	private boolean gotWarmthPath = false;
	private boolean doneWarming = false;
	private boolean doneSleeping = false;

	protected Stack<UnitTask> tasks = null;
	protected UnitTask currentTask = null;

	Stack<Node> pathBackToTask = new Stack<Node>();

	Stack<Node> currentPath = null;

	private final int INVENTORY_MAX_SIZE = 10;

	/*
	 * 
	 * Constructors
	 */
	public Unit(int x, int y) {
		// call the other constructor with these values stored in Point form
		this(new Point(x, y));
	}

	public Unit(Point location) {
		this(location, 100);
	}

	public Unit(Point location, int initSust) {
		this.spawnLocation = location;
		this.location = location;
		this.sustenance = initSust;
		this.fatigue = 100;
		this.warmth = 100;
		// lightEmitter = new LightEmitter(new Point(location.x, location.y), 6,
		// BasicLightRadiusStrategy.CIRCLE);
		name = NameGenerator.getInstance().getRandomName();
		tasks = new Stack<UnitTask>();
		state = UnitState.Wandering;
		unitPanel = new UnitPanel(this);
		inventory = new LinkedList<Item>();
		// inventory.add(new Axe());
		// inventory.add(new Torch(this));

		inventory.add(new FishingRod());
	}

	/*
	 * Main update method
	 */
	public boolean update() {
		if (isDead) {
			updateGatheringandBuildingNums();
			return false;
		}
		// adjust sustenance
		this.sustenance -= 0.05f;
		// adjust fatigue
		this.fatigue -= 0.07f;
		adjustWarmth();
		/*
		 * Need fulfillment checks
		 */

		if (!hasItem(InventoryItem.Torch)) {
			lightEmitter = null;
		} else {
			getTorch().use();
			if (!getTorch().usable()) {
				removeTorch();
				GameGUI.log("Torch used up!");
			}
		}

		// If the unit is cold, find warmth
		if (this.getState().equals("Cold")) {
			if (!gotWarmthPath) {
				currentPath = null;
			}
			rechargeWarmth();
			return true;
		}

		// First off, if unit is already sleeping, keep sleeping
		if (this.getState().equals("Sleeping")) {
			if (!gotBedPath) {
				currentPath = null;
			}
			sleep();
			return true;
		}

		// otherwise if unit is tired, and a bed is avaliable, sleep in it
		if (fatigue < 20 && BedCollection.getInstance().areOpenBeds() > 0) {
			if (!gotBedPath) {
				currentPath = null;
			}
			sleep();
			return true;
		}

		// Second, if unit is already eating, keep eating
		setHungerBoolean();
		if (this.getState().equals("Eating")) {
			if (!gotFoodPath) {
				currentPath = null;
			}
			comsumeFood();
			return true;
		}

		// otherwise, if he's hungry, and there is food, eat
		if (isHungry && this.hasItem(InventoryItem.Food)) {
			Food toEat = this.depositFood();
			sustenance += toEat.getFoodAmount();

		}
		if (isHungry && FoodCollection.getInstance().size() > 0) {
			if (!gotFoodPath) {
				currentPath = null;
			}
			comsumeFood();
			return true;
		}

		// Make sure unit is alive, kill if hunger or fatigue get too low. Do
		// nothing else
		if (sustenance < 0 || fatigue < 0 || warmth < 0) {
			GameGUI.log(name + " died ):\n");
			updateGatheringandBuildingNums();
			return false;
		}
		// Eat if hungry, and continue along with other tasks
		if (isHungry && !FoodCollection.getInstance().isEmpty()) {
			comsumeFood();
		}
		// end of need fulfillment checks

		/*
		 * Task Management
		 */

		// if unit is doing a task, keep doing it
		if (currentTask != null) {
			if (!taskComplete)
				taskComplete = currentTask.execute();
			// if task is done, reset proper task things and return, thats it
			else {
				UnitTaskCollection.getInstance().setChanged(true);
				currentPath = null;
				taskComplete = false;
				currentTask = null;
				return true;
			}
			return true;
		}
		// if unit has unit specific tasks to complete from personal stack,
		// assign it to
		// current task
		else if (!tasks.isEmpty()) {
			taskComplete = false;
			currentTask = tasks.pop();
			UnitTaskCollection.getInstance().setChanged(true);
			taskComplete = currentTask.execute();
			return true;
		}
		// Otherwise, grab a task off the main task list (if there is one)
		else if (!UnitTaskCollection.getInstance().isEmpty()) {
			updateGatheringandBuildingNums();
			addTask(UnitTaskCollection.getInstance().getTask());
			currentTask = tasks.pop();
			UnitTaskCollection.getInstance().setChanged(true);
			taskComplete = currentTask.execute();
			return true;
		}
		// otherwise, if task free, wander around
		else {
			if (this.hasItem(InventoryItem.Wood)) {
				this.addTask(new DepositWoodTask(this));
				currentPath = null;
				return true;

			}
			if (this.hasItem(InventoryItem.Food)) {
				this.addTask(new DepositFoodTask(this));
				currentPath = null;
				return true;

			}
			if (this.hasItem(InventoryItem.Metal)) {
				this.addTask(new DepositMetalTask(this));
				currentPath = null;
				return true;

			}

			updateGatheringandBuildingNums();
			wander();
			return true;
		}
	}

	private void rechargeWarmth() {
		state = UnitState.Cold;
		if (currentPath == null || currentPath.size() == 0) {
			UnitTaskCollection.getInstance().setChanged(true);
			currentPath = Pathfinder.getInstance().findGoal(location.x, location.y, new FindWarmthGoal());

			if (currentPath != null && currentPath.size() > 0) {
				// pop the starting point
				gotWarmthPath = true;
				pathBackToTask.push(currentPath.pop());
			} else {
				setState(UnitState.Wandering);
			}
		} else if (currentPath.size() == 1 && !doneWarming) {
			inventory.add(new Torch(this));
			doneWarming = true;
		} else if (doneWarming && pathBackToTask.size() > 0) {
			Node nextMove = pathBackToTask.pop();
			Point nextCoords = nextMove.coords;
			location.x = nextCoords.x;
			location.y = nextCoords.y;
			if (lightEmitter != null) {
				lightEmitter.loc.x = location.x;
				lightEmitter.loc.y = location.y;
			}
			if (pathBackToTask.size() == 0) {
				gotWarmthPath = false;
				currentPath = null;
				doneWarming = false;
				setState(UnitState.Wandering);
			}
		}
		// skip generation, just get next move
		else {
			Node nextMove = currentPath.pop();
			pathBackToTask.push(nextMove);
			Point nextCoords = nextMove.coords;
			location.x = nextCoords.x;
			location.y = nextCoords.y;
			if (lightEmitter != null) {
				lightEmitter.loc.x = location.x;
				lightEmitter.loc.y = location.y;
			}
		}

	}

	private void adjustWarmth() {
		TerrainTile tile = getTileBeneath();
		float lv = tile.getLightValue();
		if (warmth < 0.50f) {
			warmth -= (0.50f - lv) * 2;
		} else {
			warmth += (lv - 0.50f) * 2;
		}
		if (warmth < 0) {
			isDead = true;
		} else if (warmth < 30) {
			this.setState(UnitState.Cold);
		} else if (warmth > 100) {
			warmth = 100;
		}
	}

	private void updateGatheringandBuildingNums() {
		if (this.getState().equals("Gathering")) {
			UnitCollection.getInstance();
			UnitCollection.gatheringUnits--;
		}
		if (this.getState().equals("Building")) {
			UnitCollection.getInstance();
			UnitCollection.buildingUnits--;
		}
	}

	private void setHungerBoolean() {
		if (sustenance < 20) {
			isHungry = true;
		} else {
			isHungry = false;
		}
	}

	/*
	 * Inventory Related Methods
	 */

	public boolean hasWood(int amount) {
		int count = 0;
		for (Item item : inventory) {
			if (item instanceof Wood) {
				count++;
			}
		}
		return (count >= amount);
	}

	// returns a wood and removes from inventory if exists

	public Axe getAxe() {
		Axe toReturn = null;
		for (Item item : inventory) {
			if (item instanceof Axe) {
				toReturn = (Axe) item;
				break;
			}
		}
		return toReturn;
	}

	public Axe removeAxe() {
		Axe toReturn = null;
		for (Item item : inventory) {
			if (item instanceof Axe) {
				toReturn = (Axe) item;
				break;
			}
		}
		// only remove item if it was found
		if (toReturn != null)
			inventory.remove(toReturn);
		return toReturn;
	}

	// returns food and removes from inventory
	public Food depositFood() {
		Food toReturn = null;
		for (Item item : inventory) {
			if (item instanceof Food) {
				toReturn = (Food) item;
				break;
			}
		}
		// only remove item if it was found
		if (toReturn != null)
			inventory.remove(toReturn);
		return toReturn;
	}

	public Wood depositWood() {
		Wood toReturn = null;
		for (Item item : inventory) {
			if (item instanceof Wood) {
				toReturn = (Wood) item;
				break;
			}
		}
		// only remove item if it was found
		if (toReturn != null)
			inventory.remove(toReturn);
		return toReturn;
	}

	public Metal depositMetal() {
		Metal toReturn = null;
		for (Item item : inventory) {
			if (item instanceof Metal) {
				toReturn = (Metal) item;
				break;
			}
		}
		// only remove item if it was found
		if (toReturn != null)
			inventory.remove(toReturn);
		return toReturn;
	}

	public boolean hasItem(InventoryItem item) {
		boolean flag = false;
		switch (item) {
		case Axe:
			for (Item toGet : inventory) {
				if (toGet instanceof Axe) {
					flag = true;
					break;
				}
			}
			break;
		case Torch:
			for (Item toGet : inventory) {
				if (toGet instanceof Torch) {
					flag = true;
					break;
				}
			}
			break;
		case FishingRod:
			for (Item toGet : inventory) {
				if (toGet instanceof FishingRod) {
					flag = true;
					break;
				}
			}
			break;
		case Food:
			for (Item toGet : inventory) {
				if (toGet instanceof Food) {
					flag = true;
					break;
				}
			}
			break;
		case Wood:
			for (Item toGet : inventory) {
				if (toGet instanceof Wood) {
					flag = true;
					break;
				}
			}
			break;
		case Apple:
			for (Item toGet : inventory) {
				if (toGet instanceof Apple) {
					flag = true;
					break;
				}
			}
			break;
		case Metal:
			for (Item toGet : inventory) {
				if (toGet instanceof Metal) {
					flag = true;
					break;
				}
			}
			break;
		default:
			break;
		}

		return flag;
	}

	public Torch removeTorch() {
		Torch toReturn = null;
		for (Item item : inventory) {
			if (item instanceof Torch) {
				toReturn = (Torch) item;
				break;
			}
		}
		// only remove item if it was found
		if (toReturn != null)
			inventory.remove(toReturn);
		return toReturn;
	}

	public Torch getTorch() {
		Torch toReturn = null;
		for (Item item : inventory) {
			if (item instanceof Torch) {
				toReturn = (Torch) item;
				break;
			}
		}
		return toReturn;
	}

	public void setDead(boolean toSet) {
		isDead = toSet;
	}

	private void wander() {
		state = UnitState.Wandering;
		// generate random path if none exists
		if (currentPath == null || currentPath.size() == 0) {
			// generates a random number between -20 and 20
			int wanderX = GameView.RANDOM.nextInt(20) - 10;
			int wanderY = GameView.RANDOM.nextInt(20) - 10;
			int goalX = location.x + wanderX;
			int goalY = location.y + wanderY;

			TerrainTile tile = GameMap.getInstance().get(goalX, goalY);

			if (tile == null) {
				return;
			}

			if (GameMap.getInstance().safeTilesExist && !tile.isSafe) {
				return;
			}

			if (tile.getLightValue() < 0.5f) {
				return;
			}

			currentPath = Pathfinder.getInstance().findPath(location.x, location.y, goalX, goalY);

			if (currentPath == null)
				return;
			if (currentPath.size() > 0) {
				// pop the starting point
				currentPath.pop();
			}
		}
		// skip generation, just get next move
		else {
			Node nextMove = currentPath.pop();
			Point nextCoords = nextMove.coords;
			location.x = nextCoords.x;
			location.y = nextCoords.y;
			if (lightEmitter != null) {
				lightEmitter.loc.x = location.x;
				lightEmitter.loc.y = location.y;
			}
		}

	}

	/*
	 * 
	 * Getters / Setters
	 */
	public Point getLocation() {
		return location;
	}

	public Point getOffset() {
		return new Point(xOffset, yOffset);
	}

	public LightEmitter getLight() {
		return this.lightEmitter;
	}

	public float getSustenance() {
		return this.sustenance;
	}

	public String getName() {
		return name;
	}

	public String getState() {
		return state.stateName;
	}

	public void setState(Unit.UnitState state) {

		this.state = state;
	}

	public List<Item> getInventory() {
		return inventory;
	}

	public boolean inventoryIsFull() {
		if (inventory.size() == INVENTORY_MAX_SIZE) {
			return true;
		} else
			return false;
	}

	public boolean addItem(Item item) {
		if (inventory.size() < INVENTORY_MAX_SIZE)
			return inventory.add(item);
		else
			return false;

	}

	public void setLocation(Point point) {
		location.x = point.x;
		location.y = point.y;
	}

	public void addTask(UnitTask task) {
		task.setUnit(this);
		tasks.push(task);
	}

	public Float getFatigue() {
		return fatigue;
	}

	public Float getWarmth() {
		return warmth;
	}

	public void sleep() {
		state = UnitState.Sleeping;
		if (currentPath == null || currentPath.size() == 0) {

			currentPath = Pathfinder.getInstance().findGoal(location.x, location.y, new FindBedGoal());

			if (currentPath != null && currentPath.size() > 0) {
				// pop the starting point
				TerrainTile tile = GameMap.getInstance().get(currentPath.get(0).coords);
				if (tile.construct != null && tile.construct instanceof Bed) {
					bed = (Bed) tile.construct;
					bed.setOpen(false);
				}
				gotBedPath = true;
				pathBackToTask.push(currentPath.pop());
			} else {
				fatigue = 100;
			}
		} else if (currentPath.size() == 1 && !doneSleeping) {
			if (fatigue < 100) {
				if (fatigue + 1.5 > 100) {
					fatigue = 100;
					doneSleeping = true;
					bed.setOpen(true);

				} else {
					fatigue += 1.5;
				}
			} else {
				doneSleeping = true;
				bed.setOpen(true);

			}
		} else if (doneSleeping && pathBackToTask.size() > 0) {
			Node nextMove = pathBackToTask.pop();
			Point nextCoords = nextMove.coords;
			location.x = nextCoords.x;
			location.y = nextCoords.y;
			if (lightEmitter != null) {
				lightEmitter.loc.x = location.x;
				lightEmitter.loc.y = location.y;
			}
			if (pathBackToTask.size() == 0) {
				gotBedPath = false;
				currentPath = null;
				doneSleeping = false;
				setState(UnitState.Wandering);
			}
		}
		// skip generation, just get next move
		else {
			Node nextMove = currentPath.pop();
			pathBackToTask.push(nextMove);
			Point nextCoords = nextMove.coords;
			location.x = nextCoords.x;
			location.y = nextCoords.y;
			if (lightEmitter != null) {
				lightEmitter.loc.x = location.x;
				lightEmitter.loc.y = location.y;
			}
		}
	}

	public boolean isTired() {
		return isTired();
	}

	public void comsumeFood() {
		state = UnitState.Eating;
		if (currentPath == null || currentPath.size() == 0) {
			UnitTaskCollection.getInstance().setChanged(true);
			currentPath = Pathfinder.getInstance().findGoal(location.x, location.y, new DepositFoodGoal());

			if (currentPath.size() > 0) {
				// pop the starting point
				gotFoodPath = true;
				pathBackToTask.push(currentPath.pop());
			}
		} else if (currentPath.size() == 1 && !doneEatingFood) {
			if (sustenance <= 99) {
				if (!FoodCollection.getInstance().isEmpty()) {

					Food food = FoodCollection.getInstance().get();

					if ((food.getFoodAmount() + sustenance) > 100)
						sustenance = 100;
					else
						sustenance += food.getFoodAmount();
				} else {
					doneEatingFood = true;
				}
			} else {
				doneEatingFood = true;
			}
		} else if (doneEatingFood && pathBackToTask.size() > 0) {
			Node nextMove = pathBackToTask.pop();
			Point nextCoords = nextMove.coords;
			location.x = nextCoords.x;
			location.y = nextCoords.y;
			if (lightEmitter != null) {
				lightEmitter.loc.x = location.x;
				lightEmitter.loc.y = location.y;
			}
			if (pathBackToTask.size() == 0) {
				gotFoodPath = false;
				currentPath = null;
				doneEatingFood = false;
				setState(UnitState.Wandering);
			}
		}
		// skip generation, just get next move
		else {
			Node nextMove = currentPath.pop();
			pathBackToTask.push(nextMove);
			Point nextCoords = nextMove.coords;
			location.x = nextCoords.x;
			location.y = nextCoords.y;
			if (lightEmitter != null) {
				lightEmitter.loc.x = location.x;
				lightEmitter.loc.y = location.y;
			}
		}
	}

	public void updateLightEmitter(Point point) {
		if (lightEmitter != null) {
			lightEmitter.loc.x = point.x;
			lightEmitter.loc.y = point.y;
		}
	}

	// Return the tile the unit is standing on
	public TerrainTile getTileBeneath() {
		return GameMap.getInstance().get(getLocation());
	}

	public void setLightEmitter(LightEmitter emitter) {
		lightEmitter = emitter;
	}

	/*
	 * 
	 * Rendering Code
	 */
	@Override
	public void draw(Graphics g, SpriteSheet tiles, Point camera) {
		Point locDraw = new Point(location.x * 32 - camera.x, location.y * 32 - camera.y);
		tiles.getSubImage(0, 2).draw(locDraw.x, locDraw.y);
		g.setColor(Color.yellow);
		g.drawString("" + name, locDraw.x - name.length() * 2, locDraw.y - 16);
	}

	@Override
	public void draw(Graphics g, SpriteSheet tiles, Point camera, Color lighting) {
		Point locDraw = new Point(location.x * 32 - camera.x, location.y * 32 - camera.y);
		tiles.getSubImage(0, 2).draw(locDraw.x, locDraw.y, lighting);
		g.setColor(Color.yellow);
		g.drawString("" + name, locDraw.x - name.length() * 2, locDraw.y - 16);
	}

	public void draw(Graphics g, SpriteSheet tiles, Point camera, float sunlight) {
		float light = 0;
		float tileLight = getTileBeneath().getLightValue();
		light = (sunlight > tileLight) ? sunlight : tileLight;
		Color hue = new Color(0.50f + light / 2, 0.50f + light / 2, 1);
		Point locDraw = new Point(location.x * 32 - camera.x, location.y * 32 - camera.y);
		tiles.getSubImage(0, 2).draw(locDraw.x, locDraw.y, hue);
		g.setColor(Color.yellow);
		g.drawString("" + name, locDraw.x - name.length() * 2, locDraw.y - 16);
	}

	@Override
	public int compareTo(Unit other) {
		if (this.state.doingTask > other.state.doingTask) {
			return 1;
		} else if (this.state.doingTask > other.state.doingTask) {
			return 0;
		} else {
			return -1;
		}
	}

	public void load() {
		unitPanel = new UnitPanel(this);
	}
}
