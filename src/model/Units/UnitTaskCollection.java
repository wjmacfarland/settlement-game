package model.Units;

import java.util.LinkedList;
import java.util.Queue;

import view.GameGUI;
import model.Units.Tasks.UnitTask;

public class UnitTaskCollection {

	protected Queue<UnitTask> otherTaskList;
	protected Queue<UnitTask> buildingTaskList;
	protected Queue<UnitTask> gatheringTaskList;
	private static UnitTaskCollection instance = null;
	private boolean isReachable;
	private boolean hasChanged = false;

	// amount of units on build and gather tasks defauls to one each
	public int unitsToGather = 2, unitsToBuild = 2;

	public static UnitTaskCollection getInstance() {
		if (instance == null) {
			instance = new UnitTaskCollection();
		}
		return instance;
	}

	public UnitTaskCollection() {
		otherTaskList = new LinkedList<UnitTask>();
		buildingTaskList = new LinkedList<UnitTask>();
		gatheringTaskList = new LinkedList<UnitTask>();
	}

	public void addBuildingTask(UnitTask task) {
		buildingTaskList.add(task);
	}

	public void addOtherTask(UnitTask task) {
		otherTaskList.add(task);
	}

	public void addGatheringTask(UnitTask task) {
		gatheringTaskList.add(task);
	}

	// returns null if list is empty
	public UnitTask getTask() {
		UnitCollection.getInstance();
		if (UnitCollection.buildingUnits < unitsToBuild) {
			if (!buildIsEmpty()) {
				UnitCollection.getInstance();
				UnitCollection.buildingUnits++;
				return buildingTaskList.poll();
			}
		}
		UnitCollection.getInstance();
		if (UnitCollection.gatheringUnits < unitsToGather) {
			if (!gatherIsEmpty()) {
				UnitCollection.getInstance();
				UnitCollection.gatheringUnits++;
				return getReachableGatheringTask();
			}
		}
		return otherTaskList.poll();
	}

	public boolean otherIsEmpty() {
		return otherTaskList.isEmpty();
	}

	private UnitTask getReachableGatheringTask() {
		UnitTask toReturn = null;
		for (UnitTask task : gatheringTaskList) {
			if (task.isReachable()) {
				toReturn = task;
			}

		}
		gatheringTaskList.remove(toReturn);
		return toReturn;
	}

	public boolean isEmpty() {
		UnitCollection.getInstance();
		if (!buildIsEmpty() && unitsToBuild > UnitCollection.buildingUnits) {
			return false;
		} else {
			UnitCollection.getInstance();
			if (!gatherIsEmpty() && unitsToGather > UnitCollection.gatheringUnits
					&& doesContainReachableTasks()) {
				return false;
			} else
				return otherTaskList.isEmpty();
		}
	}

	public boolean buildIsEmpty() {
		return this.buildingTaskList.isEmpty();
	}

	public boolean gatherIsEmpty() {
		return this.gatheringTaskList.isEmpty();
	}

	public boolean doesContainReachableTasks() {
		boolean toReturn = false;
		for (UnitTask task : gatheringTaskList) {
			if (task.isReachable()) {
				toReturn = true;
			}
		}
		return toReturn;
	}

	public void setUnitsToGather(int toGather) {
		unitsToGather = toGather;
	}

	public void adjustUnitsToGather(int adjAmount) {
		unitsToGather += adjAmount;
		if (unitsToGather < 0) {
			unitsToGather = 0;
		}
	}

	public void setUnitsToBuild(int toBuild) {
		unitsToBuild = toBuild;
	}

	public void adjustUnitsToBuild(int adjAmount) {
		unitsToBuild += adjAmount;
		if (unitsToBuild < 0) {
			unitsToBuild = 0;
		}
	}

	public void checkReachability() {
		updateReachability();
		if (!isReachable && (gatheringTaskList.size() > 0)) {
			GameGUI.log("GatheringTasks Cleared");
			gatheringTaskList.clear();
		}
	}

	public void updateReachability() {
		isReachable = false;
		for (UnitTask task : gatheringTaskList) {
			if (task.isReachable()) {
				isReachable = true;
			}
		}

	}

	public void setChanged(boolean toSet) {
		hasChanged = toSet;
	}

	public boolean getChangeState() {
		return hasChanged;
	}
	
	public void clearAllTasks(){
		clearGatheringTasks();
		clearBuildingTasks();
		clearOtherTasks();
	}

	public void clearGatheringTasks() {
		gatheringTaskList.clear();
	}

	public void clearBuildingTasks() {
		buildingTaskList.clear();
	}

	public void clearOtherTasks() {
		otherTaskList.clear();
	}

	public int getTotalAmtTasks() {
		return buildingTaskList.size() + gatheringTaskList.size() + otherTaskList.size();
	}
	
	//SAVE/LOAD FUNCTIONS
	public Queue<UnitTask> getGatheringTasksForSaving(){
		return gatheringTaskList;
	}
	public Queue<UnitTask> getBuildingTasksForSaving(){
		return buildingTaskList;
	}
	public Queue<UnitTask> getOtherTasksForSaving(){
		return otherTaskList;
	}
	
	public void loadGathering(Queue<UnitTask> toLoad ){
		gatheringTaskList = toLoad;
	}
	public void loadBuilding(Queue<UnitTask> toLoad ){
		buildingTaskList = toLoad;
	}
	public void loadOther(Queue<UnitTask> toLoad ){
		otherTaskList = toLoad;
	}
	
	public int getGathering(){
		return unitsToGather;
	}
	public int getBuilding(){
		return unitsToBuild;
	}
	
	public int getCurrentUnitsGathering(){
		return UnitCollection.gatheringUnits;
	}
	public int getCurrentUnitsBuilding(){
		return UnitCollection.buildingUnits;	
	}
	public void setCurrentUnitsGathering(int toSet){
		UnitCollection.gatheringUnits = toSet;
	}
	public void setCurrentUnitsBuilding(int toSet){
		UnitCollection.buildingUnits = toSet;	
	}

}
