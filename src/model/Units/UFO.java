package model.Units;

import java.awt.Point;
import java.io.Serializable;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import model.GameMap;

//class defines a UFO object
//UFO will fly around a square, wraping around to the other
//side when it is sufficiently far off the planet
//UFO will check every iteration to see if it is ontop of a player
//if the UFO is ontop of a player, and the player is outside of the 
//alien protection, that player is abducted

public class UFO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Point position;
	Point velocity;
	int updates;
	static final float MAX_VELOCITY = (float) 3.0;
	static final int MAX_Y = GameMap.getInstance().getSize().y;
	static final int MAX_X = GameMap.getInstance().getSize().x;
	static final int UPDATES_BEFORE_VEL_CHANGE = 50;

	public UFO(Point spawnLocation) {
		position = spawnLocation;
		velocity = new Point(1, 1);
		// velocity.setLocation((int)Math.random()*2*MAX_VELOCITY -
		// MAX_VELOCITY,(int) Math.random()*2*MAX_VELOCITY-MAX_VELOCITY);
		updates = UPDATES_BEFORE_VEL_CHANGE;
	}

	public void updateUFO() {
		position.setLocation(position.x + velocity.getX(), position.y + velocity.getY());
		checkForPlayersAt(position);
		updates--;
		if (updates < 0) {
			updateVelocity();
			updates = UPDATES_BEFORE_VEL_CHANGE;
		}
		wrapMovement();
	}

	private void updateVelocity() {
		velocity.setLocation(velocity.x + Math.random() * .5 * MAX_VELOCITY - .25 * MAX_VELOCITY, velocity.y
				+ Math.random() * .5 * MAX_VELOCITY - .25 * MAX_VELOCITY);
		if (velocity.getX() > MAX_VELOCITY) {
			velocity.setLocation(MAX_VELOCITY, velocity.y);
		}
		if (velocity.getX() < -MAX_VELOCITY) {
			velocity.setLocation(-MAX_VELOCITY, velocity.y);
		}
		if (velocity.getY() > MAX_VELOCITY) {
			velocity.setLocation(velocity.x, MAX_VELOCITY);
		}
		if (velocity.getX() < -MAX_VELOCITY) {
			velocity.setLocation(velocity.x, -MAX_VELOCITY);
		}
		if (velocity.x == 0 && velocity.y == 0) {
			velocity = new Point(1, -1);
		}
	}

	private void wrapMovement() {
		if (position.x > MAX_X) {
			position.setLocation(0, position.y);
		}
		if (position.x < 0) {
			position.setLocation(MAX_X, position.y);
		}
		if (position.y > MAX_Y) {
			position.setLocation(position.x, 0);
		}
		if (position.y < 0) {
			position.setLocation(position.x, MAX_Y);
		}
	}

	private void checkForPlayersAt(Point location) {
		UnitCollection units = UnitCollection.getInstance();
		
		for(Unit unit : units){
			for(int i = -2;i<3;i++){
				for(int j=-2;j<3;j++){
					Point checkHere = new Point(location.x+i,location.y+j);
					if(unit.location.x == checkHere.x && unit.location.y == checkHere.y){
						if(!GameMap.getInstance().get(checkHere).isSafe){

							unit.isDead = true;
						}
					}
				}
			}
		}
	}

	public void draw(Graphics g, SpriteSheet tiles, Point camera) {
		Point locDraw = new Point(position.x * 32 - camera.x, position.y * 32 - camera.y);
		tiles.getSubImage(6, 123).draw(locDraw.x, locDraw.y);
	}
}
