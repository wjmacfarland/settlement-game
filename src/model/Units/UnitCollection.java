package model.Units;

import java.util.Collections;
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class UnitCollection implements Iterable<Unit>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4645765309075153863L;

	private static LinkedList<Unit> units;
	private static LinkedList<Unit> deadUnits;

	public static int gatheringUnits = 0;
	public static int buildingUnits = 0;

	private static UnitCollection instance = null;

	public static UnitCollection getInstance() {
		if (instance == null)
			instance = new UnitCollection();
		return instance;
	}

	public UnitCollection() {
		units = new LinkedList<Unit>();
		deadUnits = new LinkedList<Unit>();
	}

	public void add(Unit unit) {
		units.add(unit);

	}

	public void remove(Unit unit) {
		UnitCollection.units.remove(unit);
	}

	public void addDeadUnit(Unit unit) {
		deadUnits.add(unit);
	}

	public void clearDeadUnits() {
		units.removeAll(deadUnits);
	}

	@Override
	public Iterator<Unit> iterator() {
		return units.iterator();
	}

	public LinkedList<Unit> getDeadUnits() {
		return deadUnits;
	}

	// Clear all units from collection
	public void clear() {
		units.clear();
		deadUnits.clear();
	}

	public int getGatheringUnits() {
		int count = 0;
		for (Unit unit : units) {
			String temp = unit.getState();
			if (temp.equals("Gathering"))
				count++;
		}
		return count;
	}

	public int getBuildingUnits() {
		int count = 0;
		for (Unit unit : units) {
			if (unit.getState().equals("Building"))
				count++;

		}
		return count;
	}

	public Unit getUnit() {
		return units.peek();
	}

	@SuppressWarnings("unchecked")
	public void sort() {
		Collections.sort(units);
	}

	public Queue<Unit> getUnitsForSaving() {
		return units;
	}

	public int size() {
		return units.size();
	}

}
