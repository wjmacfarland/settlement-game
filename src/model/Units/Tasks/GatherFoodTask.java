package model.Units.Tasks;

import java.awt.Point;
import java.util.LinkedList;
import java.util.Stack;

import model.GameMap;
import model.Pathfinder;
import model.TerrainTile;
import model.Node;
import model.Units.Unit;
import model.Units.Unit.InventoryItem;
import model.constructs.FishingHole;
import model.constructs.Harvestable;
import model.resource.Food;
import model.resource.Item;

/*
 * Gets food from selected tile and deposits it in the food container, if there is
 * a container built
 */

public class GatherFoodTask implements UnitTask {

	protected TerrainTile tile = null;
	protected int x, y;
	protected Unit unit = null;
	protected Stack<Node> currentPath = null;
	private boolean hasGathered = false;
	private boolean hasNotUpdated = true;
	private int appleAmount;
	private boolean isReachable = true;


	public GatherFoodTask(int x, int y) {
		this.x = x;
		this.y = y;
		tile = GameMap.getInstance().get(x, y);
		Stack<Node> tempPath = Pathfinder.getInstance().findPath(
				GameMap.getInstance().xSpawn, GameMap.getInstance().ySpawn, x,
				y, true);
		if (tempPath == null) {
			isReachable = false;
		}
	}

	@Override
	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Override
	public void removeUnit() {
		this.unit = null;

	}

	@Override
	public boolean execute() {
		// go to the food
		if((tile.construct instanceof FishingHole) && !unit.hasItem(InventoryItem.FishingRod)){
			return true;
		}
		if (!hasGathered) {
			unit.setState(Unit.UnitState.Gathering);
			gatherFood();
			return false;
		} else {
			if (unit.inventoryIsFull()) {
				unit.addTask(new DepositFoodTask(this.unit));
				return true;
			} else if(appleAmount > 0){
				unit.addTask(new GatherFoodTask(x,y));
				return true;
			}
			else {
				return true;
			}
		}
	}

	private void gatherFood() {
		// if there is no path to the resource, get a path
		if (currentPath == null || currentPath.isEmpty()
				|| currentPath.size() == 0) {
			currentPath = Pathfinder.getInstance().findPath(
					unit.getLocation().x, unit.getLocation().y, x, y, true);
			if (currentPath != null && currentPath.size() > 0) {
				// pop the starting point
				currentPath.pop();
				hasGathered = false;
			} else
				hasGathered = true;
		}
		// the food is directly in front of them
		else if (currentPath.size() == 1) {
			// pop stack to make it empty
			currentPath.pop();
			// check to make sure it is harvestable

			if (tile.construct != null && tile.construct instanceof Harvestable) {
				// get the harvestable resource
				Harvestable resource = (Harvestable) tile.construct;

				// harvest the resource into array
				Food[] harvestedFood = (Food[]) resource.harvest(1);
				appleAmount = resource.getQuantityRemaining();
				// these are the items we will add to the units inventory
				LinkedList<Item> itemsToAdd = new LinkedList<Item>();
				if (harvestedFood != null && harvestedFood.length != 0) {
					for (int i = 0; i < harvestedFood.length; i++) {
						itemsToAdd.add(harvestedFood[i]);
					}
					// add the items to unit inventory
					for (int i = 0; i < itemsToAdd.size(); i++) {
						unit.addItem(itemsToAdd.get(i));
					}
				}
			}
			hasGathered = true;

		}
		// otherwise, continue along path
		else {
			// get coordinates of next move, update units location
			Node nextMove = currentPath.pop();
			Point nextCoords = nextMove.coords;
			unit.setLocation(nextCoords);
			unit.updateLightEmitter(nextCoords);
			hasGathered = false;
		}
	}

	@Override
	public boolean isReachable() {
		if(GameMap.getInstance().get(x-1, y).isWalkable() ||
				GameMap.getInstance().get(x+1, y).isWalkable() ||
				GameMap.getInstance().get(x, y+1).isWalkable() ||
				GameMap.getInstance().get(x, y - 1).isWalkable()){
			isReachable = true;
		}
		else{
			isReachable = false;
		}

		return isReachable;
	}

}
