package model.Units.Tasks;

import java.awt.Point;
import java.util.Stack;

import model.GameMap;
import model.Pathfinder;
import model.TerrainTile;
import model.Node;
import model.Units.Unit;
import model.goals.DepositFoodGoal;

/*
 * Gets food from selected tile and deposits it in the food container, if there is
 * a container built
 */

public class EatFoodTask implements UnitTask {

	protected TerrainTile tile = null;
	protected int x, y;
	protected Unit unit = null;
	protected Stack<Node> currentPath = null;
	private boolean hasEaten = false;

	public EatFoodTask(int x, int y) {
		this.x = x;
		this.y = y;
		tile = GameMap.getInstance().get(x, y);
	}
	
	public EatFoodTask(Unit unit){
		this.unit = unit;
		currentPath = Pathfinder.getInstance().findGoal(
				unit.getLocation().x, unit.getLocation().y, new DepositFoodGoal());
	}

	@Override
	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Override
	public void removeUnit() {
		this.unit = null;

	}

	@Override
	public boolean execute() {
		if (!hasEaten) {
			eatFood();
			return false;
		} else
			return true;
	}

	private void eatFood() {
		if (currentPath == null || currentPath.isEmpty()
				|| currentPath.size() == 0) {

			currentPath = Pathfinder.getInstance().findGoal(
					unit.getLocation().x, unit.getLocation().y,
					new DepositFoodGoal());
			if (currentPath != null && currentPath.size() > 0) {
				// pop the starting point
				currentPath.pop();
			}
		} 
		else if (currentPath.size() == 1) {
			// get the tile with the container
			unit.comsumeFood();
			hasEaten = true;
		} 
		else {
			// get coordinates of next move, update units location
			Node nextMove = currentPath.pop();
			Point nextCoords = nextMove.coords;
			unit.setLocation(nextCoords);
			hasEaten = false;
		}
	}

	@Override
	public boolean isReachable() {
		return true;
	}
}
