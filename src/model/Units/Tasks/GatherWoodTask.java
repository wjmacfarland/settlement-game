package model.Units.Tasks;

import java.awt.Point;
import java.util.LinkedList;
import java.util.Stack;

import model.GameMap;
import model.Pathfinder;
import model.TerrainTile;
import model.Node;
import model.Units.Unit;
import model.Units.Unit.InventoryItem;
import model.constructs.Tree;
import model.goals.FindWorkbenchGoal;
import model.resource.Axe;
import model.resource.Item;
import model.resource.Wood;

public class GatherWoodTask implements UnitTask {

	protected TerrainTile tile = null;
	protected int x, y;
	protected Unit unit = null;
	protected Stack<Node> currentPath = null;
	private boolean hasGathered = false;
	private boolean hasNotUpdated = true;
	private int woodAmount = 0;
	private boolean isReachable = true;
	private boolean workbenchExists;
	private boolean woodshedExists = false;
	private Tree resource;

	public GatherWoodTask(int x, int y) {
		this.x = x;
		this.y = y;
		tile = GameMap.getInstance().get(x, y);
		resource = (Tree) tile.construct;

		Stack<Node> tempPath = Pathfinder.getInstance().findPath(
				GameMap.getInstance().xSpawn, GameMap.getInstance().ySpawn, x,
				y, true);
		if (tempPath == null) {
			isReachable = false;
		}
		tempPath = Pathfinder.getInstance().findGoal(
				GameMap.getInstance().xSpawn, GameMap.getInstance().ySpawn,
				new FindWorkbenchGoal());
		if (tempPath == null) {
			workbenchExists = false;
		} else
			workbenchExists = true;
	}

	@Override
	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Override
	public void removeUnit() {
		this.unit = null;

	}

	@Override
	public boolean execute() {
		if (workbenchExists && !unit.hasItem(InventoryItem.Axe) && unit.hasWood(3)) {
			unit.setState(Unit.UnitState.Gathering);
			getToWorkbench();
			return false;
		} else if (!hasGathered) {
			unit.setState(Unit.UnitState.Gathering);
			gatherWood();
			return false;
		} else {
			if (unit.hasItem(InventoryItem.Axe)) {
				unit.getAxe().use();
				if (!(unit.getAxe().usable())) {
					unit.removeAxe();
				}
			}
			if (unit.inventoryIsFull()) {
				if (woodAmount > 0)
					unit.addTask(new GatherWoodTask(x, y));
				unit.addTask(new DepositWoodTask(this.unit));
				return true;
			} else if (woodAmount > 0) {
				unit.addTask(new GatherWoodTask(x, y));
				return true;
			}

			else {
				return true;
			}
		}
	}

	private void getToWorkbench() {
		// if there is no path to the resource, get a path
		if (currentPath == null || currentPath.isEmpty()
				|| currentPath.size() == 0) {
			currentPath = Pathfinder.getInstance().findGoal(
					unit.getLocation().x, unit.getLocation().y,
					new FindWorkbenchGoal());
		}
		// the food is directly in front of them
		else if (currentPath.size() == 1) {
			// pop stack to make it empty
			currentPath.pop();
			// check to make sure it is harvestable

			unit.depositWood();
			unit.depositWood();
			unit.depositWood();
			unit.addItem(new Axe());
			currentPath = null;
		}
		// otherwise, continue along path
		else {
			// get coordinates of next move, update units location
			Node nextMove = currentPath.pop();
			Point nextCoords = nextMove.coords;
			unit.setLocation(nextCoords);
			unit.updateLightEmitter(nextCoords);
		}

	}

	private void gatherWood() {
		// if there is no path to the resource, get a path
		if (currentPath == null || currentPath.isEmpty()
				|| currentPath.size() == 0) {
			currentPath = Pathfinder.getInstance().findPath(
					unit.getLocation().x, unit.getLocation().y, x, y, true);
			if (currentPath != null && currentPath.size() > 0) {
				// pop the starting point
				currentPath.pop();
				hasGathered = false;
			} else {
				hasGathered = true;
			}
		}
		// the food is directly in front of them
		else if (currentPath.size() == 1) {
			// pop stack to make it empty
			currentPath.pop();
			// check to make sure it is harvestable
			if (tile.construct != null && tile.construct instanceof Tree) {
				resource = (Tree) tile.construct;
				// harvest the resource into array
				Wood[] harvestedWood = resource.getWood();
				woodAmount = resource.getWoodQuantity();
				// these are the items we will add to the units inventory
				LinkedList<Item> itemsToAdd = new LinkedList<Item>();
				if (harvestedWood != null && harvestedWood.length != 0) {
					for (int i = 0; i < harvestedWood.length; i++) {
						itemsToAdd.add(harvestedWood[i]);
					}
					// add the items to unit inventory
					for (int i = 0; i < itemsToAdd.size(); i++) {
						unit.addItem(itemsToAdd.get(i));
					}
				}
			}
			hasGathered = true;
		}
		// otherwise, continue along path
		else {
			// get coordinates of next move, update units location
			Node nextMove = currentPath.pop();
			Point nextCoords = nextMove.coords;
			unit.setLocation(nextCoords);
			unit.updateLightEmitter(nextCoords);
			hasGathered = false;
		}

	}

	public Stack<Node> getCurrentPath() {
		return currentPath;
	}

	@Override
	public boolean isReachable() {
		if (GameMap.getInstance().get(x - 1, y).isWalkable()
				|| GameMap.getInstance().get(x + 1, y).isWalkable()
				|| GameMap.getInstance().get(x, y + 1).isWalkable()
				|| GameMap.getInstance().get(x, y - 1).isWalkable()) {
			isReachable = true;
		} else {
			isReachable = false;
		}

		return isReachable;
	}

}
