package model.Units.Tasks;

import java.awt.Point;
import java.util.Stack;

import model.GameMap;
import model.Pathfinder;
import model.TerrainTile;
import model.Node;
import model.Units.Unit;
import model.Units.UnitTaskCollection;
import model.constructs.Campfire;
import model.constructs.Construct;
import view.lighting.BasicLightRadiusStrategy;
import view.lighting.LightEmitter;
import view.lighting.LightingEngine;

public class PlotCampfireTask implements UnitTask {

	protected TerrainTile tile = null;
	protected int x, y;
	protected Unit unit = null;
	protected Stack<Node> currentPath = null;
	protected boolean placedPlot = false;
	protected Campfire toBuild;
	private boolean hasNotUpdated = true;

	public PlotCampfireTask(int x, int y) {
		this.x = x;
		this.y = y;
		tile = GameMap.getInstance().get(x, y);

	}

	@Override
	public boolean execute() {
		if (!placedPlot) {
			unit.setState(Unit.UnitState.Building);
			placePlot();
			return false;
		} else {
			Construct construct = tile.getConstruct();
			if (construct instanceof Campfire)
				toBuild = (Campfire) construct;
			
				UnitTaskCollection.getInstance().addBuildingTask(new BuildPlotTask(x, y));
			
			return true;
		}
	}

	private void placePlot() {
		// get a path to desired plot spot
		if (currentPath == null || currentPath.isEmpty() || currentPath.size() == 0) {
			currentPath = Pathfinder.getInstance().findPath(unit.getLocation().x, unit.getLocation().y, x, y);
			if (currentPath != null && currentPath.size() > 0) {
				// pop the starting point
				currentPath.pop();
			}
			placedPlot = false;
		}
		// if standing next to desired tile
		else if (currentPath.size() == 1) {
			// pop last element from stack
			currentPath.pop();

			if (tile.isValidConstructionSite()) {
				Campfire campfire = new Campfire();
				campfire.emitter = new LightEmitter(new Point(x, y), 12, BasicLightRadiusStrategy.CIRCLE);
				LightingEngine.lightEmitters.add(campfire.emitter);
				tile.construct = campfire;
			}
			placedPlot = true;
		}
		// otherwise, continue along path
		else {
			// get coordinates of next move, update units location
			Node nextMove = currentPath.pop();
			Point nextCoords = nextMove.coords;
			unit.setLocation(nextCoords);
			unit.updateLightEmitter(nextCoords);
			placedPlot = false;
		}

	}

	@Override
	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Override
	public void removeUnit() {
		this.unit = null;
	}

	@Override
	public boolean isReachable() {
		return true;
	}

}
