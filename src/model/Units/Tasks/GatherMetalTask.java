package model.Units.Tasks;

import java.awt.Point;
import java.util.Stack;

import model.GameMap;
import model.Pathfinder;
import model.TerrainTile;
import model.Node;
import model.Units.Unit;
import model.constructs.Harvestable;
import model.constructs.Ore;
import model.goals.DepositMetalGoal;
import model.resource.Item;

/*
 * Gets food from selected tile and deposits it in the food container, if there is
 * a container built
 */

public class GatherMetalTask implements UnitTask {

	protected TerrainTile tile = null;
	protected int x, y;
	protected Unit unit = null;
	protected Stack<Node> currentPath = null;
	private boolean hasGathered = false;
	private int appleAmount;
	private boolean isReachable = true;
	private boolean metalStorageExists;

	public GatherMetalTask(int x, int y) {
		this.x = x;
		this.y = y;
		tile = GameMap.getInstance().get(x, y);
		Stack<Node> tempPath = Pathfinder.getInstance().findPath(
				GameMap.getInstance().xSpawn, GameMap.getInstance().ySpawn, x,
				y, true);
		if (tempPath == null) {
			isReachable = false;
		}
		tempPath = Pathfinder.getInstance().findGoal(
				GameMap.getInstance().xSpawn, GameMap.getInstance().ySpawn,
				new DepositMetalGoal());
		if (tempPath == null) {
			metalStorageExists = false;
		} else
			metalStorageExists = true;
	}

	@Override
	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Override
	public void removeUnit() {
		this.unit = null;

	}

	@Override
	public boolean execute() {

		if (!hasGathered) {
			unit.setState(Unit.UnitState.Gathering);
			gatherMetal();

			return false;
		} else {
			if (unit.inventoryIsFull()) {
				unit.addTask(new DepositMetalTask(this.unit));
				return true;
			}
			return true;
		}
	}

	private void gatherMetal() {
		// if there is no path to the resource, get a path
		if (currentPath == null || currentPath.isEmpty()
				|| currentPath.size() == 0) {

			currentPath = Pathfinder.getInstance().findPath(
					unit.getLocation().x, unit.getLocation().y, x, y, true);
			if (currentPath != null && currentPath.size() > 0) {
				// pop the starting point
				currentPath.pop();
				hasGathered = false;
			} else
				hasGathered = true;
		}
		// the metal is directly in front of them
		else if (currentPath.size() == 1) {

			// pop stack to make it empty
			currentPath.pop();
			// check to make sure it is harvestable

			if (tile.construct != null && tile.construct instanceof Harvestable) {
				// get the harvestable resource
				Harvestable resource = (Harvestable) tile.construct;

				if (resource instanceof Ore) {
					Item toAdd = ((Ore) resource).breakRock();
					if (toAdd != null) {
						unit.addItem(toAdd);
					}
				}
				hasGathered = true;

			}
			// otherwise, continue along path
		} 
		else {
			// get coordinates of next move, update units location
			Node nextMove = currentPath.pop();
			Point nextCoords = nextMove.coords;
			unit.setLocation(nextCoords);
			unit.updateLightEmitter(nextCoords);
			hasGathered = false;
		}
	}

	@Override
	public boolean isReachable() {
		if (GameMap.getInstance().get(x - 1, y).isWalkable()
				|| GameMap.getInstance().get(x + 1, y).isWalkable()
				|| GameMap.getInstance().get(x, y + 1).isWalkable()
				|| GameMap.getInstance().get(x, y - 1).isWalkable()) {
			isReachable = true;
		} else {
			isReachable = false;
		}

		return isReachable;
	}

}
