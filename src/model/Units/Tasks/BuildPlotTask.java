package model.Units.Tasks;

import java.awt.Point;
import java.util.Stack;

import model.GameMap;
import model.Pathfinder;
import model.TerrainTile;
import model.Node;
import model.Units.Unit;
import model.constructs.Plottable;
import model.constructs.Construct;
import model.resource.Wood;

public class BuildPlotTask implements UnitTask {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected TerrainTile tile = null;
	protected int x, y;
	protected Unit unit = null;
	protected Stack<Node> currentPath = null;
	protected boolean woodPlaced = false;
	protected Plottable toBuild;

	public BuildPlotTask(int x, int y) {
		this.x = x;
		this.y = y;
		tile = GameMap.getInstance().get(x, y);
		Construct construct = tile.getConstruct();
		if (construct instanceof Plottable)
			toBuild = (Plottable) construct;
	}

	@Override
	public boolean execute() {
		if (!woodPlaced) {
			unit.setState(Unit.UnitState.Building);
			placeWood();
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Override
	public void removeUnit() {
		this.unit = null;
	}

	private void placeWood() {
		// get a path to desired plot spot
		if (currentPath == null || currentPath.isEmpty()
				|| currentPath.size() == 0) {
			currentPath = Pathfinder.getInstance().findPath(
					unit.getLocation().x, unit.getLocation().y, x, y);
			if (currentPath != null && currentPath.size() > 0) {
				// pop the starting point
				currentPath.pop();
			}
			woodPlaced = false;
		} else if (currentPath.size() == 1) {
			// pop last element from stack
			currentPath.pop();

			int count = 0;
			while (count < toBuild.woodNeededToBuild()) {
				toBuild.addMaterial(new Wood());
				count++;
			}
			woodPlaced = true;
		}
		// otherwise, continue along path
		else {
			// get coordinates of next move, update units location
			Node nextMove = currentPath.pop();
			Point nextCoords = nextMove.coords;
			unit.setLocation(nextCoords);
			unit.updateLightEmitter(nextCoords);
			woodPlaced = false;
		}
	}

	private void moveToClosestWalkableTile() {
		if (GameMap.getInstance().get(x - 1, y).isWalkable()) {
			unit.setLocation(new Point(x - 1, y));
		} else if (GameMap.getInstance().get(x + 1, y).isWalkable()) {
			unit.setLocation(new Point(x + 1, y));
		} else if (GameMap.getInstance().get(x, y + 1).isWalkable()) {
			unit.setLocation(new Point(x, y + 1));

		} else {
			unit.setLocation(new Point(x, y - 1));

		}

	}

	@Override
	public boolean isReachable() {
		return true;
	}
}
