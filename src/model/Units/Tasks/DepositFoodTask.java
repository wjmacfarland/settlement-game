package model.Units.Tasks;

import java.awt.Point;
import java.util.Stack;

import model.GameMap;
import model.Pathfinder;
import model.TerrainTile;
import model.Node;
import model.Units.Unit;
import model.Units.Unit.InventoryItem;
import model.constructs.FoodContainer;
import model.goals.DepositFoodGoal;

/*
 * Gets food from selected tile and deposits it in the food container, if there is
 * a container built
 */

public class DepositFoodTask implements UnitTask {

	protected TerrainTile tile = null;
	protected int x, y;
	protected Unit unit = null;
	protected Stack<Node> currentPath = null;
	private boolean hasDeposited = false;
	private boolean hasNotUpdated = true;


	public DepositFoodTask(int x, int y) {
		this.x = x;
		this.y = y;
		tile = GameMap.getInstance().get(x, y);
	}
	
	public DepositFoodTask(Unit unit){
		this.unit = unit;
		currentPath = Pathfinder.getInstance().findGoal(
				unit.getLocation().x, unit.getLocation().y, new DepositFoodGoal());
	}

	@Override
	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Override
	public void removeUnit() {
		this.unit = null;

	}

	@Override
	public boolean execute() {
		if (!hasDeposited) {
			unit.setState(Unit.UnitState.Gathering);
			depositFood();
			return false;
		} else
			return true;
	}

	private void depositFood() {
		if (currentPath == null || currentPath.isEmpty()
				|| currentPath.size() == 0) {

			currentPath = Pathfinder.getInstance().findPath(
					unit.getLocation().x, unit.getLocation().y,
					x, y, true);
			if (currentPath != null && currentPath.size() > 0) {
				// pop the starting point
				currentPath.pop();
			}
			else
				hasDeposited = true;
		} 
		else if (currentPath.size() == 1) {
			// get the tile with the container
			Node buildingLoc = currentPath.pop();
			Point buildingCoords = buildingLoc.coords;
			TerrainTile tile = GameMap.getInstance().get(buildingCoords.x,
					buildingCoords.y);

			//
			if (tile.construct != null
					&& tile.construct instanceof FoodContainer) {
				FoodContainer container = (FoodContainer) tile.construct;

				while (unit.hasItem(InventoryItem.Food)) {
					container.depositFood(unit.depositFood());
				}
			}
			hasDeposited = true;
		} 
		else {
			// get coordinates of next move, update units location
			Node nextMove = currentPath.pop();
			Point nextCoords = nextMove.coords;
			unit.setLocation(nextCoords);
			unit.updateLightEmitter(nextCoords);
			hasDeposited = false;
		}
	}

	@Override
	public boolean isReachable() {
		return true;
	}
}
