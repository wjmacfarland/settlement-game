package model.Units.Tasks;

import java.awt.Point;
import java.util.Stack;

import model.GameMap;
import model.Pathfinder;
import model.TerrainTile;
import model.Node;
import model.Units.Unit;
import model.Units.Unit.InventoryItem;
import model.constructs.MetalStorage;
import model.goals.DepositMetalGoal;

/*
 * Gets food from selected tile and deposits it in the food container, if there is
 * a container built
 */

public class DepositMetalTask implements UnitTask {

	protected TerrainTile tile = null;
	protected int x, y;
	protected Unit unit = null;
	protected Stack<Node> currentPath = null;
	private boolean hasDeposited = false;


	public DepositMetalTask(int x, int y) {
		this.x = x;
		this.y = y;
		tile = GameMap.getInstance().get(x, y);
	}
	
	public DepositMetalTask(Unit unit){
		this.unit = unit;
		currentPath = Pathfinder.getInstance().findGoal(
				unit.getLocation().x, unit.getLocation().y, new DepositMetalGoal());
	}

	@Override
	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Override
	public void removeUnit() {
		this.unit = null;

	}

	@Override
	public boolean execute() {
		if (!hasDeposited) {
			unit.setState(Unit.UnitState.Gathering);
			depositMetal();
			return false;
		} else
			return true;
	}

	private void depositMetal() {
		if (currentPath == null || currentPath.isEmpty()
				|| currentPath.size() == 0) {

			currentPath = Pathfinder.getInstance().findPath(
					unit.getLocation().x, unit.getLocation().y,
					x, y, true);
			if (currentPath != null && currentPath.size() > 0) {
				// pop the starting point
				currentPath.pop();
			}
			else
				hasDeposited = true;
		} 
		else if (currentPath.size() == 1) {
			// get the tile with the container
			Node buildingLoc = currentPath.pop();
			Point buildingCoords = buildingLoc.coords;
			TerrainTile tile = GameMap.getInstance().get(buildingCoords.x,
					buildingCoords.y);

			//
			if (tile.construct != null
					&& tile.construct instanceof MetalStorage) {
				MetalStorage container = (MetalStorage) tile.construct;

				while (unit.hasItem(InventoryItem.Metal)) {
					container.depositMetal(unit.depositMetal());
				}
			}
			hasDeposited = true;
		} 
		else {
			// get coordinates of next move, update units location
			Node nextMove = currentPath.pop();
			Point nextCoords = nextMove.coords;
			unit.setLocation(nextCoords);
			unit.updateLightEmitter(nextCoords);
			hasDeposited = false;
		}
	}

	@Override
	public boolean isReachable() {
		return true;
	}
}
