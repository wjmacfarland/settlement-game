package model.Units.Tasks;

import java.awt.Point;
import java.util.Stack;

import model.GameMap;
import model.Pathfinder;
import model.TerrainTile;
import model.Node;
import model.Units.Unit;
import model.constructs.Construct;
import model.constructs.HouseWall;
import model.constructs.WorkshopWall;

public class PlotWorkshopTask implements UnitTask {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected TerrainTile tile = null;
	protected int x, y;
	protected Unit unit = null;
	protected Stack<Node> currentPath = null;
	protected boolean placedPlot = false;
	protected HouseWall toBuild;

	public PlotWorkshopTask(int x, int y) {
		this.x =
				x;
		this.y = y;
		tile = GameMap.getInstance().get(x, y);
	}

	@Override
	public boolean execute() {
		if (!placedPlot) {
			unit.setState(Unit.UnitState.Building);
			placePlot();
			return false;
		} else {
			Construct construct = tile.getConstruct();
			if (construct instanceof HouseWall)
				toBuild = (HouseWall) construct;
				//UnitTaskCollection.getInstance().addBuildingTask(new BuildPlotTask(x, y));
			}
		return true;
		
	}

	private void placePlot() {
		// get a path to desired plot spot
		if((unit.getLocation().x == x) && (unit.getLocation().y == y)){
			moveToClosestWalkableTile();
			if (tile.isValidConstructionSite()) {
				tile.construct = new WorkshopWall();
			}
			placedPlot = true;
		}
		
		else if (currentPath == null || currentPath.isEmpty() ) {
			currentPath = Pathfinder.getInstance().findPath(unit.getLocation().x, unit.getLocation().y, x, y);
			if (currentPath != null && currentPath.size() > 0) {
				// pop the starting point
				currentPath.pop();
				placedPlot = false;
			}
			else{
				placedPlot = true;
			}
			
		}
		// if standing next to desired tile
		else if (currentPath.size() == 1 || currentPath.size() == 0) {
			
			// pop last element from stack
			//currentPath.pop();

			if (tile.isValidConstructionSite()) {
				tile.construct = new WorkshopWall();
			}
			placedPlot = true;
		}
		// otherwise, continue along path
		else {
			// get coordinates of next move, update units location
			Node nextMove = currentPath.pop();
			Point nextCoords = nextMove.coords;
			unit.setLocation(nextCoords);
			unit.updateLightEmitter(nextCoords);
			placedPlot = false;
		}

	}

	private void moveToClosestWalkableTile() {
		if (GameMap.getInstance().get(x - 1, y).isWalkable()){
			unit.setLocation(new Point (x-1, y));
		}else if(GameMap.getInstance().get(x + 1, y).isWalkable()){
			unit.setLocation(new Point (x+1, y));
		}
		else if (GameMap.getInstance().get(x, y + 1).isWalkable()){
			unit.setLocation(new Point (x, y+1));

		}else{
			unit.setLocation(new Point (x, y-1));

		}
		
	}

	@Override
	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Override
	public void removeUnit() {
		this.unit = null;
	}

	@Override
	public boolean isReachable() {
		return true;
	}
	


}
