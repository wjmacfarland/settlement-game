package model.Units.Tasks;

import java.io.Serializable;

import model.Units.Unit;

public interface UnitTask extends Serializable {

	// returns true when task has been completed
	public boolean execute();

	// assigns task to move a specific unit
	public void setUnit(Unit unit);

	// unit must be removed if task is completed
	public void removeUnit();

	public boolean isReachable();

}
