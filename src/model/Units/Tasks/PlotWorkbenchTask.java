package model.Units.Tasks;

import java.awt.Point;
import java.util.Stack;

import model.GameMap;
import model.Pathfinder;
import model.TerrainTile;
import model.Node;
import model.Units.Unit;
import model.Units.UnitTaskCollection;
import model.constructs.Construct;
import model.constructs.Workbench;

public class PlotWorkbenchTask implements UnitTask {

	protected TerrainTile tile = null;
	protected int x, y;
	protected Unit unit = null;
	protected Stack<Node> currentPath = null;
	protected boolean placedPlot = false;
	protected Workbench toBuild;
	private boolean hasNotUpdated = true;


	public PlotWorkbenchTask(int x, int y) {
		this.x = x;
		this.y = y;
		tile = GameMap.getInstance().get(x, y);
		
	}

	@Override
	public boolean execute() {
		if (!placedPlot) {
			unit.setState(Unit.UnitState.Building);
			placePlot();
			return false;
		} else {
			Construct construct = tile.getConstruct();
			if (construct instanceof Workbench)
				toBuild = (Workbench) construct;
			
				UnitTaskCollection.getInstance().addBuildingTask(
						new BuildPlotTask(x, y));
			
			return true;
		}
	}

	private void placePlot() {
		// get a path to desired plot spot
		if (currentPath == null || currentPath.isEmpty()
				|| currentPath.size() == 0) {
			currentPath = Pathfinder.getInstance().findPath(
					unit.getLocation().x, unit.getLocation().y, x, y);
			if (currentPath != null && currentPath.size() > 0) {
				// pop the starting point
				currentPath.pop();
			}
			placedPlot = false;
		}
		// if standing next to desired tile
		else if (currentPath.size() == 1) {
			// pop last element from stack
			currentPath.pop();

			if (tile.isValidConstructionSite()) {
				tile.construct = new Workbench();
			}
			placedPlot = true;
		}
		// otherwise, continue along path
		else {
			// get coordinates of next move, update units location
			Node nextMove = currentPath.pop();
			Point nextCoords = nextMove.coords;
			unit.setLocation(nextCoords);
			unit.updateLightEmitter(nextCoords);
			placedPlot = false;
		}

	}

	@Override
	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Override
	public void removeUnit() {
		this.unit = null;
	}

	@Override
	public boolean isReachable() {
		return true;
	}

}
