package model.Units.Tasks;

import java.awt.Point;
import java.util.Stack;

import model.GameMap;
import model.Pathfinder;
import model.TerrainTile;
import model.Node;
import model.Units.Unit;
import model.constructs.Bed;
import model.goals.FindBedGoal;


public class SleepTask implements UnitTask {

	protected TerrainTile tile = null;
	protected int x, y;
	protected Unit unit = null;
	protected Stack<Node> currentPath = null;
	private boolean hasSlept = false;
	private boolean hasNotUpdated = true;


	public SleepTask(int x, int y) {
		this.x = x;
		this.y = y;
		tile = GameMap.getInstance().get(x, y);
	}
	
	public SleepTask(Unit unit){
		this.unit = unit;
		currentPath = Pathfinder.getInstance().findGoal(
				unit.getLocation().x, unit.getLocation().y, new FindBedGoal());
	}

	@Override
	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Override
	public void removeUnit() {
		this.unit = null;

	}

	@Override
	public boolean execute() {
		if (!hasSlept) {
			if (hasNotUpdated) {
				hasNotUpdated = false;
			}
			unit.setState(Unit.UnitState.Sleeping);
			sleep();
			return false;
		} else
			unit.setState(Unit.UnitState.Wandering);
			return true;
	}

	private void sleep() {
		if (currentPath == null || currentPath.isEmpty()
				|| currentPath.size() == 0) {

			currentPath = Pathfinder.getInstance().findPath(
					unit.getLocation().x, unit.getLocation().y,
					x, y, true);
			if (currentPath != null && currentPath.size() > 0) {
				// pop the starting point
				currentPath.pop();
			}
			else
				hasSlept = true;
		} 
		else if (currentPath.size() == 1) {
			// get the tile with the container
			Node buildingLoc = currentPath.pop();
			Point buildingCoords = buildingLoc.coords;
			TerrainTile tile = GameMap.getInstance().get(buildingCoords.x,
					buildingCoords.y);

			//Sleep if there is a bed there (there should be at this point
			if (tile.construct != null
					&& tile.construct instanceof Bed) {
				unit.sleep();
			}
			if(unit.getFatigue() < 100){
				hasSlept = false;
			}
			else{
				hasSlept = true;
			}
		} 
		else {
			// get coordinates of next move, update units location
			Node nextMove = currentPath.pop();
			Point nextCoords = nextMove.coords;
			unit.setLocation(nextCoords);
			hasSlept = false;
		}
	}

	@Override
	public boolean isReachable() {
		return true;
	}
}
