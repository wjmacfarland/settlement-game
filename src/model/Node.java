package model;

import java.awt.Point;
import java.io.Serializable;

public class Node implements Comparable<Node>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Point coords;
	public int g, h;
	public Node previousNode;
	public boolean open;

	Node(int x, int y, int g) {
		this.coords = new Point(x, y);
		this.g = g;
	}

	public void setPrevious(Node previousNode) {
		this.previousNode = previousNode;
	}

	@Override
	public int compareTo(Node o) {
		return (g + h) - (o.g + o.h);
	}

	// Check if two nodes refer to the same TerrainTile
	public boolean equalsNode(Node o) {
		if (this.coords.x == o.coords.x && this.coords.y == o.coords.y)
			return true;
		return false;
	}

	public TerrainTile correspondingTile() {
		return GameMap.getInstance().get(this.coords.x, this.coords.y);
	}
}