package model;

import java.io.Serializable;
import java.util.LinkedList;

import model.resource.Wood;

public class WoodCollection implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static WoodCollection instance = null;

	// This might be able to be an int. I do not know what's going to work out
	// this early on
	private LinkedList<Wood> woodStash;

	public static WoodCollection getInstance() {
		if (instance == null) {
			instance = new WoodCollection();
		}
		return instance;
	}

	public WoodCollection() {
		woodStash = new LinkedList<Wood>();
		for(int i = 0; i < 30; i++){
		add(new Wood());
		}
	}
	
	public void clear(){
		woodStash.clear();
		for(int i = 0; i < 30; i++){
			add(new Wood());
			}
	}

	public synchronized boolean add(Wood woodToAdd) {
		return woodStash.add(woodToAdd);
	}

	public synchronized Wood get() {
		return woodStash.remove();
	}
	public synchronized void get(int amount) {
		for(int i = 0; i < amount; i++){
			woodStash.remove();
		}
	}

	public int size() {
		return woodStash.size();
	}

	public boolean isEmpty() {
		return woodStash.isEmpty();
	}

	@Override
	public String toString() {
		String toReturn = "" + this.size();
		return toReturn;
	}
	
	public  LinkedList<Wood> getWoodForSaving(){
		return woodStash;
	}
	
	public void loadWood(LinkedList<Wood> obj){
		woodStash = obj;
	}
	

}
