package model;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.geom.Rectangle;

public class Zone {

	public class TileWrapper {
		public final TerrainTile tile;
		public final int x, y;

		TileWrapper(TerrainTile tile, int x, int y) {
			this.tile = tile;
			this.x = x;
			this.y = y;
		}
	}

	public int w;
	public int h;
	protected Rectangle zone;

	public Zone(int x, int y, int w, int h) {
		this.zone = new Rectangle(x, y, w, h);
		this.w = w;
		this.h = h;
	}

	public Zone(Rectangle rectangle) {
		
		this.zone = rectangle;
	}

	public Rectangle getRectangle() {
		return zone;
	}

	public List<TileWrapper> getTiles() {
		List<TileWrapper> tiles = new LinkedList<TileWrapper>();
		for (int i = (int) zone.getX(); i < (int) (zone.getX() + zone.getWidth()); i++) {
			for (int j = (int) zone.getY(); j < (int) (zone.getY() + zone.getHeight()); j++) {
				// Wrap the tile along with the x and y location that the tile
				// was at.
				TileWrapper tile = new TileWrapper(GameMap.getInstance().get(i, j), i, j);
				if (tile != null) {
					tiles.add(tile);
				}
			}
		}
		return tiles;
	}

	// Return true if x and y are within the zone (taking into account tile size
	// and camera positioning)
	public boolean isClickInZone(Point camera, int x, int y) {
		Rectangle clickRect = new Rectangle((x + camera.x) / 32 + 0.25f, (y + camera.y) / 32 + 0.25f, .25f,
				.25f);
		if (clickRect.intersects(zone)) {
			return true;
		}
		return false;
	}

	// Return inner zone
	public Zone getInnerZone() {
		int innerX = (int) (zone.getX() + 1);
		int innerY = (int) (zone.getY() + 1);
		int innerW = (int) (zone.getWidth() - 2);
		int innerH = (int) (zone.getHeight() - 2);
		if (innerH <= 0 || innerW <= 0) {
			return null;
		} else {
			return new Zone(innerX, innerY, innerW, innerH);
		}
	}
}
