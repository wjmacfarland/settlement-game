package model;

import java.util.LinkedList;

import model.resource.Food;

public class FoodCollection {

	private static FoodCollection instance = null;

	// This might be able to be an int. I do not know what's going to work out
	// this early on
	private LinkedList<Food> foodStash;

	public static FoodCollection getInstance() {
		if (instance == null) {
			instance = new FoodCollection();
		}
		return instance;
	}

	public FoodCollection() {
		foodStash = new LinkedList<Food>();
	}

	public synchronized boolean add(Food foodToAdd) {
		return foodStash.add(foodToAdd);
	}

	public synchronized Food get() {
		return foodStash.remove();
	}

	public int size() {
		return foodStash.size();
	}

	public boolean isEmpty() {
		return foodStash.isEmpty();
	}
	
	public void clear(){
		foodStash.clear();
	}


	@Override
	public String toString() {
		String toReturn = "" + this.size();
		return toReturn;
	}
	
	public  LinkedList<Food> getFoodForSaving(){
		return foodStash;
	}
	
	public void loadFood(LinkedList<Food> obj){
		foodStash = obj;
	}
}
