package model.goals;

import model.TerrainTile;
import model.constructs.Tree;

public final class TreeGoal implements PathfinderGoal {

	@Override
	public boolean isThisTileGoal(TerrainTile tile) {
		if (tile.construct != null && tile.construct instanceof Tree) {
			return true;
		}
		return false;
	}

}
