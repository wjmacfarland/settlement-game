package model.goals;

import model.TerrainTile;
import model.constructs.Campfire;

public class FindWarmthGoal implements PathfinderGoal {

	@Override
	public boolean isThisTileGoal(TerrainTile tile) {
		if (tile.construct != null && tile.construct instanceof Campfire) {
			Campfire building = (Campfire) tile.construct;
			if (!building.isPlot()) {
				return true;
			}
		}
		return false;
	}

}
