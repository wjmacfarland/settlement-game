package model.goals;

import model.TerrainTile;
import model.constructs.Harvestable;

public final class HarvestGoal implements PathfinderGoal {

	@Override
	public boolean isThisTileGoal(TerrainTile tile) {
		if (tile.construct != null && tile.construct instanceof Harvestable) {
			Harvestable harvestableTile = (Harvestable) tile.construct;
			if (harvestableTile.getQuantityRemaining() > 0) {
				return true;
			}
		}
		return false;
	}

}
