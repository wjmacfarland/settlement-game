package model.goals;

import model.TerrainTile;
import model.constructs.Workbench;

public class FindWorkbenchGoal implements PathfinderGoal {

	@Override
	public boolean isThisTileGoal(TerrainTile tile) {
		if (tile.construct != null && tile.construct instanceof Workbench) {
			Workbench building = (Workbench) tile.construct;
			if (!building.isPlot()) {
				return true;
			}
		}
		return false;
	}


}
