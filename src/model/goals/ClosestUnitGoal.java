package model.goals;

import model.TerrainTile;
import model.Units.Unit;
import model.Units.UnitCollection;

public final class ClosestUnitGoal implements PathfinderGoal {

	@Override
	public boolean isThisTileGoal(TerrainTile tile) {
		for (Unit unit : UnitCollection.getInstance()) {
			if (unit.getTileBeneath() == tile) {
				return true;
			}
		}
		return false;
	}

}