package model.goals;

import model.TerrainTile;
import model.constructs.WoodBin;

public final class DepositWoodGoal implements PathfinderGoal {

	@Override
	public boolean isThisTileGoal(TerrainTile tile) {
		if (tile.construct != null && tile.construct instanceof WoodBin) {
			WoodBin building = (WoodBin) tile.construct;
			if (!building.isPlot()) {
				return true;
			}
		}
		return false;
	}

}
