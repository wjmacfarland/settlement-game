package model.goals;

import model.TerrainTile;
import model.constructs.Bed;

public class FindBedGoal implements PathfinderGoal{

	@Override
	public boolean isThisTileGoal(TerrainTile tile) {
		if (tile.construct != null && tile.construct instanceof Bed) {
			Bed building = (Bed) tile.construct;
			if (!building.isPlot()) {
				return true;
			}
		}
		return false;
	}

}
