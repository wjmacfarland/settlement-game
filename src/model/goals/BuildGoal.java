package model.goals;

import model.TerrainTile;
import model.constructs.Plottable;

public final class BuildGoal implements PathfinderGoal {

	@Override
	public boolean isThisTileGoal(TerrainTile tile) {
		if (tile.construct != null && tile.construct instanceof Plottable) {
			Plottable plot = (Plottable) tile.construct;
			if (plot.isPlot()) {
				return true;
			}
		}
		return false;
	}

}
