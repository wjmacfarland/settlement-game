package model.goals;

import model.TerrainTile;
import model.constructs.Ore;

public final class OreGoal implements PathfinderGoal {

	@Override
	public boolean isThisTileGoal(TerrainTile tile) {
		if (tile.construct != null && tile.construct instanceof Ore) {
			return true;
		}
		return false;
	}

}
