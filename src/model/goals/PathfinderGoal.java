package model.goals;

import model.TerrainTile;

public interface PathfinderGoal {
	public boolean isThisTileGoal(TerrainTile tile);
}
