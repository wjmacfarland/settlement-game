package model.goals;

import model.TerrainTile;
import model.constructs.FoodContainer;

public final class DepositFoodGoal implements PathfinderGoal {

	@Override
	public boolean isThisTileGoal(TerrainTile tile) {
		if (tile.construct != null && tile.construct instanceof FoodContainer) {
			FoodContainer building = (FoodContainer) tile.construct;
			if (!building.isPlot()) {
				return true;
			}
		}
		return false;
	}

}
