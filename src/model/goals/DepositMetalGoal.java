package model.goals;

import model.TerrainTile;
import model.constructs.MetalStorage;

public final class DepositMetalGoal implements PathfinderGoal {

	@Override
	public boolean isThisTileGoal(TerrainTile tile) {
		if (tile.construct != null && tile.construct instanceof MetalStorage) {
			MetalStorage building = (MetalStorage) tile.construct;
			if (!building.isPlot()) {
				return true;
			}
		}
		return false;
	}

}
