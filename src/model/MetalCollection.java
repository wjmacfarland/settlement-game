package model;

import java.io.Serializable;
import java.util.LinkedList;

import model.resource.Metal;

public class MetalCollection implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static MetalCollection instance = null;
	private LinkedList<Metal> metalList;

	public static MetalCollection getInstance() {
		if (instance == null) {
			instance = new MetalCollection();
		}
		return instance;
	}

	public MetalCollection() {
		metalList = new LinkedList<Metal>();
	}

	public synchronized boolean add(Metal toAdd) {
		return metalList.add(toAdd);
	}

	public synchronized Metal get() {
		return metalList.remove();
	}

	public int size() {
		return metalList.size();
	}

	public boolean isEmpty() {
		return metalList.isEmpty();
	}
	
	public void clear(){
		metalList.clear();
	}


	@Override
	public String toString() {
		String toReturn = "" + this.size();
		return toReturn;
	}
	
	public  LinkedList<Metal> getMetalForSaving(){
		return metalList;
	}
	
	public void loadMetal(LinkedList<Metal> obj){
		metalList = obj;
	}

}
