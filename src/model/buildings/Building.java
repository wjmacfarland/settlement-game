package model.buildings;

import view.GameGUI;
import model.Zone;
import model.Units.UnitTaskCollection;
import model.Units.Tasks.PlotHouseTask;
import model.Units.Tasks.PlotStorageBuildingTask;
import model.Units.Tasks.PlotWorkshopTask;

public class Building extends Zone {

	public static enum BuildingType {
		HOUSE, WORKSHOP, STORAGE;
	};

	public BuildingType buildingType;

	public Building(BuildingType buildingType, Zone zone) {
		// Set the building dimensions to the inner zone
		super(zone.getInnerZone().getRectangle());
		this.buildingType = buildingType;
		constructWalls();
	}

	public void addHouseWallTask(int x, int y) {
		UnitTaskCollection.getInstance().addBuildingTask(
				new PlotHouseTask(x, y));
		GameGUI.log("Wall task created at (" + x + ", " + y + ")");
	}

	public void addStorageWallTask(int x, int y) {
		UnitTaskCollection.getInstance().addBuildingTask(
				new PlotStorageBuildingTask(x, y));
		GameGUI.log("Wall task created at (" + x + ", " + y + ")");
	}

	public void addWorkshopWallTask(int x, int y) {
		UnitTaskCollection.getInstance().addBuildingTask(
				new PlotWorkshopTask(x, y));
		GameGUI.log("Wall task created at (" + x + ", " + y + ")");
	}

	public void constructWalls() {
		switch (buildingType) {
		case HOUSE:
			for (int x = (int) (zone.getX() - 1); x < (int) (zone.getX() + zone
					.getWidth()) + 1; x++) {

				addHouseWallTask(x, (int) (zone.getY() - 1));
				if (x == (int) (zone.getX() - 1 + Math
						.round(zone.getWidth() / 2))) {
					// leave middle tile(s) blank for the doorway
				} else {
					addHouseWallTask(x, (int) (zone.getY() + zone.getHeight()));
				}
			}
			for (int y = (int) (zone.getY()); y < (int) (zone.getY() + zone
					.getHeight()) + 1; y++) {
				addHouseWallTask((int) (zone.getX() - 1), y);
				addHouseWallTask((int) (zone.getX() + zone.getWidth()), y);
			}
			break;

		case STORAGE:
			for (int x = (int) (zone.getX() - 1); x < (int) (zone.getX() + zone
					.getWidth()) + 1; x++) {

				addStorageWallTask(x, (int) (zone.getY() - 1));
				if (x == (int) (zone.getX() - 1 + Math
						.round(zone.getWidth() / 2))) {
					// leave middle tile(s) blank for the doorway
				} else {
					addStorageWallTask(x,
							(int) (zone.getY() + zone.getHeight()));
				}
			}
			for (int y = (int) (zone.getY()); y < (int) (zone.getY() + zone
					.getHeight()) + 1; y++) {
				addStorageWallTask((int) (zone.getX() - 1), y);
				addStorageWallTask((int) (zone.getX() + zone.getWidth()), y);
			}
			break;
		case WORKSHOP:
			for (int x = (int) (zone.getX() - 1); x < (int) (zone.getX() + zone
					.getWidth()) + 1; x++) {

				addWorkshopWallTask(x, (int) (zone.getY() - 1));
				if (x == (int) (zone.getX() - 1 + Math
						.round(zone.getWidth() / 2))) {
					// leave middle tile(s) blank for the doorway
				} else {
					addWorkshopWallTask(x,
							(int) (zone.getY() + zone.getHeight()));
				}
			}
			for (int y = (int) (zone.getY()); y < (int) (zone.getY() + zone
					.getHeight()) + 1; y++) {
				addWorkshopWallTask((int) (zone.getX() - 1), y);
				addWorkshopWallTask((int) (zone.getX() + zone.getWidth()), y);
			}
			break;
		}

	}

}
