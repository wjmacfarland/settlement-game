package model;

import helpers.HelperFunctions;
import helpers.NoiseGenerator;

import java.awt.Point;
import java.io.Serializable;

import launcher.Launcher;
import model.TerrainTile.TerrainType;
import model.Units.Unit;
import model.Units.UnitCollection;
import model.constructs.*;
import view.GameView;

public class GameMap implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static GameMap INSTANCE = new GameMap();
	public boolean safeTilesExist;

	public static GameMap getInstance() {
		return INSTANCE;
	}

	private static TerrainTile[][] mapTiles;

	private GameMap() {
		mapTiles = new TerrainTile[GameView.TILES_X][GameView.TILES_Y];
		generateRandom();
	}

	/*
	 * 
	 * Generate map using value noise as basis
	 */
	public void useNoise() {
		int width = GameView.TILES_X / 5;
		int height = GameView.TILES_Y / 5;
		NoiseGenerator noiseGen = new NoiseGenerator(width, height);
		int oreAmount = 0;
		int forestAmount = 0;
		int waterAmount = 0;
		// Build terrain tiles
		for (int x = 0; x < GameView.TILES_X; x++) {
			for (int y = 0; y < GameView.TILES_Y; y++) {
				if (Math.sqrt((x - GameView.TILES_X / 2) * (x - GameView.TILES_X / 2)
						+ (y - GameView.TILES_Y / 2) * (y - GameView.TILES_Y / 2)) < GameView.TILES_Y / 2) {
					float value = (float) noiseGen.turbulence(x, y, 25);
					if (value < 0.35f) {
						GameMap.mapTiles[x][y] = new TerrainTile(TerrainType.LAKE);
						waterAmount++;
					} else if (value < 0.36) {
						GameMap.mapTiles[x][y] = new TerrainTile(TerrainType.LAKE);
						if (Math.random() > 0.3f) {
							GameMap.mapTiles[x][y].construct = new FishingHole(x, y);
						}
					} else if (value < 0.43f) {
						GameMap.mapTiles[x][y] = new TerrainTile(TerrainType.SAND);
					} else if (value < 0.55f) {
						GameMap.mapTiles[x][y] = new TerrainTile(TerrainType.GRASS);
					} else if (value < 0.70f) {
						GameMap.mapTiles[x][y] = new TerrainTile(TerrainType.GRASS);
						if (Math.random() > 0.1f) {
							GameMap.mapTiles[x][y].construct = new Tree(x, y);
						} else {
							GameMap.mapTiles[x][y].construct = new Tree(Harvestable.Resource.APPLES, 3, x, y);
						}
						forestAmount++;
					} else {
						GameMap.mapTiles[x][y] = new TerrainTile(TerrainType.GRASS);
						if (Math.random() > 0.3f) {
							GameMap.mapTiles[x][y].construct = new Ore(x, y);
						} else {
							GameMap.mapTiles[x][y].construct = new Ore(2, x, y);
						}
						oreAmount++;
					}
				} else {
					GameMap.mapTiles[x][y] = new TerrainTile(TerrainType.SPACE);
				}
			}
		}
		int totalTiles = GameView.TILES_X * GameView.TILES_Y;
		if (oreAmount < totalTiles / 32.0f)
			useNoise();
		if (forestAmount < totalTiles / 21.0f)
			useNoise();
		if (waterAmount > totalTiles / 2.0f)
			useNoise();
	}

	/*
	 * 
	 * Generate map by "growing" nodes representing each resource type
	 */
	// public void useGrowth() {
	// RandomTerrainCreator creator = new RandomTerrainCreator(160, 78, 80, 45,
	// 80, 29, 30, 60, 25, 20);
	// char[][] tempMap = creator.getMap();
	// for (int x = 0; x < GameView.TILES_X; x++) {
	// for (int y = 0; y < GameView.TILES_Y; y++) {
	// if (tempMap[x][y] == 'T') {
	// GameMap.mapTiles[x][y] = new TerrainGrass();
	// GameMap.mapTiles[x][y].construct = new Tree(x, y);
	// } else if (tempMap[x][y] == 'S') {
	// GameMap.mapTiles[x][y] = new TerrainSpace();
	// } else if (tempMap[x][y] == 'M') {
	// GameMap.mapTiles[x][y] = new TerrainGrass();
	// GameMap.mapTiles[x][y].construct = new Ore(x, y);
	// } else if (tempMap[x][y] == 'W') {
	// GameMap.mapTiles[x][y] = new TerrainLake();
	// } else if (tempMap[x][y] == 'C') {
	// GameMap.mapTiles[x][y] = new TerrainGrass();
	// } else if (tempMap[x][y] == 'Q') {
	// GameMap.mapTiles[x][y] = new TerrainSand();
	// } else if (tempMap[x][y] == 'A') {
	// GameMap.mapTiles[x][y] = new TerrainGrass();
	// GameMap.mapTiles[x][y].construct = new Tree(Harvestable.Resource.APPLES,
	// 3, x, y);
	// } else {
	// GameMap.mapTiles[x][y] = new TerrainGrass();
	// }
	// }
	// }
	// }

	public void generateRandom() {
		useNoise();
	}

	public TerrainTile get(int x, int y) {
		if (HelperFunctions.valueInRange(x, 0, GameView.TILES_X)
				&& HelperFunctions.valueInRange(y, 0, GameView.TILES_Y)) {
			return mapTiles[x][y];
		}
		return null;
	}

	public TerrainTile get(Point point) {
		return get(point.x, point.y);
	}

	public Point getSize() {
		return new Point(GameView.TILES_X, GameView.TILES_Y);
	}

	public int xSpawn;
	public int ySpawn;

	public void spawnUnits(UnitCollection units, Point camera) {
		// Find walkable area to place starting units
		int walkableTiles = 0;
		int posX = 0;
		int posY = 0;
		do {
			posX = 0;
			posY = 0;
			do {
				posX = (int) (Math.random() * getSize().x);
				posY = (int) (Math.random() * getSize().y);
			} while (!GameMap.getInstance().get(posX, posY).isWalkable());
			walkableTiles = 0;
			for (int x = posX - 5; x < posX + 5; x++) {
				for (int y = posY - 5; y < posY + 5; y++) {
					if (HelperFunctions.valueInRange(x, 0, GameMap.getInstance().getSize().x)
							&& HelperFunctions.valueInRange(y, 0, GameMap.getInstance().getSize().y)) {
						if (GameMap.getInstance().get(x, y).isWalkable()) {
							walkableTiles++;
						}
					}
				}
			}
		} while (walkableTiles < 20);

		xSpawn = posX;
		ySpawn = posY;
		// Spawn units in "safe" area
		Unit unit1 = new Unit(new Point(posX, posY), 100);
		units.add(unit1);
		Unit unit2 = new Unit(new Point(posX, posY), 100);
		units.add(unit2);
		this.get(posX, posY).construct = new WoodBin(false);

		// Center camera on unit spawn location
		camera.x = (posX * 32) - Launcher.getSettings().RESOLUTION_X / 2;
		camera.y = (posY * 32) - Launcher.getSettings().RESOLUTION_Y / 2;

	}

	public void setInstance(GameMap map) {
		GameMap.INSTANCE = map;
	}

	public static TerrainTile[][] getMapTilesForSaving() {
		return mapTiles;
	}

	public void loadMapTiles(TerrainTile[][] obj) {
		mapTiles = obj;
		for (int x = 0; x < GameView.TILES_X; x++) {
			for (int y = 0; y < GameView.TILES_Y; y++) {
				mapTiles[x][y].load();
			}
		}
	}

	public int[][] getSafeTilesForSaving() {
		// TODO Auto-generated method stub
		return null;
	}
}
