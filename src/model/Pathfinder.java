package model;

import helpers.HelperFunctions;

import java.awt.Point;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Stack;

import view.GameView;
import model.goals.PathfinderGoal;

public class Pathfinder {

	// Maximum movement cost to attempt before giving up
	private static final int MAX_COST = 250;

	private static final Pathfinder INSTANCE = new Pathfinder();

	public static Pathfinder getInstance() {
		return INSTANCE;
	}

	public Stack<Node> findGoal(final int startX, final int startY, PathfinderGoal goal) {
		// Check start and destination are within map range
		if (!(HelperFunctions.valueInRange(startX, 0, GameMap.getInstance().getSize().x) && HelperFunctions
				.valueInRange(startY, 0, GameMap.getInstance().getSize().y))) {
			return null;
		}
		// Initialize the stack to hold the final path to the goal
		Stack<Node> path = new Stack<Node>();
		HashMap<Point, Node> closedMap = new HashMap<Point, Node>();
		// Store the open list in a priority queue for fast access to lowest
		// cost node
		PriorityQueue<Node> openList = new PriorityQueue<Node>();
		// Create and add starting node to openList
		Node startingNode = new Node(startX, startY, 0);
		startingNode.setPrevious(null);
		openList.add(startingNode);

		Node curNode = startingNode;

		boolean pathFound = false;
		while (pathFound == false) {
			if (openList.size() > 0) {
				curNode = openList.peek();
				if (goal.isThisTileGoal(curNode.correspondingTile())) {
					pathFound = true;
				} else {
					if (curNode.g > MAX_COST) {
						return null;
					}
					closedMap.put(curNode.coords, curNode);
					openList.remove();
					addNeighborsNoHeuristic(closedMap, openList, curNode, goal);
				}
			} else {
				return null;
			}
		}

		// Build the final path by reverse-traversing the references left in the
		// nodes
		do {
			path.push(curNode);
			curNode = curNode.previousNode;
		} while (curNode != null);
		return path;
	}

	private void addNeighborsNoHeuristic(HashMap<Point, Node> closedMap, PriorityQueue<Node> openList,
			Node parent, PathfinderGoal goal) {
		// Determine if near any edges of the map size to prevent out of bounds
		// errors

		// Edges go left, right, top, bottom
		boolean[] edge = new boolean[4];
		final Point[] offsets = { new Point(-1, 0), new Point(1, 0), new Point(0, 1), new Point(0, -1) };

		if (parent.coords.x - 1 < 0) {
			edge[0] = true;
		}
		if (parent.coords.x + 1 == GameMap.getInstance().getSize().x) {
			edge[1] = true;
		}
		if (parent.coords.y - 1 < 0) {
			edge[2] = true;
		}
		if (parent.coords.y + 1 == GameMap.getInstance().getSize().y) {
			edge[3] = true;
		}

		// Check all neighboring nodes and add valid ones to list
		for (int offset = 0; offset < 4; offset++) {
			if (!edge[offset]) {
				// If neighbor node is out of range, continue
				if (!HelperFunctions.valueInRange(parent.coords.x + offsets[offset].x, 0, GameView.TILES_X))
					continue;
				if (!HelperFunctions.valueInRange(parent.coords.y + offsets[offset].y, 0, GameView.TILES_Y))
					continue;
				// Otherwise, continue algorithm
				TerrainTile neighborTile = GameMap.getInstance().get(parent.coords.x + offsets[offset].x,
						parent.coords.y + offsets[offset].y);
				// 1 Below represents movement cost of moving to neighboring
				// tile
				Node neighborNode = new Node(parent.coords.x + offsets[offset].x, parent.coords.y
						+ offsets[offset].y, parent.g + 1);
				boolean constructWalkable = true;
				if (neighborTile.construct != null) {
					constructWalkable = neighborTile.construct.isWalkable();
				}
				if ((neighborTile.isWalkable() && constructWalkable)
						|| goal.isThisTileGoal(neighborNode.correspondingTile())) {
					// If the neighboring node is already in the closed list
					Node oldClosedNode = closedMap.get(neighborNode.coords);
					neighborNode.setPrevious(parent);
					if (oldClosedNode != null) {
						if (oldClosedNode.g > neighborNode.g) {
							closedMap.put(neighborNode.coords, neighborNode);
						}
					} else {
						// If the neighboring node is in the open list
						Node openListNode = null;
						for (Node n : openList) {
							if (neighborNode.equalsNode(n)) {
								openListNode = n;
							}
						}
						if (openListNode != null) {
							if (openListNode.g > neighborNode.g) {
								openList.remove(openListNode);
								openList.add(neighborNode);
							}
						} else {
							openList.add(neighborNode);
						}
					}
				}
			}
		}
	}

	// Call the findPath method but default to lastNodeGoal = false
	public Stack<Node> findPath(final int startX, final int startY, final int goalX, final int goalY) {
		return findPath(startX, startY, goalX, goalY, false);
	}

	public Stack<Node> findPath(final int startX, final int startY, final int goalX, final int goalY,
			boolean lastNodeGoal) {
		// Check start and destination are within map range
		if (!(HelperFunctions.valueInRange(startX, 0, GameMap.getInstance().getSize().x)
				&& HelperFunctions.valueInRange(goalX, 0, GameMap.getInstance().getSize().x)
				&& HelperFunctions.valueInRange(startY, 0, GameMap.getInstance().getSize().y) && HelperFunctions
					.valueInRange(goalY, 0, GameMap.getInstance().getSize().y))) {
			return null;
		}
		// Initialize the stack to hold the final path to the goal
		Stack<Node> path = new Stack<Node>();
		HashMap<Point, Node> closedMap = new HashMap<Point, Node>();
		// Store the open list in a priority queue for fast access to lowest
		// cost node
		PriorityQueue<Node> openList = new PriorityQueue<Node>();
		// Create and add starting node to openList
		Node startingNode = new Node(startX, startY, 0);
		startingNode.setPrevious(null);
		openList.add(startingNode);

		Node curNode = startingNode;

		boolean pathFound = false;
		while (pathFound == false) {
			if (openList.size() > 0) {
				curNode = openList.peek();
				if (curNode.coords.x == goalX && curNode.coords.y == goalY) {
					pathFound = true;
				} else {
					if (curNode.g > MAX_COST) {
						return null;
					}
					closedMap.put(curNode.coords, curNode);
					openList.remove();
					addNeighborsFindPath(closedMap, openList, curNode, goalX, goalY, lastNodeGoal);
				}
			} else {
				return null;
			}
		}

		// Build the final path by reverse-traversing the references left in the
		// nodes
		do {
			path.push(curNode);
			curNode = curNode.previousNode;
		} while (curNode != null);
		return path;
	}

	private void addNeighborsFindPath(HashMap<Point, Node> closedMap, PriorityQueue<Node> openList,
			Node parent, int goalX, int goalY, boolean lastNodeGoal) {
		// Determine if near any edges of the map size to prevent out of bounds
		// errors

		// Edges go left, right, top, bottom
		boolean[] edge = new boolean[4];
		final Point[] offsets = { new Point(-1, 0), new Point(1, 0), new Point(0, 1), new Point(0, -1) };

		if (parent.coords.x - 1 < 0) {
			edge[0] = true;
		}
		if (parent.coords.x + 1 == GameMap.getInstance().getSize().x) {
			edge[1] = true;
		}
		if (parent.coords.y - 1 < 0) {
			edge[2] = true;
		}
		if (parent.coords.y + 1 == GameMap.getInstance().getSize().y) {
			edge[3] = true;
		}

		// Check all neighboring nodes and add valid ones to list
		for (int offset = 0; offset < 4; offset++) {
			if (!edge[offset]) {
				// If neighbor node is out of range, continue
				if (!HelperFunctions.valueInRange(parent.coords.x + offsets[offset].x, 0, GameView.TILES_X))
					continue;
				if (!HelperFunctions.valueInRange(parent.coords.y + offsets[offset].y, 0, GameView.TILES_Y))
					continue;
				TerrainTile neighborTile = GameMap.getInstance().get(parent.coords.x + offsets[offset].x,
						parent.coords.y + offsets[offset].y);
				// Check if the construct on top of the neighbor tile is
				// walkable
				boolean constructWalkable = true;
				if (neighborTile.construct != null) {
					constructWalkable = neighborTile.construct.isWalkable();
				}
				// If lastNodeGoal is specified as true, then check if the
				// neighboring tile is the goal tile (even if it's unwalkable)
				boolean neighborNodeIsGoal = false;
				if (lastNodeGoal) {
					if (parent.coords.x + offsets[offset].x == goalX
							&& parent.coords.y + offsets[offset].y == goalY) {
						neighborNodeIsGoal = true;
					}
				}
				// If the neighbor tile is walkable and the construct on top of
				// it is walkable OR the neighbor tile is the goal tile (hence
				// the last tile in the path)
				if ((neighborTile.isWalkable() && constructWalkable) || neighborNodeIsGoal) {
					// 1 Below represents movement cost of moving to neighboring
					// tile
					Node neighborNode = new Node(parent.coords.x + offsets[offset].x, parent.coords.y
							+ offsets[offset].y, parent.g + 1);
					int h = Math.abs(neighborNode.coords.x - goalX) + Math.abs(neighborNode.coords.y - goalY);
					neighborNode.setPrevious(parent);
					neighborNode.h = h;
					// If the neighboring node is already in the closed list
					Node oldClosedNode = closedMap.get(neighborNode.coords);
					if (oldClosedNode != null) {
						if (oldClosedNode.g > neighborNode.g) {
							closedMap.put(neighborNode.coords, neighborNode);
						}
					} else {
						// If the neighboring node is in the open list
						Node openListNode = null;
						for (Node n : openList) {
							if (neighborNode.equalsNode(n)) {
								openListNode = n;
							}
						}
						if (openListNode != null) {
							if (openListNode.g > neighborNode.g) {
								openList.remove(openListNode);
								openList.add(neighborNode);
							}
						} else {
							openList.add(neighborNode);
						}
					}
				}
			}
		}
	}
}
