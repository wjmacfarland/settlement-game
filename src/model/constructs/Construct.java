package model.constructs;

import java.awt.Point;
import java.io.Serializable;

import org.newdawn.slick.Color;
import org.newdawn.slick.SpriteSheet;

public interface Construct extends Serializable {

	Point getGraphicCoordinates();

	boolean isWalkable();

	boolean isVisionBlocked();

	Color getTileColor();

	void draw(int x, int y, Color lighting, SpriteSheet tiles, Point camera);

	void draw(int x, int y, SpriteSheet tiles, Point camera);

}
