//package model.constructs;
//
//import java.awt.Point;
//
////
//import java.awt.Point;
//import java.util.ArrayList;
////
//import model.GameMap;
////
//import org.newdawn.slick.Color;
////
//////class represents a multi-tiled structure
//////children are the points inside the structures which sub-structures
//////can be created on, for instance a Woodbin in a woodShed
//////topLeft is the position of the top left tile of the structure
//////Walls is an array representing tiles where walls need to be drawn
//////relative to the topLeft
//////dimension represent the largest x-y dimensions of the building
//
//public class Structure implements StructureConstruct {
//
//	Point topLeft;
//	ArrayList<Wall> walls;
//	ArrayList<Point> children;
//	boolean hasBeenPlotted;
//	boolean hasBeenConstructed;
//	int woodCost = 5;
//	GameMap map = GameMap.getInstance();
//
//	public Structure(Point location, int xSize, int ySize) {
//		topLeft = location;
//		boolean isValid = true;
//		if (xSize >= 3 && ySize >= 3) {
//			for (int i = 0; i < xSize; i++) {
//				for (int j = 0; j < ySize; j++) {
//					if (!map.get(location.x + i, location.y + j).isWalkable()) {
//						isValid = false;
//					}
//				}
//			}
//		} else {
//			isValid = false;
//		}
//		if (isValid)
//			plotStructure(location, xSize, ySize);
//	}
//
//	private void plotStructure(Point location, int xSize, int ySize) {
//
//		for (int i = 0; i < xSize; i++) {
//			for (int j = 0; j < ySize; j++) {
//				if (i == 0 || i == xSize - 1 || j == 0 || j == ySize - 1) {
//					Wall wall = new Wall(new Point(topLeft.x + i, topLeft.y + j));
//					walls.add(wall);
//				} else {
//					Point child = new Point(topLeft.x + 1, topLeft.y + j);
//					children.add(child);
//				}
//			}
//		}
//
//		Point tempDoor = new Point(2, 0);
//		pickDoor(tempDoor);
//	}
//
//	private void pickDoor(Point doorLoc) {
//		Point mapLoc = new Point(doorLoc.x + topLeft.x, doorLoc.y + topLeft.y);
//		walls.remove(new Wall(mapLoc));
//	}
//
//	@Override
//	public boolean hasBeenPlotted() {
//		if (walls.size() > 0)
//			return true;
//		return false;
//	}
//
//	@Override
//	public boolean hasBeenBuilt() {
//		for (int i = 0; i < walls.size(); i++) {
//			if (!walls.get(i).isPlot()) {
//				hasBeenConstructed = false;
//				return false;
//			}
//		}
//		hasBeenConstructed = true;
//		return true;
//	}
//
//	@Override
//	public Color getTileColor() {
//		return new Color(100, 100, 100);
//	}
//
//	public ArrayList<Wall> getWalls() {
//		return walls;
//	}
//
//	public ArrayList<Point> getChildren() {
//		return children;
//	}
//
//}
