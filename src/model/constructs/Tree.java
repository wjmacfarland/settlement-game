package model.constructs;

import java.awt.Point;

import org.newdawn.slick.Color;
import org.newdawn.slick.SpriteSheet;

import model.GameMap;
import model.resource.Apple;
import model.resource.Food;
import model.resource.Wood;

public class Tree implements Construct, Harvestable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	 * 
	 * Instance Variables
	 */
	private Resource resource;
	private int apples;
	private int woodQuantity;
	private final int wood = 4;
	private int xLoc;
	private int yLoc;
	private boolean markedForHarvest;

	/*
	 * 
	 * Constructors
	 */
	public Tree(int x, int y) {
		this.setResource(null);
		this.setQuantity(0);
		this.markedForHarvest = false;
		woodQuantity = wood;
		xLoc = x;
		yLoc = y;
	}

	public Tree(Resource resource, int quantity, int x, int y) {
		this.setResource(resource);
		this.setQuantity(quantity);
		woodQuantity = wood;
		xLoc = x;
		yLoc = y;
	}

	@Override
	public Food[] harvest(int amount) {
		// If there is no resource to harvest in this tree return null
		if (resource == null)
			return null;
		// If there isn't any resource return null
		if (apples == 0)
			return null;
		// If there isn't as much as requested, return the remaining amount
		if (apples - amount < 0)
			amount = apples;
		// Create new array of food for amount requested
		Food[] food = new Food[amount];
		for (int i = 0; i < amount; i++) {
			switch (resource) {
			case APPLES:
				food[i] = new Apple();
				break;
			}
		}
		apples -= amount;
		return food;
	}

	public boolean hasApples() {
		return (apples > 0);
	}

	public Wood[] getWood() {
		Wood[] wood = new Wood[1];
		wood[0] = new Wood();
		woodQuantity -= 1;

		if (woodQuantity == 0) {
			GameMap.getInstance().get(xLoc, yLoc).removeContruct();
		}
		return wood;
	}

	/*
	 * 
	 * Getters and Setters
	 */

	static final Point gfx_PLAIN_TREE = new Point(6, 0);
	static final Point gfx_APPLE_TREE = new Point(7, 0);

	@Override
	public Point getGraphicCoordinates() {
		if (apples >= 1) {
			return gfx_APPLE_TREE;
		}
		return gfx_PLAIN_TREE;
	}

	@Override
	public boolean isWalkable() {
		return false;
	}

	@Override
	public boolean isVisionBlocked() {
		return true;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public int getQuantity() {
		return apples;
	}

	public void setQuantity(int quantity) {
		this.apples = quantity;
	}

	@Override
	public int getQuantityRemaining() {
		return this.apples;
	}

	public int getWoodQuantity() {
		return woodQuantity;
	}

	@Override
	public String toString() {
		if (apples >= 1) {
			return "T";
		} else {
			return "A";
		}
	}

	@Override
	public Color getTileColor() {
		return new Color(0, 128, 0, 192); // Dark green
	}

	public int getApples() {
		return apples;
	}

	@Override
	public boolean isMarkedForHarvest() {
		return markedForHarvest;
	}

	@Override
	public void setMarkedForHarvest(boolean b) {
		markedForHarvest = b;
	}

	@Override
	public void draw(int x, int y, Color lighting, SpriteSheet tiles, Point camera) {
		if (isMarkedForHarvest()) {
			tiles.getSubImage(0, 31).draw(x * 32 - camera.x, y * 32 - camera.y, 1, lighting);
		}
		tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y).draw(x * 32 - camera.x,
				y * 32 - camera.y, 1, lighting);
	}

	@Override
	public void draw(int x, int y, SpriteSheet tiles, Point camera) {
		if (isMarkedForHarvest()) {
			tiles.getSubImage(0, 31).draw(x * 32 - camera.x, y * 32 - camera.y);
		}
		tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y).draw(x * 32 - camera.x,
				y * 32 - camera.y);
	}
}
