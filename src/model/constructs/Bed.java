package model.constructs;

import java.awt.Point;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.SpriteSheet;

import model.resource.*;

public class Bed extends Plottable implements Serializable{

	public static final Point gfx_BED = new Point(14, 7);
	public static final Point gfx_CONSTRUCTION_SITE = new Point(0, 4);
	public static final int WOOD_TO_BUILD = 10;

	public boolean isOpen = false;

	// Contains a list of items as the building is being constructed. Should be
	// empty once building is erected.
	public List<Item> storage;

	public Bed() {
		super(gfx_BED);
		this.plot = true;
		this.walkable = true;
		this.storage = new LinkedList<Item>();
	}

	@Override
	public boolean addMaterial(Item material) {
		if (material instanceof Wood) {
			this.storage.add(material);
			if (storage.size() == WOOD_TO_BUILD) {
				// Clear the 5 wood as they are used to construct the food
				// container
				storage.clear();
				// Set the food container as no longer just a plot!
				this.plot = false;
				this.isOpen = true;
				// Also, the units can't walk over it any longer.
				this.walkable = false;
			}
			return true;
		}
		return false;
	}

	@Override
	public Point getGraphicCoordinates() {
		if (this.plot) {
			return gfx_CONSTRUCTION_SITE;
		} else {
			return gfx_BED;
		}
	}

	@Override
	public boolean isVisionBlocked() {
		if (plot) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isOpen() {
		return isOpen;
	}

	public void setOpen(boolean toSet) {
		isOpen = toSet;
	}

	@Override
	public int woodNeededToBuild() {
		return WOOD_TO_BUILD;
	}

	@Override
	public String toString() {
		return "B";
	}

	@Override
	public void draw(int x, int y, Color lighting, SpriteSheet tiles, Point camera) {
		tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y).draw(x * 32 - camera.x,
				y * 32 - camera.y, 1, lighting);
	}

	@Override
	public void draw(int x, int y, SpriteSheet tiles, Point camera) {
		tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y).draw(x * 32 - camera.x,
				y * 32 - camera.y);
	}

}
