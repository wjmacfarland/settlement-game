package model.constructs;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.SpriteSheet;

import view.lighting.LightEmitter;
import model.resource.*;

public class Campfire extends Plottable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Different stages of construction
	public static final Point gfx_STAGE_1 = new Point(0, 123);
	public static final Point gfx_STAGE_2 = new Point(1, 123);
	public static final Point gfx_STAGE_3 = new Point(2, 123);
	public static final Point gfx_ANIMATION_START = new Point(3, 123);

	public static final int WOOD_TO_BUILD = 10;

	public LightEmitter emitter;

	// Contains a list of items as the building is being constructed. Should be
	// empty once building is erected.
	public List<Item> storage;

	public Campfire() {
		super(gfx_STAGE_1);
		this.plot = true;
		this.walkable = true;
		this.storage = new LinkedList<Item>();
	}

	@Override
	public boolean addMaterial(Item material) {
		if (material instanceof Wood) {
			this.storage.add(material);
			if (storage.size() == WOOD_TO_BUILD) {
				// Clear the 5 wood as they are used to construct the food
				// container
				storage.clear();
				// Set the food container as no longer just a plot!
				this.plot = false;
				// Also, the units can't walk over it any longer.
				this.walkable = false;
			}
			return true;
		}
		return false;
	}

	@Override
	public Point getGraphicCoordinates() {
		if (this.plot) {
			if (storage.size() == 2) {
				return gfx_STAGE_2;
			}
			return gfx_STAGE_1;
		} else {
			return gfx_STAGE_3;
		}
	}

	@Override
	public boolean isVisionBlocked() {
		if (plot) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public int woodNeededToBuild() {
		return WOOD_TO_BUILD;
	}

	@Override
	public void draw(int x, int y, Color lighting, SpriteSheet tiles, Point camera) {
		if (this.isPlot()) {
			tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y).draw(x * 32 - camera.x,
					y * 32 - camera.y, 1, lighting);
		} else {
			long time = System.currentTimeMillis();
			if (time % 600 < 200) {
				tiles.getSubImage(gfx_ANIMATION_START.x, gfx_ANIMATION_START.y).draw(x * 32 - camera.x,
						y * 32 - camera.y, 1, lighting);
			} else if (time % 600 < 400) {
				tiles.getSubImage(gfx_ANIMATION_START.x + 1, gfx_ANIMATION_START.y).draw(x * 32 - camera.x,
						y * 32 - camera.y, 1, lighting);
			} else {
				tiles.getSubImage(gfx_ANIMATION_START.x + 2, gfx_ANIMATION_START.y).draw(x * 32 - camera.x,
						y * 32 - camera.y, 1, lighting);
			}
		}
	}

	@Override
	public void draw(int x, int y, SpriteSheet tiles, Point camera) {
		if (this.isPlot()) {
			tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y).draw(x * 32 - camera.x,
					y * 32 - camera.y);
		} else {
			long time = System.currentTimeMillis();
			if (time % 600 < 200) {
				tiles.getSubImage(gfx_ANIMATION_START.x, gfx_ANIMATION_START.y).draw(x * 32 - camera.x,
						y * 32 - camera.y);
			} else if (time % 600 < 400) {
				tiles.getSubImage(gfx_ANIMATION_START.x + 1, gfx_ANIMATION_START.y).draw(x * 32 - camera.x,
						y * 32 - camera.y);
			} else {
				tiles.getSubImage(gfx_ANIMATION_START.x + 2, gfx_ANIMATION_START.y).draw(x * 32 - camera.x,
						y * 32 - camera.y);
			}
		}
	}

}
