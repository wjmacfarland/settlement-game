package model.constructs;

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;

import org.newdawn.slick.Color;

public interface StructureConstruct extends Serializable{

	ArrayList<HouseWall> getWalls();
	
	ArrayList<Point> getChildren();

	boolean hasBeenPlotted();
	
	boolean hasBeenBuilt();
	
	Color getTileColor();

	
}
