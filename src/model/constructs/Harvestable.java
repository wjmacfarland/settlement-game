package model.constructs;

import model.resource.Item;

public interface Harvestable {

	public static enum Resource {
		APPLES, WOOD, METAL;
	}

	public boolean isMarkedForHarvest();
	public void setMarkedForHarvest(boolean b);

	public Item[] harvest(int amount);

	public int getQuantityRemaining();

}
