package model.constructs;

import java.awt.Point;

import model.GameMap;
import model.resource.Item;
//import model.resource.Metal;

import model.resource.Metal;

import org.newdawn.slick.Color;
import org.newdawn.slick.SpriteSheet;

public class Ore implements Construct, Harvestable {

	static final Point gfx_METAL_ORE = new Point(4, 5);
	static final Point gfx_ROCK = new Point(3, 5);

	private Resource resource;
	public int metalQuantity;
	private int xLoc;
	private int yLoc;
	private boolean markedForHarvest;

	public Ore(int x, int y) {
		xLoc = x;
		yLoc = y;
		metalQuantity = 0;
		markedForHarvest = false;
	}

	public Ore(int quantity, int x, int y) {
		metalQuantity = quantity;
		xLoc = x;
		yLoc = y;
		markedForHarvest = false;
	}

	@Override
	public Point getGraphicCoordinates() {
		if (metalQuantity >= 1) {
			return gfx_METAL_ORE;
		} else {
			return gfx_ROCK;
		}
	}

	@Override
	public boolean isWalkable() {
		return false;
	}

	@Override
	public boolean isVisionBlocked() {
		return true;
	}

	@Override
	public String toString() {
		return "M";
	}

	@Override
	public Color getTileColor() {
		return Color.gray;
	}
	
	public Item breakRock(){
		if(metalQuantity > 0){
			metalQuantity = 0;
			GameMap.getInstance().get(xLoc, yLoc).removeContruct();
			return new Metal();
		}
		else{
			GameMap.getInstance().get(xLoc, yLoc).removeContruct();
			return null;
		}
	}

	@Override
	public void draw(int x, int y, Color lighting, SpriteSheet tiles,
			Point camera) {
		tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y)
				.draw(x * 32 - camera.x, y * 32 - camera.y, 1, lighting);
	}

	@Override
	public void draw(int x, int y, SpriteSheet tiles, Point camera) {
		tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y)
				.draw(x * 32 - camera.x, y * 32 - camera.y);
	}

	@Override
	public boolean isMarkedForHarvest() {
		return markedForHarvest;
	}

	@Override
	public void setMarkedForHarvest(boolean b) {
		markedForHarvest = b;

	}

	@Override
	public Item[] harvest(int amount) {
		// If there is no resource to harvest in this tree return null
		if (resource == null)
			return null;
		// If there isn't any resource return null
		if (metalQuantity == 0)
			return null;
		// If there isn't as much as requested, return the remaining amount
		if (metalQuantity - amount < 0)
			amount = metalQuantity;
		// Create new array of food for amount requested
		Item[] item = new Item[amount];
		for (int i = 0; i < amount; i++) {
			item[i] = new Metal();
			break;
		}
		metalQuantity -= amount;
		return item;
	}

	@Override
	public int getQuantityRemaining() {
		return metalQuantity;
	}
}
