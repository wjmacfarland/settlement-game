package model.constructs;

import java.awt.Point;

import model.resource.Fish;
import model.resource.Food;
import model.resource.Item;
import org.newdawn.slick.Color;
import org.newdawn.slick.SpriteSheet;

public class FishingHole implements Construct, Harvestable {

	private static final Point gfx_FISH_HOLE = new Point(3, 1);
	private int xLoc;
	private int yLoc;
	
	public FishingHole(int x, int y){
		xLoc = x;
		yLoc = y;
		
	}

	// They have unlimited fish, so just return fish, no worries about if any
	// are left
	@Override
	public Item[] harvest(int amount) {
		// Create new array of food for amount requested
		Food[] food = new Food[amount];
		for (int i = 0; i < amount; i++) {
			food[i] = new Fish();
		}
		return food;

	}


	@Override
	public int getQuantityRemaining() {
		return 0;
	}

	@Override
	public Point getGraphicCoordinates() {
		return gfx_FISH_HOLE;
	}

	@Override
	public boolean isWalkable() {
		return false;
	}

	@Override
	public boolean isVisionBlocked() {
		return false;
	}

	@Override
	public Color getTileColor() {
		return new Color(0, 0, 1, 0.75f); // blue, same as normal water
	}

	@Override
	public boolean isMarkedForHarvest() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setMarkedForHarvest(boolean b) {
		// TODO Auto-generated method stub

	}

	@Override
	public void draw(int x, int y, Color lighting, SpriteSheet tiles, Point camera) {
		tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y).draw(x * 32 - camera.x,
				y * 32 - camera.y, 1, lighting);
	}

	@Override
	public void draw(int x, int y, SpriteSheet tiles, Point camera) {
		tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y).draw(x * 32 - camera.x,
				y * 32 - camera.y);
	}

}
