package model.constructs;

import java.io.Serializable;
import java.util.LinkedList;


public class BedCollection implements Serializable {

	private static BedCollection instance = null;

	// This might be able to be an int. I do not know what's going to work out
	// this early on
	private LinkedList<Bed> beds;

	public static BedCollection getInstance() {
		if (instance == null) {
			instance = new BedCollection();
		}
		return instance;
	}

	public BedCollection() {
		beds = new LinkedList<Bed>();
	}

	public synchronized boolean add(Bed bedToAdd) {
		return beds.add(bedToAdd);
	}

	public synchronized Bed get() {
		return beds.remove();
	}

	public int size() {
		return beds.size();
	}

	public boolean isEmpty() {
		return beds.isEmpty();
	}

	@Override
	public String toString() {
		String toReturn = "" + this.size();
		return toReturn;
	}
	
	public int areOpenBeds(){
		int count = 0;
		for (Bed bed : beds){
			if(bed.isOpen){
				count++;
			}
		}
		return count;
	}
	
	public LinkedList<Bed> saveBeds(){
		return beds;
	}
	
	public void loadBeds(LinkedList<Bed> toLoad){
		beds = toLoad;
	}
}
