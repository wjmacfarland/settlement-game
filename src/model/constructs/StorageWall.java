package model.constructs;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.SpriteSheet;

import model.resource.Item;
import model.resource.Wood;

public class StorageWall extends Plottable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final Point GFX = new Point(7, 3);

	public final int WOOD_TO_BUILD = 0;
	public List<Item> storage;

	public StorageWall() {
		super(GFX);
		this.walkable = false;
		this.plot = false;
		this.storage = new LinkedList<Item>();
	}

	@Override
	public boolean addMaterial(Item material) {
		if (material instanceof Wood) {
			this.storage.add(material);
			if (storage.size() == WOOD_TO_BUILD) {
				// Clear the wood
				// container
				storage.clear();
				// Set the food container as no longer just a plot!
				this.plot = false;
				// Also, the units can't walk over it any longer.
				this.walkable = false;
			}
			return true;
		}
		return false;
	}

	@Override
	public int woodNeededToBuild() {
		return WOOD_TO_BUILD;
	}

	@Override
	public String toString() {
		return "W";
	}

	@Override
	public void draw(int x, int y, Color lighting, SpriteSheet tiles, Point camera) {
		tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y).draw(x * 32 - camera.x,
				y * 32 - camera.y, 1, lighting);
	}

	@Override
	public void draw(int x, int y, SpriteSheet tiles, Point camera) {
		tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y).draw(x * 32 - camera.x,
				y * 32 - camera.y);
	}
}
