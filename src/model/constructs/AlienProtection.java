package model.constructs;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;

import model.GameMap;
import model.resource.*;

public class AlienProtection extends Plottable implements Construct {

	public static Image forcefield;

	public static final Point gfx_PROTECTION = new Point(0, 28);
	public static final Point gfx_CONSTRUCTION_SITE = new Point(0, 4);
	public static final int WOOD_TO_BUILD = 20;

	public int xLoc, yLoc;

	// Contains a list of items as the building is being constructed. Should be
	// empty once building is erected.
	public List<Item> storage;

	public AlienProtection(int x, int y) {
		super(gfx_PROTECTION);
		this.plot = true;
		this.walkable = true;
		this.storage = new LinkedList<Item>();
		xLoc = x;
		yLoc = y;
	}

	@Override
	public boolean addMaterial(Item material) {
		if (material instanceof Wood) {
			this.storage.add(material);
			if (storage.size() == WOOD_TO_BUILD) {
				// Clear the 5 wood as they are used to construct the food
				// container
				storage.clear();
				this.plot = false;
				// Also, the units can't walk over it any longer.
				this.walkable = false;
				setSurroundingTilesToSafe();
			}
			return true;
		}
		return false;
	}

	private void setSurroundingTilesToSafe() {

		for (int col = xLoc - 10; col < xLoc + 10; col++) {
			for (int row = yLoc - 10; row < yLoc + 10; row++) {
				if ((xLoc - col) * (xLoc - col) + (yLoc - row) * (yLoc - row) < 100) {
					GameMap.getInstance().get(col, row).setSafe(true);
				}
			}
		}
		GameMap.getInstance().get(xLoc, yLoc).protectorBuiltHere = true;
	}

	@Override
	public Point getGraphicCoordinates() {
		if (this.plot) {
			return gfx_CONSTRUCTION_SITE;
		} else {
			return gfx_PROTECTION;
		}
	}

	@Override
	public boolean isVisionBlocked() {
		if (plot) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public int woodNeededToBuild() {
		return WOOD_TO_BUILD;
	}

	@Override
	public String toString() {
		return "B";
	}

	@Override
	public void draw(int x, int y, Color lighting, SpriteSheet tiles, Point camera) {
		tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y).draw(x * 32 - camera.x,
				y * 32 - camera.y, 1, lighting);
	}

	@Override
	public void draw(int x, int y, SpriteSheet tiles, Point camera) {
		tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y).draw(x * 32 - camera.x,
				y * 32 - camera.y);
	}

}
