package model.constructs;

import java.awt.Point;

import model.resource.Item;

import org.newdawn.slick.Color;

public abstract class Plottable implements Construct {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected boolean walkable;
	protected Point gfx;
	protected boolean plot;

	/*
	 * 
	 * Constructors
	 */
	public Plottable(Point gfx) {
		this.setGfx(gfx);
		this.plot = true;
	}

	/*
	 * 
	 * Getters / Setters
	 */
	@Override
	public Point getGraphicCoordinates() {
		return gfx;
	}

	@Override
	public boolean isWalkable() {
		return walkable;
	}

	@Override
	public boolean isVisionBlocked() {
		if (plot) {
			return false;
		} else {
			return true;
		}
	}

	public Point getGfx() {
		return gfx;
	}

	public void setGfx(Point gfx) {
		this.gfx = gfx;
	}

	public boolean isPlot() {
		return plot;
	}
	
	public abstract int woodNeededToBuild();

	public abstract boolean addMaterial(Item material);

	@Override
	public Color getTileColor() {
		return new Color(160, 82, 45); // Brown
	}

}
