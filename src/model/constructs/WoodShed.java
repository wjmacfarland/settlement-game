package model.constructs;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.SpriteSheet;

import model.WoodCollection;
import model.resource.Item;
import model.resource.Wood;

public class WoodShed extends Plottable {

	public static final Point gfx_STORAGE = new Point(14, 1);
	public static final Point gfx_CONSTRUCTION_SITE = new Point(0, 4);
	public final int WOOD_TO_BUILD = 5;

	// Contains a list of items as the building is being constructed. Should be
	// empty once building is erected.
	public List<Item> storage;

	public WoodShed() {
		super(gfx_STORAGE);
		this.plot = true;
		this.walkable = true;
		this.storage = new LinkedList<Item>();
	}
	
	public WoodShed(boolean isPlot) {
		super(gfx_STORAGE);
		this.plot = isPlot;
		this.walkable = true;
		this.storage = new LinkedList<Item>();
	}

	@Override
	public boolean addMaterial(Item material) {
		if (material instanceof Wood) {
			this.storage.add(material);
			if (storage.size() == WOOD_TO_BUILD) {
				// Clear the 5 wood as they are used to construct the food
				// container
				storage.clear();
				// Set the food container as no longer just a plot!
				this.plot = false;
				// Also, the units can't walk over it any longer.
				this.walkable = false;
			}
			return true;
		}
		return false;
	}

	public void depositWood(Wood wood) {
		if (!isPlot())
			WoodCollection.getInstance().add(wood);
	}

	@Override
	public Point getGraphicCoordinates() {
		if (this.plot) {
			return gfx_CONSTRUCTION_SITE;
		} else {
			return gfx_STORAGE;
		}
	}

	@Override
	public boolean isVisionBlocked() {
		if (plot) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public int woodNeededToBuild() {
		return WOOD_TO_BUILD;
	}

	@Override
	public String toString() {
		return "C";
	}

	@Override
	public void draw(int x, int y, Color lighting, SpriteSheet tiles, Point camera) {
		tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y).draw(x * 32 - camera.x,
				y * 32 - camera.y, 1, lighting);
	}

	@Override
	public void draw(int x, int y, SpriteSheet tiles, Point camera) {
		tiles.getSubImage(getGraphicCoordinates().x, getGraphicCoordinates().y).draw(x * 32 - camera.x,
				y * 32 - camera.y);
	}

}
