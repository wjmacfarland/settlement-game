package model;

import java.awt.Point;
import java.io.Serializable;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

import model.constructs.Construct;

public class TerrainTile implements Serializable {

	public enum TerrainType {
		GRASS(6, 1, true, new Color(0, 1, 0, 0.75f)),
		LAKE(0, 1, false, new Color(0, 0, 1, 0.75f)),
		SAND(6, 5, true, new Color(1, 1, 0, 0.75f)),
		SPACE(0, 0, false, new Color(0, 0, 0, 0));

		public final Point gfx;
		public final boolean walkable;
		public final Color tileColor;

		TerrainType(int x, int y, boolean walkable, Color tileColor) {
			gfx = new Point(x, y);
			this.walkable = walkable;
			this.tileColor = tileColor;
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private TerrainType type;
	protected transient float lightValue;
	public Construct construct;
	public boolean isSafe;
	public boolean protectorBuiltHere;
	public boolean isHouse;
	public boolean isStorage;
	public boolean isWorkshop;

	public TerrainTile(TerrainType type) {
		this.type = type;
		this.isSafe = false;
	}

	public TerrainTile(TerrainType type, Construct construct) {
		this.type = type;
		this.construct = construct;
		this.isSafe = false;
	}

	public boolean isWalkable() {
		if (construct != null) {
			return type.walkable && construct.isWalkable();
		}
		return type.walkable;
	}

	public boolean blocksVision() {
		if (construct != null) {
			return construct.isVisionBlocked();
		}
		return false;
	}

	public void setLightValue(float val) {
		lightValue = val;
	}

	public float getLightValue() {
		return lightValue;
	}

	public Construct getConstruct() {
		return construct;
	}

	public boolean isValidConstructionSite() {
		if (type.walkable && construct == null) {
			return true;
		}
		return false;
	}

	public boolean hasBuilding() {
		if (construct != null)
			return true;
		return false;
	}

	public void removeContruct() {
		construct = null;
	}

	public Color getTileColor() {
		if (construct != null) {
			return construct.getTileColor();
		}
		return type.tileColor;
	}

	public void load() {
	}

	public void draw(int x, int y, Color lighting, SpriteSheet tiles, Point camera, Graphics g)
			throws SlickException {
		if (type != TerrainType.SPACE) {
			tiles.getSubImage(type.gfx.x, type.gfx.y).draw(x * 32 - camera.x, y * 32 - camera.y, 1, lighting);
			if (construct != null) {
				construct.draw(x, y, lighting, tiles, camera);
			}
		}
	}

	public void draw(int x, int y, SpriteSheet tiles, Point camera, Graphics g) {
		if (type != TerrainType.SPACE) {
			tiles.getSubImage(type.gfx.x, type.gfx.y).draw(x * 32 - camera.x, y * 32 - camera.y);
			if (construct != null) {
				construct.draw(x, y, tiles, camera);
			}
		}
	}

	public TerrainType getType() {
		return type;
	}

	public void setType(TerrainType type) {
		this.type = type;
	}

	public boolean isSafe() {
		return isSafe;
	}

	public void setSafe(boolean toSet) {
		isSafe = toSet;

		if (toSet && !GameMap.getInstance().safeTilesExist) {
			GameMap.getInstance().safeTilesExist = true;
		}
	}

	public boolean isHouse() {
		return isHouse;
	}

	public void setHouse(boolean toSet) {
		isHouse = toSet;

	}

	public boolean isStorage() {
		return isStorage;
	}

	public void setStorage(boolean toSet) {
		isStorage = toSet;

	}

	public boolean isWorkshop() {
		return isWorkshop;
	}

	public void isWorkshop(boolean toSet) {
		isWorkshop = toSet;

	}

}
