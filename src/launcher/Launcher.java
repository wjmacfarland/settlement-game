package launcher;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

import view.GameView;

public class Launcher extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6370545645218613418L;

	// Game
	private static GameView settlementGame;

	// Serialized settings object
	public static Settings settings;

	// Settings loaded from disk
	private static int savedResX;
	private static int savedResY;
	private static boolean savedFullscreen;
	private static boolean savedMusic;

	// GUI Related Variables
	private JButton launchButton;
	private JTextField resX;
	private JTextField resY;
	private JCheckBox fullscreen;
	private JCheckBox music;

	private Launcher() throws IOException {
		initializeWindow();
	}

	// Create GUI for Settings Launcher
	public static void main(String[] args) throws IOException {
		loadSettings();
		Launcher aWindow = new Launcher();
		aWindow.setVisible(true);
	}

	private void startGame() throws LWJGLException {
		settlementGame = new GameView("Planetary Survival");
		try {
			AppGameContainer container = new AppGameContainer(settlementGame);
			container.setDisplayMode(settings.RESOLUTION_X, settings.RESOLUTION_Y, settings.FULLSCREEN);
			container.setShowFPS(false);
			container.setUpdateOnlyWhenVisible(false);
			// Cap frame rate to 120 fps
			container.setTargetFrameRate(120);
			container.start();
		} catch (SlickException e) {
		}
	}

	public static void loadSettings() {
		// Read from disk using FileInputStream
		FileInputStream f_in = null;
		try {
			f_in = new FileInputStream("settings.dat");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		if (f_in == null)
			return;

		// Read object using ObjectInputStream
		ObjectInputStream obj_in = null;
		try {
			obj_in = new ObjectInputStream(f_in);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (obj_in == null)
			return;

		// Read an object
		Object obj = null;
		try {
			obj = obj_in.readObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (obj == null)
			return;

		settings = (Settings) obj;
		savedResX = settings.RESOLUTION_X;
		savedResY = settings.RESOLUTION_Y;
		savedFullscreen = settings.FULLSCREEN;
		savedMusic = settings.MUSIC_ENABLED;
	}

	public static void saveSettings() {
		// Write to disk with FileOutputStream
		FileOutputStream f_out = null;
		try {
			f_out = new FileOutputStream("settings.dat");
		} catch (FileNotFoundException e3) {
			e3.printStackTrace();
		}

		// Write object with ObjectOutputStream
		ObjectOutputStream obj_out = null;
		if (f_out == null)
			return;
		try {
			obj_out = new ObjectOutputStream(f_out);
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		if (obj_out == null)
			return;
		// Write object out to disk
		try {
			obj_out.writeObject(settings);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	class SplashPanel extends JPanel {

		Image splashImage;

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SplashPanel() throws IOException {
			this.setPreferredSize(new Dimension(400, 320));
			this.setLayout(new BorderLayout());
			splashImage = ImageIO.read(getClass().getResource("planet_art.png"));
		}

		@Override
		public void paintComponent(Graphics g) {
			Graphics2D g2 = (Graphics2D) g;
			g2.setColor(Color.black);
			g2.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
			// Draw splash image centered in canvas
			g2.drawImage(splashImage, this.getWidth() / 2 - splashImage.getWidth(this) / 2, 16, this);
		}
	}

	class ButtonPanel extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		ButtonPanel(Launcher launcher) {

			this.setLayout(new BorderLayout());

			JLabel printOptions = new JLabel("Configure options below and then hit Launch Game.",
					SwingConstants.CENTER);
			add(printOptions, BorderLayout.NORTH);

			// Create actionList nested JPanel (horrible way to do it, but just
			// gotta get it working for now)
			JPanel optionsList = new JPanel();
			optionsList.setLayout(new GridLayout(4, 2, 16, 2));

			// Set width
			JLabel widthText = new JLabel("Resolution Width:", SwingConstants.CENTER);
			optionsList.add(widthText);
			// Draw width value
			resX = new JTextField("" + savedResX);
			optionsList.add(resX);

			// Set height
			JLabel heightText = new JLabel("Resolution Height:", SwingConstants.CENTER);
			optionsList.add(heightText);
			// Draw height value
			resY = new JTextField("" + savedResY);
			resY.setPreferredSize(new Dimension(64, 16));
			optionsList.add(resY);

			// Set fullscreen enabled/disabled
			JLabel fullscreenText = new JLabel("Enable Fullscreen:", SwingConstants.CENTER);
			optionsList.add(fullscreenText);
			fullscreen = new JCheckBox();
			fullscreen.setSelected(savedFullscreen);
			optionsList.add(fullscreen);

			// Set music enabled/disabled
			JLabel musicText = new JLabel("Enable Music:", SwingConstants.CENTER);
			optionsList.add(musicText);
			music = new JCheckBox();
			music.setSelected(savedMusic);
			optionsList.add(music);

			// Add the optionsList to the main GUI
			this.add(optionsList, BorderLayout.CENTER);

			// Continue building main GUI
			launchButton = new JButton("Launch Game");
			launchButton.addActionListener(launcher);
			this.add(launchButton, BorderLayout.SOUTH);
		}
	}

	public void initializeWindow() throws IOException {
		// Set the look and feel to native defaults
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Planetary Survival Launcher");
		// I think we can set this to false now since we're setting the size
		// manually
		setResizable(false);

		setLayout(new BorderLayout(0, 0));

		this.add(new SplashPanel(), BorderLayout.NORTH);

		JTextPane text = new JTextPane();
		StyledDocument doc = text.getStyledDocument();
		SimpleAttributeSet center = new SimpleAttributeSet();
		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		doc.setParagraphAttributes(0, doc.getLength(), center, false);
		text.setBackground(Color.black);
		text.setForeground(Color.green);
		text.setFont(new Font("Courier", Font.PLAIN, 12));
		text.setText("Thank you for playing Planetary Survival!\n\nThis game was created by four "
				+ "University of Arizona students: Jordan, Walter, Charlie, and Zach for Csc 335 during "
				+ "the spring of 2014. Music by Walter, Jordan, and Zach.\n\nSpecial thanks to:\nWaldo - provided free use to his tileset "
				+ "(http://smf.cataclysmdda.com/index.php?topic=4859.0)\nGabe - For guidance pertaining to "
				+ "pathfinding and noise generation algorithms and other inquiries we had along the way.");
		text.setEditable(false);
		add(text, BorderLayout.CENTER);

		this.add(new ButtonPanel(this), BorderLayout.SOUTH);

		// Set window size to correct dimensions
		// Force the window to be the correct size for now
		this.setSize(485, 650);

		// Set the window to the center of the screen
		setLocationRelativeTo(null);

		// Default to desktop res and fullscreen - true if no settings file was
		// found
		if (settings == null) {
			DisplayMode mode = Display.getDesktopDisplayMode();
			resX.setText("" + mode.getWidth());
			resY.setText("" + mode.getHeight());
			fullscreen.setSelected(true);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(launchButton)) {
			int rx = Integer.parseInt(resX.getText());
			int ry = Integer.parseInt(resY.getText());
			boolean fullscreenBool = fullscreen.isSelected();
			boolean musicBool = music.isSelected();
			settings = new Settings(rx, ry, fullscreenBool);
			settings.MUSIC_ENABLED = musicBool;
			saveSettings();
			this.setVisible(false);
			// Attempt to load past save data
			try {
				startGame();
			} catch (LWJGLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public static Settings getSettings() {
		return settings;
	}
}
