package launcher;

import java.io.Serializable;

public class Settings implements Serializable {

	// Constants which cannot change after game is running
	private static final long serialVersionUID = 3128685504492098480L;
	public final int RESOLUTION_X;
	public final int RESOLUTION_Y;
	public final int TILES_ON_SCREEN_X;
	public final int TILES_ON_SCREEN_Y;

	// Changeable values during runtime
	public boolean FULLSCREEN;
	public boolean DEBUG_INFO;
	public boolean MUSIC_ENABLED;

	public Settings(final int w, final int h, final boolean fullscreen) {
		RESOLUTION_X = w;
		RESOLUTION_Y = h;
		TILES_ON_SCREEN_X = (RESOLUTION_X + 16) / 32 + 1;
		TILES_ON_SCREEN_Y = (RESOLUTION_Y + 16) / 32 + 1;
		FULLSCREEN = fullscreen;
	}
}
