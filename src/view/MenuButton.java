package view;

import java.awt.Point;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

public class MenuButton extends Button {

	public Menu subMenu;

	private boolean expanded;

	public MenuButton(String buttonLabel, int x, int y) {
		super(x, y, buttonLabel);
		this.h = 32;
	}

	protected boolean mouseCollides(int x, int y) {
		Rectangle buttonRect = new Rectangle(this.x, this.y, w, h);
		Rectangle mouseRect = new Rectangle(x, y, 1, 1);
		if (buttonRect.intersects(mouseRect)) {
			return true;
		}
		return false;
	}

	public MenuButton click(int x, int y) {
		if (subMenu != null && expanded) {
			return subMenu.click(x, y);
		} else {
			if (mouseCollides(x, y)) {
				return this;
			}
		}
		return null;
	}

	public void draw(Graphics g, Point camera) {
		int sumX = x - camera.x;
		int sumY = y - camera.y;
		if (subMenu != null) {
			g.setColor(new Color(153, 153, 153, 128));
		} else {
			g.setColor(new Color(102, 153, 153, 128));
		}
		g.fillRect(sumX, sumY, w, h);
		g.setColor(Color.white);
		g.drawRect(sumX, sumY, w, h);
		g.drawString(label, sumX + 8, sumY + 8);
		if (subMenu != null) {
			if (expanded) {
				for (MenuButton button : subMenu.buttons) {
					button.draw(g, camera);
				}
			}
		}
	}

	public boolean mouseOver(int x, int y) {
		if (mouseCollides(x, y)) {
			return true;
		} else {
			if (subMenu != null && expanded) {
				boolean changeExpanded = false;
				MenuButton expandedButton = null;
				for (MenuButton button : subMenu.buttons) {
					if (button.mouseOver(x, y)) {
						button.setExpanded(true);
						changeExpanded = true;
						expandedButton = button;
					}
				}
				if (changeExpanded) {
					for (MenuButton button : subMenu.buttons) {
						if (!button.equals(expandedButton)) {
							button.setExpanded(false);
						}
					}
					return true;
				}
			}
		}
		return false;
	}

	public MenuButton add(String string) {
		// If no subMenu has been initialized, create it now.
		if (subMenu == null) {
			subMenu = new Menu(x + w, y);
		}
		return subMenu.add(string);
	}

	public boolean hasSubMenu() {
		if (subMenu != null) {
			return true;
		}
		return false;
	}

	public void setExpanded(boolean b) {
		expanded = b;
	}

	public int getWidth() {
		return w;
	}

	public void setX(int rightX) {
		this.x = rightX;
		if (hasSubMenu()) {
			subMenu.x = rightX + w;
			for (MenuButton subButton : subMenu.buttons) {
				if (subButton.x < rightX + w) {
					subButton.setX(rightX + w);
				}
			}
		}
	}
}
