package view;

import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.newdawn.slick.Music;
import org.newdawn.slick.MusicListener;
import org.newdawn.slick.SlickException;

public class MusicPlayer implements MusicListener {

	List<Music> tracks;

	int currentSong;
	Music currentlyPlayingTrack;

	MusicPlayer() throws SlickException {
		tracks = new LinkedList<Music>();
		tracks.add(new Music("res/bgm/bgm1.ogg"));
		tracks.add(new Music("res/bgm/bgm2.ogg"));
		tracks.add(new Music("res/bgm/bgm3.ogg"));
	}

	public void startMusic() {
		currentlyPlayingTrack = tracks.get(currentSong);
		currentlyPlayingTrack.play();
		tracks.get(currentSong).addListener(this);
	}

	@Override
	public void musicEnded(Music music) {
		tracks.get(currentSong).removeListener(this);
		currentSong++;
		if (currentSong > tracks.size() - 1) {
			currentSong = 0;
		}
		Timer delayTimer = new Timer();
		delayTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				startMusic();
			}
		}, 30 * 1000);
	}

	@Override
	public void musicSwapped(Music music, Music newMusic) {
		//
	}

	public void stop() {
		if (currentlyPlayingTrack != null) {
			currentlyPlayingTrack.stop();
		}
	}
}
