package view;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class MiniButton extends Button {

	public MiniButton(int x, int y, String buttonLabel) {
		super(x, y, buttonLabel);
		this.h = 16;
		this.w = buttonLabel.length() * 9 + 16;
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(new Color(102, 153, 153));
		g.fillRect(x, y, w, h);
		g.setColor(Color.white);
		g.drawRect(x, y, w, h);
		g.drawString(label, x + 8, y);
	}
}
