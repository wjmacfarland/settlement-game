package view;

import java.awt.Point;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

public interface Drawable {

	public void draw(Graphics g, SpriteSheet tiles, Point camera);
	public void draw(Graphics g, SpriteSheet tiles, Point camera, Color lighting);

}
