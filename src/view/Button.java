package view;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

public class Button {

	public int x, y;
	public int w, h;
	public String label;

	public Button(int x, int y, String buttonLabel) {
		this.label = buttonLabel;
		this.x = x;
		this.y = y;
		this.h = 48;
		this.w = buttonLabel.length() * 9 + 32;
	}

	// Returns true if the mouse click occured within the button's dimensions
	public boolean wasClicked(int mouseX, int mouseY) {
		Rectangle buttonRect = new Rectangle(x, y, w, h);
		Rectangle mouseRect = new Rectangle(mouseX, mouseY, 1, 1);
		if (buttonRect.intersects(mouseRect)) {
			return true;
		}
		return false;
	}

	public void draw(Graphics g) {
		g.setColor(new Color(102, 153, 153));
		g.fillRect(x, y, w, h);
		g.setColor(Color.white);
		g.drawRect(x, y, w, h);
		g.drawString(label, x + 8, y + 8);
	}

	@Override
	public String toString() {
		return label;
	}

	public boolean doesRectCollide(Rectangle rect) {
		Rectangle buttonRect = new Rectangle(x, y, w, h);
		if (rect.intersects(buttonRect)) {
			return true;
		}
		return false;
	}
}
