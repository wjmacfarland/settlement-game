package view;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Graphics;

public class Menu {

	List<MenuButton> buttons;
	int x, y;

	Menu(int x, int y) {
		this.x = x;
		this.y = y;
		this.buttons = new LinkedList<MenuButton>();
	}

	public MenuButton click(int mouseX, int mouseY) {
		for (MenuButton button : buttons) {
			MenuButton clicked = button.click(mouseX, mouseY);
			if (clicked != null) {
				return clicked;
			}
		}
		return null;
	}

	public void mouseOver(int x, int y) {
		boolean changeExpanded = false;
		MenuButton expandedButton = null;
		for (MenuButton button : buttons) {
			if (button.mouseOver(x, y)) {
				button.setExpanded(true);
				changeExpanded = true;
				expandedButton = button;
			}
		}
		if (changeExpanded) {
			for (MenuButton button : buttons) {
				if (!button.equals(expandedButton)) {
					button.setExpanded(false);
				}
			}
		}
	}

	public void render(Graphics g, Point camera) {
		for (MenuButton button : buttons) {
			button.draw(g, camera);
		}
	}

	public MenuButton add(String string) {
		MenuButton buttonAdded = new MenuButton(string, x, y + buttons.size() * 32);
		for (MenuButton button : buttons) {
			if (button.hasSubMenu()) {
				button.subMenu.x = buttonAdded.x + buttonAdded.w;
				for (MenuButton subButton : button.subMenu.buttons) {
					if (subButton.x < buttonAdded.x + buttonAdded.w) {
						subButton.setX(buttonAdded.x + buttonAdded.w);
					}
				}
			}
		}
		buttons.add(buttonAdded);
		// Return the button just added
		return buttons.get(buttons.size() - 1);
	}

	public void remove(MenuButton buttonToRemove) {
		for (MenuButton button : buttons) {
			if (button == buttonToRemove) {
				buttons.remove(buttonToRemove);
				return;
			}
		}
	}
}
