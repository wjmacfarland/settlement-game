package view;

import helpers.MersenneTwister;

import java.awt.Point;
import java.util.LinkedList;
import launcher.Launcher;
import launcher.Settings;
import model.FoodCollection;
import model.GameMap;
import model.MetalCollection;
import model.SaveGameSystem;
import model.TerrainTile;
import model.WoodCollection;
import model.Units.UFO;
import model.Units.Unit;
import model.Units.UnitCollection;
import model.Units.UnitTaskCollection;
import model.constructs.AlienProtection;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;

import view.lighting.LightingEngine;
import view.panels.GameOverPanel;

public class GameView extends BasicGame {

	public static final int METAL_TO_WIN = 50;

	public static boolean gameOver;

	public static MersenneTwister RANDOM = new MersenneTwister();

	public static final int TILES_X = 200;
	public static final int TILES_Y = 200;

	public static SpriteSheet tiles;
	Image background;
	Sound ping;
	MusicPlayer musicPlayer;
	public GameGUI gui;

	Input input;

	public Point camera;

	LightingEngine lightingEngine;
	boolean lightingEnabled = true;

	UnitCollection units;
	public LinkedList<Unit> unitsToDraw;
	public LinkedList<UFO> enemies;
	float unitTimer = 0;
	float alienTimer = 0;
	public static final int ALIEN_FREQUENCY = 20000;

	// Variables for day/night cycle
	public float sunPhase = 0;

	final Settings gameSettings;

	public GameView(String title) {
		super(title);
		gameSettings = Launcher.getSettings();
	}

	public GameContainer container;

	public Image planet;
	public Image planetFrame;

	float sunlight;

	@Override
	public void init(GameContainer container) throws SlickException {

		// Set reference to container
		this.container = container;

		// Get input context
		input = container.getInput();

		// Initialize resources
		musicPlayer = new MusicPlayer();
		tiles = new SpriteSheet("res/gfx/Waldo.png", 32, 32);
		tiles.setFilter(Image.FILTER_NEAREST);
		planet = new Image("res/gfx/planet.png");
		planetFrame = new Image("res/gfx/planet_frame.png");
		background = new Image("res/gfx/background.png", false, Image.FILTER_NEAREST);
		AlienProtection.forcefield = new Image("res/gfx/forcefield.png");
		ping = new Sound("res/sfx/click.wav");
		lightingEngine = new LightingEngine();
		unitsToDraw = new LinkedList<Unit>();

		// Start music if enabled
		if (Launcher.getSettings().MUSIC_ENABLED) {
			musicPlayer.startMusic();
		}

		// Initialize objects
		GameMap.getInstance();
		camera = new Point(TILES_X * 16, TILES_Y * 16);
		units = new UnitCollection();
		gui = new GameGUI(units, input);

		// If the game loads successfully, we're done initializing
		if (SaveGameSystem.loadGame(this)) {
			return;
		}
		enemies = new LinkedList<UFO>();
		for (int i = 0; i < 5; i++) {
			enemies.add(new UFO(new Point(10 + 20 * i, 10 + 20 * i)));
		}
		// Spawn starting units
		GameMap.getInstance().spawnUnits(units, camera);
		for (Unit unit : units) {
			unitsToDraw.add(unit);
		}
	}

	@Override
	public void render(GameContainer container, Graphics g) throws SlickException {
		// Draw background
		drawParallaxBackground(g);

		sunlight = 0.5f + (float) (Math.sin(sunPhase) / 2);

		// Draw tiles with lighting if enabled
		int camXDiv = camera.x >> 5;
		int camYDiv = camera.y >> 5;
		for (int x = camXDiv; x < camXDiv + gameSettings.TILES_ON_SCREEN_X; x++) {
			for (int y = camYDiv; y < camYDiv + gameSettings.TILES_ON_SCREEN_Y; y++) {
				TerrainTile tile = GameMap.getInstance().get(x, y);
				if (tile == null) {
					continue;
				}
				float light = 0;
				if (lightingEnabled) {
					light = tile.getLightValue();
					// If the sunlight overpowers the local light
					if (sunlight > light) {
						light = sunlight;
					}
					Color lighting = new Color(light, light, light, 1);
					tile.draw(x, y, lighting, tiles, camera, g);
				} else {
					tile.draw(x, y, tiles, camera, g);
				}
			}
		}

		for (int x = camXDiv - 10; x < camXDiv + gameSettings.TILES_ON_SCREEN_X + 10; x++) {
			for (int y = camYDiv - 10; y < camYDiv + gameSettings.TILES_ON_SCREEN_Y + 10; y++) {
				TerrainTile tile = GameMap.getInstance().get(x, y);
				if (tile == null) {
					continue;
				}
				if (tile.protectorBuiltHere) {
					AlienProtection.forcefield.draw(x * 32 - camera.x - AlienProtection.forcefield.getWidth()
							/ 2, y * 32 - camera.y - AlienProtection.forcefield.getHeight() / 2, 1.0f,
							new Color(1, 1, 1, 0.10f));
				}
			}
		}

		// Draw the units
		for (Unit unit : units) {
			unit.draw(g, tiles, camera, sunlight);
		}
		for (UFO ufo : enemies) {
			ufo.draw(g, tiles, camera);
		}

		// Draw the debug panel and any informations that needs to be updated
		// per-frame
		if (Launcher.getSettings().DEBUG_INFO) {
			GameGUI.debugPanel.addInfo("FPS: " + container.getFPS());

			GameGUI.debugPanel.addInfo("Sun Phase: "
					+ ((int) (Math.sin(sunPhase) * 100) + " ( " + (int) Math.sin(sunPhase) + " )"));
			GameGUI.debugPanel.addInfo("Amount of Food: " + FoodCollection.getInstance().toString());
			GameGUI.debugPanel.addInfo("Amount of Wood: " + WoodCollection.getInstance().toString());
			GameGUI.debugPanel.addInfo("Amount of Wood: " + WoodCollection.getInstance().toString());
			GameGUI.debugPanel.addInfo("Amount of Metal: " + MetalCollection.getInstance().toString());
			GameGUI.debugPanel.addInfo("Units gathering: " + UnitCollection.getInstance().gatheringUnits);
			GameGUI.debugPanel.addInfo("Units building: " + UnitCollection.getInstance().buildingUnits);
			GameGUI.debugPanel.addInfo("Amount of tasks: "
					+ UnitTaskCollection.getInstance().getTotalAmtTasks());
			GameGUI.debugPanel.addInfo("Alien Countdown: "
					+ Math.round(((ALIEN_FREQUENCY - alienTimer % ALIEN_FREQUENCY) / 1000)));
			GameGUI.debugPanel.draw(container, g);
		} else {
			// Draw the minimap
			GameGUI.miniMap.draw(g, tiles, camera);
		}

		// Render the gui
		gui.render(this, g);

		// Render the planet rotation graphic
		planet.setRotation((sunPhase * 180) / 3.1415f - 90);
		int centerPlanetX = Launcher.getSettings().RESOLUTION_X / 2 - planet.getWidth() / 2;
		String timeString = "Current Time of Day";
		g.drawString(timeString, centerPlanetX + planet.getWidth() / 2 - timeString.length() * 4.5f, 40);
		planet.draw(centerPlanetX, 64);
		planetFrame.draw(centerPlanetX, 56);
	}

	public void exitGame() {
		musicPlayer.stop();
		container.exit();
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException {

		// Update the GUI
		gui.update(this, input);

		// Enable/disable debug info panel
		if (input.isKeyPressed(Input.KEY_3)) {
			Launcher.getSettings().DEBUG_INFO = !Launcher.getSettings().DEBUG_INFO;
		}

		// If the game isn't over yet
		if (!gameOver) {

			// Move camera
			if (input.isKeyDown(Input.KEY_A)) {
				camera.x -= 2 * delta;
			}
			if (input.isKeyDown(Input.KEY_D)) {
				camera.x += 2 * delta;
			}
			if (input.isKeyDown(Input.KEY_W)) {
				camera.y -= 2 * delta;
			}
			if (input.isKeyDown(Input.KEY_S)) {
				camera.y += 2 * delta;
			}

			// If escape menu is open, pause the game logic
			if (!GameGUI.escapeMenuOpen) {
				// Update units
				unitTimer += delta;
				alienTimer += delta;
				if (unitTimer > 200) {

					// Update lighting
					if (lightingEnabled) {
						sunPhase += 0.005;
						lightingEngine.updateLighting(camera, units, sunlight);
					}

					// Update units
					for (Unit unit : units) {
						boolean isAlive = unit.update();
						if (!isAlive) {
							units.addDeadUnit(unit);
						}
					}
					for (UFO ufo : enemies) {
						ufo.updateUFO();
					}

					// Clear dead units
					units.clearDeadUnits();
					unitsToDraw.removeAll(units.getDeadUnits());

					// Sort units
					units.sort();

					if (UnitTaskCollection.getInstance().getChangeState()) {
						int count = 0;
						for (Unit unit : units) {
							if (unit.getState().equals("Gathering")) {
								count++;
							}
						}
						if (count == 0) {
							UnitTaskCollection.getInstance().checkReachability();
						}
						UnitTaskCollection.getInstance().setChanged(false);

					}
					unitTimer %= 200;
				}
				if ((sunPhase > (2 * Math.PI)) && alienTimer > ALIEN_FREQUENCY) {
					for (Unit unit : units) {
						GameGUI.log(unit.getName()
								+ (unit.getTileBeneath().isSafe ? " is safe." : " is unprotected."));
						if (!unit.getTileBeneath().isSafe) {
							if (Math.random() > .9f) {
								unit.setDead(true);
								GameGUI.log(unit.getName() + " was abducted!");
								break;
							}
						}
					}
					alienTimer %= ALIEN_FREQUENCY;
				}

				// If lost, create gameOverPanel false (lose)
				if (UnitCollection.getInstance().size() == 0) {
					GameGUI.gameOverPanel = new GameOverPanel(false);
					gameOver = true;
					// Otherwise, if enough metal has been collected create
					// gameOverPanel true (win)
				} else if (MetalCollection.getInstance().size() >= METAL_TO_WIN) {
					GameGUI.gameOverPanel = new GameOverPanel(true);
					gameOver = true;
				}
			}
		}
	}

	// Draw the space parallax background
	private void drawParallaxBackground(Graphics g) {
		float parallaxOffsetX = (camera.x / 16.0F) % background.getWidth();
		float parallaxOffsetY = (camera.y / 16.0F) % background.getHeight();
		final int tileX = (int) (Math.ceil(gameSettings.RESOLUTION_X / background.getWidth()) + 2);
		final int tileY = (int) (Math.ceil(gameSettings.RESOLUTION_Y / background.getHeight()) + 2);
		for (int x = -1; x < tileX; x++) {
			for (int y = -1; y < tileY; y++) {
				background.draw(x * background.getWidth() - parallaxOffsetX, y * background.getHeight()
						- parallaxOffsetY);
			}
		}
		parallaxOffsetX = (camera.x / 12.0F) % background.getWidth();
		parallaxOffsetY = (camera.y / 12.0F) % background.getHeight();
		for (int x = -1; x < tileX; x++) {
			for (int y = -1; y < tileY; y++) {
				background.draw(x * background.getWidth() - parallaxOffsetX, y * background.getHeight()
						- parallaxOffsetY);
			}
		}
	}

	void restartGame() {
		gameOver = false;
		GameMap.getInstance().generateRandom();
		GameGUI.debugPanel.post("New map generated");
		GameGUI.miniMap.updateMiniMapTexture();
		units.clear();
		unitsToDraw.clear();
		GameMap.getInstance().spawnUnits(units, camera);
		for (Unit unit : units) {
			unitsToDraw.add(unit);
		}
		sunPhase = 0;
		WoodCollection.getInstance().clear();
		FoodCollection.getInstance().clear();
		MetalCollection.getInstance().clear();
		UnitTaskCollection.getInstance().clearGatheringTasks();
		UnitTaskCollection.getInstance().clearBuildingTasks();
		UnitTaskCollection.getInstance().clearOtherTasks();
		UnitTaskCollection.getInstance().setCurrentUnitsBuilding(0);
		UnitTaskCollection.getInstance().setCurrentUnitsGathering(0);
	}
}
