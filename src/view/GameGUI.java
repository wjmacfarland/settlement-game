package view;

import java.awt.Point;
import java.util.List;

import launcher.Launcher;
import model.FoodCollection;
import model.GameMap;
import model.SaveGameSystem;
import model.TerrainTile;
import model.WoodCollection;
import model.Zone;
import model.Zone.TileWrapper;
import model.Units.Unit;
import model.Units.UnitCollection;
import model.Units.UnitTaskCollection;
import model.Units.Tasks.GatherFoodTask;
import model.Units.Tasks.GatherMetalTask;
import model.Units.Tasks.GatherWoodTask;
import model.Units.Tasks.PlotBedTask;
import model.Units.Tasks.PlotCampfireTask;
import model.Units.Tasks.PlotFoodContainerTask;
import model.Units.Tasks.PlotMetalStorageTask;
import model.Units.Tasks.PlotProtectionTask;
import model.Units.Tasks.PlotWoodShedTask;
import model.Units.Tasks.PlotWorkbenchTask;
import model.buildings.Building;
import model.buildings.Building.BuildingType;
import model.constructs.AlienProtection;
import model.constructs.Bed;
import model.constructs.Campfire;
import model.constructs.FishingHole;
import model.constructs.FoodContainer;
import model.constructs.Harvestable;
import model.constructs.MetalStorage;
import model.constructs.Ore;
import model.constructs.Tree;
import model.constructs.WoodBin;
import model.constructs.Workbench;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

import view.panels.DebugInfoPanel;
import view.panels.EscapePanel;
import view.panels.GameOverPanel;
import view.panels.ResourcePanel;
import view.panels.RolePanel;

public class GameGUI {

	// Panels
	static GameOverPanel gameOverPanel;

	static EscapePanel escapePanel;
	public static DebugInfoPanel debugPanel = new DebugInfoPanel();
	static RolePanel rolePanel;
	static ResourcePanel resourcePanel;

	public static MiniMap miniMap;

	public static Input input;

	static boolean mouseIsDown = false;
	static Point mouseSelectStart;
	static Point mouseSelectEnd;

	static Zone mouseSelectZone;
	int woodCost;

	static boolean menuOpen = false;
	static Menu contextMenu;
	static Point rightClickTileCoords;

	static boolean escapeMenuOpen = false;

	static int unitPanelYOffset = 0;

	static boolean allowDragSelect = true;

	public GameGUI(UnitCollection units, Input input) {
		// buttons = new LinkedList<Button>();
		// addDefaultButtons();
		GameGUI.miniMap = new MiniMap(units);
		GameGUI.input = input;
		GameGUI.escapePanel = new EscapePanel();
		GameGUI.rolePanel = new RolePanel();
		GameGUI.resourcePanel = new ResourcePanel(GameView.tiles);
	}

	// Pass a string to the debugPanel
	public static void log(String s) {
		debugPanel.post(s);
	}

	public void render(GameView view, Graphics g) throws SlickException {
		for (Unit unit : view.unitsToDraw) {
			unit.unitPanel.draw(view.container, GameView.tiles, g);
		}

		drawMouseSelectZone(view, g);

		// Draw the right click menu if it's open
		if (escapeMenuOpen) {
			g.setColor(new Color(0, 0, 0, 64));
			g.fillRect(0, 0, Launcher.getSettings().RESOLUTION_X, Launcher.getSettings().RESOLUTION_Y);
			escapePanel.draw(g);
		} else if (menuOpen) {
			g.setColor(Color.yellow);
			g.drawRect(rightClickTileCoords.x * 32 - view.camera.x, rightClickTileCoords.y * 32
					- view.camera.y, 32, 32);
			contextMenu.render(g, view.camera);
		}

		rolePanel.draw(g);
		resourcePanel.draw(g);

		if (GameView.gameOver && gameOverPanel != null) {
			gameOverPanel.draw(g);
		}
	}

	private void drawMouseSelectZone(GameView view, Graphics g) {
		// Draw drag-select zone
		if (mouseSelectZone != null) {
			Rectangle mouseRect = new Rectangle(getMousedTileX(view.camera) + 0.25f,
					getMousedTileY(view.camera) + 0.25f, 0.5f, 0.5f);
			Rectangle selectZoneRect = mouseSelectZone.getRectangle();
			if (mouseRect.intersects(selectZoneRect)) {
				g.setColor(Color.white);
				g.drawRect(selectZoneRect.getX() * 32 - view.camera.x, selectZoneRect.getY() * 32
						- view.camera.y, selectZoneRect.getWidth() * 32, selectZoneRect.getHeight() * 32);
				g.drawRect(selectZoneRect.getX() * 32 - view.camera.x + 4, selectZoneRect.getY() * 32
						- view.camera.y + 4, selectZoneRect.getWidth() * 32 - 8,
						selectZoneRect.getHeight() * 32 - 8);
				g.setColor(new Color(0, 255, 0, 0.10f));
				g.fillRect(selectZoneRect.getX() * 32 - view.camera.x, selectZoneRect.getY() * 32
						- view.camera.y, selectZoneRect.getWidth() * 32, selectZoneRect.getHeight() * 32);
			} else {
				g.setColor(Color.yellow);
				g.drawRect(selectZoneRect.getX() * 32 - view.camera.x, selectZoneRect.getY() * 32
						- view.camera.y, selectZoneRect.getWidth() * 32, selectZoneRect.getHeight() * 32);
			}
			g.setColor(Color.yellow);
			g.drawRect(selectZoneRect.getX() * 32 - view.camera.x,
					selectZoneRect.getY() * 32 - view.camera.y, selectZoneRect.getWidth() * 32,
					selectZoneRect.getHeight() * 32);
		} else if (mouseSelectStart != null) {
			g.setColor(Color.yellow);
			int x1 = (int) Math.floor(mouseSelectStart.x / 32);
			int y1 = (int) Math.floor(mouseSelectStart.y / 32);
			int x2 = (int) Math.ceil(getAbsMouseX(view.camera) / 32);
			int y2 = (int) Math.ceil(getAbsMouseY(view.camera) / 32);
			Zone tempZone = new Zone(getRectangle(x1, y1, x2, y2));
			Rectangle tempRect = tempZone.getRectangle();
			g.drawRect(tempRect.getX() * 32 - view.camera.x, tempRect.getY() * 32 - view.camera.y,
					tempRect.getWidth() * 32, tempRect.getHeight() * 32);
		}
	}

	private Rectangle getRectangle(int x1, int y1, int x2, int y2) {
		if (x1 > x2) {
			int swap = x1;
			x1 = x2;
			x2 = swap;
		}
		if (y1 > y2) {
			int swap = y1;
			y1 = y2;
			y2 = swap;
		}
		int w = x2 - x1;
		int h = y2 - y1;
		return new Rectangle(x1, y1, w, h);
	}

	public void update(GameView view, Input input) {

		if (!GameView.gameOver)
			if (input.isKeyPressed(Input.KEY_ESCAPE)) {
				escapeMenuOpen = !escapeMenuOpen;
			}

		// Generate a new map when space is pressed
		if (input.isKeyPressed(Input.KEY_SPACE)) {
			view.restartGame();
		}

		int absMouseX = getAbsMouseX(view.camera);
		int absMouseY = getAbsMouseY(view.camera);

		// Mouse over sub-menus should open them
		if (menuOpen) {
			contextMenu.mouseOver(absMouseX, absMouseY);
		}

		if (input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
			if (GameView.gameOver) {
				String buttonClicked = gameOverPanel.sendClick(input.getMouseX(), input.getMouseY());
				switch (buttonClicked) {
				case "New Game":
					view.restartGame();
					break;
				case "Load Game":
					SaveGameSystem.loadGame(view);
					break;
				case "Quit Game":
					view.exitGame();
					break;
				}
				allowDragSelect = false;
				return;
			}
			if (escapeMenuOpen) {
				String buttonClicked = escapePanel.sendClick(input.getMouseX(), input.getMouseY());
				switch (buttonClicked) {
				case "Save Game":
					if (!GameView.gameOver)
						SaveGameSystem.saveGame(view);
					break;
				case "Load Game":
					SaveGameSystem.loadGame(view);
					break;
				case "Quit Game":
					view.exitGame();
					break;
				}
				allowDragSelect = false;
				return;
			} else if (menuOpen) {
				MenuButton clickedButton = contextMenu.click(absMouseX, absMouseY);
				// If a button was clicked
				if (clickedButton != null) {
					debugPanel.post("Button clicked: " + clickedButton.label);
					switch (clickedButton.label) {
					case "Spawn unit: " + 10:
						if (FoodCollection.getInstance().size() >= 10) {
							Unit unitToAdd = new Unit(rightClickTileCoords.x, rightClickTileCoords.y);
							view.units.add(unitToAdd);
							view.unitsToDraw.add(unitToAdd);
							for (int i = 0; i < 10; i++) {
								FoodCollection.getInstance().get();
							}
						}
						break;
					// Chop a single tree
					case "Chop down":
						UnitTaskCollection.getInstance().addGatheringTask(
								new GatherWoodTask(rightClickTileCoords.x, rightClickTileCoords.y));
						Harvestable harvestable = (Harvestable) GameMap.getInstance().get(
								rightClickTileCoords.x, rightClickTileCoords.y).construct;
						harvestable.setMarkedForHarvest(true);
						break;
					// Chop a zone of trees
					case "Chop Trees":
						List<TileWrapper> tilesInZone = mouseSelectZone.getTiles();
						for (TileWrapper tileWrapper : tilesInZone) {
							TerrainTile tileInZone = tileWrapper.tile;
							if (tileInZone.construct != null && tileInZone.construct instanceof Tree) {
								UnitTaskCollection.getInstance().addGatheringTask(
										new GatherWoodTask(tileWrapper.x, tileWrapper.y));
								Harvestable harvestable2 = (Harvestable) tileWrapper.tile.construct;
								harvestable2.setMarkedForHarvest(true);
							}
						}
						break;
					case "Get Fish":
						List<TileWrapper> fishesInZone = mouseSelectZone.getTiles();
						for (TileWrapper tileWrapper : fishesInZone) {
							TerrainTile fishInZone = tileWrapper.tile;
							if (fishInZone.construct != null && fishInZone.construct instanceof FishingHole) {
								UnitTaskCollection.getInstance().addGatheringTask(
										new GatherFoodTask(tileWrapper.x, tileWrapper.y));
								Harvestable harvestable2 = (Harvestable) tileWrapper.tile.construct;
								harvestable2.setMarkedForHarvest(true);
							}
						}
						break;
					case "Get Ore":
						List<TileWrapper> oreInZone = mouseSelectZone.getTiles();
						for (TileWrapper tileWrapper : oreInZone) {
							TerrainTile metalInZone = tileWrapper.tile;
							if (metalInZone.construct != null && metalInZone.construct instanceof Ore) {
								UnitTaskCollection.getInstance().addGatheringTask(
										new GatherMetalTask(tileWrapper.x, tileWrapper.y));
								Harvestable harvestable2 = (Harvestable) tileWrapper.tile.construct;
								harvestable2.setMarkedForHarvest(true);
							}
						}

						break;
					case "Food Container: " + FoodContainer.WOOD_TO_BUILD:
						if (WoodCollection.getInstance().size() >= FoodContainer.WOOD_TO_BUILD) {
							UnitTaskCollection.getInstance()
									.addBuildingTask(
											new PlotFoodContainerTask(rightClickTileCoords.x,
													rightClickTileCoords.y));
							WoodCollection.getInstance().get(FoodContainer.WOOD_TO_BUILD);
						}

						break;
					case "Metal Storage: " + MetalStorage.WOOD_TO_BUILD:
						if (WoodCollection.getInstance().size() >= MetalStorage.WOOD_TO_BUILD) {
							UnitTaskCollection.getInstance().addBuildingTask(
									new PlotMetalStorageTask(rightClickTileCoords.x, rightClickTileCoords.y));
							WoodCollection.getInstance().get(MetalStorage.WOOD_TO_BUILD);
						}

						break;
					case "Harvest Apples":
						UnitTaskCollection.getInstance().addGatheringTask(
								new GatherFoodTask(rightClickTileCoords.x, rightClickTileCoords.y));
						break;
					case "Bed: " + Bed.WOOD_TO_BUILD:
						if (WoodCollection.getInstance().size() >= Bed.WOOD_TO_BUILD) {
							UnitTaskCollection.getInstance().addBuildingTask(
									new PlotBedTask(rightClickTileCoords.x, rightClickTileCoords.y));
							WoodCollection.getInstance().get(Bed.WOOD_TO_BUILD);
						}
						break;
					case "Wood Shed: " + WoodBin.WOOD_TO_BUILD:
						if (WoodCollection.getInstance().size() >= WoodBin.WOOD_TO_BUILD) {
							UnitTaskCollection.getInstance().addBuildingTask(
									new PlotWoodShedTask(rightClickTileCoords.x, rightClickTileCoords.y));
							WoodCollection.getInstance().get(WoodBin.WOOD_TO_BUILD);
						}
						break;
					case "Go Fishing":
						UnitTaskCollection.getInstance().addGatheringTask(
								new GatherFoodTask(rightClickTileCoords.x, rightClickTileCoords.y));
						break;
					case "WorkBench: " + Workbench.WOOD_TO_BUILD:
						if (WoodCollection.getInstance().size() >= Workbench.WOOD_TO_BUILD) {
							UnitTaskCollection.getInstance().addBuildingTask(
									new PlotWorkbenchTask(rightClickTileCoords.x, rightClickTileCoords.y));
							WoodCollection.getInstance().get(Workbench.WOOD_TO_BUILD);
						}
						break;
					case "Campfire: " + Campfire.WOOD_TO_BUILD:
						if (WoodCollection.getInstance().size() >= Campfire.WOOD_TO_BUILD) {
							UnitTaskCollection.getInstance().addBuildingTask(
									new PlotCampfireTask(rightClickTileCoords.x, rightClickTileCoords.y));
							WoodCollection.getInstance().get(Campfire.WOOD_TO_BUILD);
						}
						break;
					case "Alien Protection: " + AlienProtection.WOOD_TO_BUILD:
						if (WoodCollection.getInstance().size() >= AlienProtection.WOOD_TO_BUILD) {
							UnitTaskCollection.getInstance().addBuildingTask(
									new PlotProtectionTask(rightClickTileCoords.x, rightClickTileCoords.y));
							WoodCollection.getInstance().get(AlienProtection.WOOD_TO_BUILD);
						}
						break;
					case "Mine Ore":
						UnitTaskCollection.getInstance().addGatheringTask(
								new GatherMetalTask(rightClickTileCoords.x, rightClickTileCoords.y));
						break;
					case "Clear Tasks":
						UnitTaskCollection.getInstance().clearAllTasks();
						// unmark constructs for harvest
						for (int x = 0; x < GameView.TILES_X; x++) {
							for (int y = 0; y < GameView.TILES_Y; y++) {
								TerrainTile tile = GameMap.getInstance().get(x, y);
								if (tile.construct != null && tile.construct instanceof Harvestable) {
									Harvestable harvestable3 = (Harvestable) tile.construct;
									harvestable3.setMarkedForHarvest(false);
								}
							}
						}
						break;

					// Buildings
					case "House":
						if (WoodCollection.getInstance().size() >= woodCost) {
							Building house = new Building(BuildingType.HOUSE, mouseSelectZone);
							List<TileWrapper> houseTiles = mouseSelectZone.getTiles();
							for (TileWrapper tileWrapper : houseTiles) {
								TerrainTile houseTile = tileWrapper.tile;
								houseTile.setHouse(true);
							}
							WoodCollection.getInstance().get(woodCost);

						}
						break;

					case "Storage Room":
						if (WoodCollection.getInstance().size() >= woodCost) {
							Building house = new Building(BuildingType.STORAGE, mouseSelectZone);
							List<TileWrapper> houseTiles = mouseSelectZone.getTiles();
							for (TileWrapper tileWrapper : houseTiles) {
								TerrainTile houseTile = tileWrapper.tile;
								houseTile.setStorage(true);

							}
							WoodCollection.getInstance().get(woodCost);

						}
						break;
					case "Workshop":
						if (WoodCollection.getInstance().size() >= woodCost) {
							Building house = new Building(BuildingType.WORKSHOP, mouseSelectZone);
							List<TileWrapper> houseTiles = mouseSelectZone.getTiles();
							for (TileWrapper tileWrapper : houseTiles) {
								TerrainTile houseTile = tileWrapper.tile;
								houseTile.isWorkshop(true);

							}
							WoodCollection.getInstance().get(woodCost);

						}
						break;
					}
					// Disallow drag select
					allowDragSelect = false;
				}
				menuOpen = false;
			} else {
				// Send mouse click to role panel
				if (rolePanel.sendClick(input.getMouseX(), input.getMouseY())) {
					allowDragSelect = false;
				}

				for (Unit unit : view.unitsToDraw) {
					unit.unitPanel.getUnitOfPanelClicked(input.getMouseX(), input.getMouseY(), view.camera);
				}

			}
		}

		if (input.isMousePressed(Input.MOUSE_RIGHT_BUTTON)) {
			view.ping.play(0.8f, 0.5f);
			int x = getMousedTileX(view.camera);
			int y = getMousedTileY(view.camera);
			rightClickTileCoords = new Point(x, y);
			contextMenu = new Menu(x * 32 + 48, y * 32);
			TerrainTile tile = GameMap.getInstance().get(rightClickTileCoords);
			// If the right mouse click occurred within the drag select zone
			if (mouseSelectZone != null
					&& mouseSelectZone.isClickInZone(view.camera, input.getMouseX(), input.getMouseY())) {
				MenuButton zoneOptions = contextMenu.add("Zone...");
				List<TileWrapper> tilesInZone = mouseSelectZone.getTiles();
				boolean treeInZone = false;
				boolean fishInZone = false;
				boolean oreInZone = false;
				boolean apples = false;
				boolean allTilesBuildable = true;
				for (TileWrapper tileWrapper : tilesInZone) {
					TerrainTile tileInZone = tileWrapper.tile;
					if (!tileInZone.isValidConstructionSite()) {
						allTilesBuildable = false;
					}
					if (tileInZone.construct != null && tileInZone.construct instanceof Tree) {
						treeInZone = true;
						Tree tree = (Tree) tileInZone.construct;
						if (tree.getApples() > 0) {
							apples = true;
						}
					}
					if (tileInZone.construct != null && tileInZone.construct instanceof FishingHole) {
						fishInZone = true;
					}
					if (tileInZone.construct != null && tileInZone.construct instanceof Ore) {
						oreInZone = true;
					}
				}

				woodCost = (int) ((int) (2 * mouseSelectZone.getRectangle().getHeight())
						+ (2 * mouseSelectZone.getRectangle().getWidth()) - 5);

				if (treeInZone) {
					zoneOptions.add("Chop Trees");
				}
				if (apples) {
					zoneOptions.add("Harvest Apple Trees");
				}
				if (fishInZone) {
					zoneOptions.add("Get Fish");
				}
				if (oreInZone) {
					zoneOptions.add("Get Ore");
				}
				if (allTilesBuildable) {
					MenuButton buildZone = zoneOptions.add("Build... (costs " + woodCost + " wood");
					buildZone.add("House");
					buildZone.add("Storage Room");
					buildZone.add("Workshop");

				}
				// If no buttons were added above remove the zone options
				// sub-menu
				if (zoneOptions.subMenu == null) {
					contextMenu.remove(zoneOptions);
				}
			}
			if (tile != null) {
				if (tile.isWalkable()) {
					contextMenu.add("Spawn unit: " + 10);
				}
				// If tile has a construct
				if (tile.construct != null) {
					if (tile.construct instanceof Tree) {
						if (tile.construct instanceof Tree) {
							Tree tree = (Tree) tile.construct;
							if (tree.getApples() > 0) {
								contextMenu.add("Harvest Apples");
							}
							contextMenu.add("Chop down");
						}
					} else if (tile.construct instanceof FishingHole) {
						contextMenu.add("Go Fishing");
					} else if (tile.construct instanceof Ore) {
						contextMenu.add("Mine Ore");
					}
				}
				if (tile.isValidConstructionSite()) {
					MenuButton buildButton = contextMenu.add("Build...");
					buildButton.add("Campfire: " + Campfire.WOOD_TO_BUILD);
					buildButton.add("Alien Protection: " + AlienProtection.WOOD_TO_BUILD);
					if (tile.isHouse) {
						buildButton.add("Bed: " + Bed.WOOD_TO_BUILD);
					}
					if (tile.isStorage) {
						buildButton.add("Wood Shed: " + WoodBin.WOOD_TO_BUILD);
						buildButton.add("Food Container: " + FoodContainer.WOOD_TO_BUILD);
						buildButton.add("Metal Storage: " + MetalStorage.WOOD_TO_BUILD);
					}
					if (tile.isWorkshop) {
						buildButton.add("WorkBench: " + Workbench.WOOD_TO_BUILD);
					}
				}
			}
			contextMenu.add("Clear Tasks");
			menuOpen = true;
		}

		// If the right click menu isn't open, then allow drag selection
		if (!menuOpen)
			updateMouseSelectZone(view);

		int sumOffset = unitPanelYOffset;
		for (Unit unit : view.unitsToDraw) {
			sumOffset = unit.unitPanel.setYOffset(sumOffset);
		}

		// Allow scrolling the unit panels
		if (input.isKeyDown(Input.KEY_UP)) {
			unitPanelYOffset += 3;
			if (unitPanelYOffset > 0) {
				unitPanelYOffset = 0;
			}
		}

		if (input.isKeyDown(Input.KEY_DOWN)) {
			unitPanelYOffset -= 3;
		}
	}

	// These getters take into account the offset of the camera and return the
	// coordinates of the tile clicked on
	private int getMousedTileX(Point camera) {
		return (input.getMouseX() + camera.x) / 32;
	}

	public int getMousedTileY(Point camera) {
		return (input.getMouseY() + camera.y) / 32;
	}

	private int getAbsMouseX(Point camera) {
		return input.getMouseX() + camera.x;
	}

	private int getAbsMouseY(Point camera) {
		return input.getMouseY() + camera.y;
	}

	private void updateMouseSelectZone(GameView view) {
		// Drag to select a zone
		if (input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON)) {
			if (allowDragSelect) {
				if (mouseSelectStart == null) {
					mouseSelectStart = new Point(getAbsMouseX(view.camera), getAbsMouseY(view.camera));
					mouseSelectZone = null;
				}
				mouseIsDown = true;
			}
		} else {
			if (mouseSelectStart != null && mouseSelectEnd == null) {
				mouseSelectEnd = new Point(getAbsMouseX(view.camera), getAbsMouseY(view.camera));
				if (mouseSelectEnd.x == mouseSelectStart.x) {
					mouseSelectEnd.x++;
				}
				if (mouseSelectEnd.y == mouseSelectStart.y) {
					mouseSelectEnd.y++;
				}
			}
			mouseIsDown = false;
			allowDragSelect = true;
		}

		if (mouseSelectStart != null && mouseSelectEnd != null) {
			int x1 = (int) Math.floor(mouseSelectStart.x / 32);
			int y1 = (int) Math.floor(mouseSelectStart.y / 32);
			int x2 = (int) Math.ceil(mouseSelectEnd.x / 32);
			int y2 = (int) Math.ceil(mouseSelectEnd.y / 32);
			Rectangle zoneRect = getRectangle(x1, y1, x2, y2);
			if (zoneRect.getWidth() >= 1 && zoneRect.getHeight() >= 1) {
				mouseSelectZone = new Zone(zoneRect);
				Rectangle mouseZoneR = mouseSelectZone.getRectangle();
				debugPanel.post("x: " + mouseZoneR.getX());
				debugPanel.post("y: " + mouseZoneR.getY());
				debugPanel.post("w: " + mouseZoneR.getWidth());
				debugPanel.post("h: " + mouseZoneR.getHeight());
			}
			mouseSelectStart = null;
			mouseSelectEnd = null;
		}
	}
}
