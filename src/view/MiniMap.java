package view;

import java.awt.Dimension;
import java.awt.Point;

import launcher.Launcher;
import model.GameMap;
import model.Units.Unit;
import model.Units.UnitCollection;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class MiniMap implements Drawable {

	// Size of a single tile on the mini-map
	private int tileSize = 1;

	// Location and dimensions of mini-map panel
	private Point location;
	private Dimension dimensions;

	private Image miniMapImage;
	Graphics imageGraphics = null;

	private UnitCollection units;

	public MiniMap(UnitCollection units) {
		this.units = units;

		// if (Launcher.getSettings().RESOLUTION_X > 1200) {
		// this.tileSize = 2;
		// }

		// Set dimensions of mini-map
		dimensions = new Dimension();
		dimensions.width = GameView.TILES_X * tileSize;
		dimensions.height = GameView.TILES_Y * tileSize;

		// Set position of mini-map
		location = new Point();
		location.x = 16;
		location.y = 16;

		// Attempt to create an image to store the mini-map
		try {
			miniMapImage = new Image(dimensions.width * tileSize, dimensions.height * tileSize);
			imageGraphics = miniMapImage.getGraphics();
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		updateMiniMapTexture();
	}

	public void updateMiniMapTexture() {
		imageGraphics.clear();
		// Draw tiles to miniMapImage
		for (int x = 0; x < GameView.TILES_X; x++) {
			for (int y = 0; y < GameView.TILES_Y; y++) {
				imageGraphics.setColor(GameMap.getInstance().get(x, y).getTileColor());
				if (imageGraphics.getColor() != Color.transparent)
					imageGraphics.fillRect(x * tileSize, y * tileSize, tileSize, tileSize);
			}
		}
		// Flush this graphics context to the underlying rendering context
		// (otherwise the image will not draw)
		imageGraphics.flush();
	}

	@Override
	public void draw(Graphics g, SpriteSheet tiles, Point camera) {

		// Draw mini-map texture (of terrain)
		g.drawImage(miniMapImage, location.x, location.y);

		// Draw units as yellow squares
		g.setColor(Color.yellow);
		for (Unit unit : units) {
			g.fillRect(unit.getLocation().x * tileSize - 2 + location.x, unit.getLocation().y * tileSize - 2
					+ location.y, 4, 4);
		}

		// Draw rectangle to show vision of camera on map
		g.drawRect(camera.x / 32 * tileSize + location.x, camera.y / 32 * tileSize + location.y,
				Launcher.getSettings().TILES_ON_SCREEN_X * tileSize, Launcher.getSettings().TILES_ON_SCREEN_Y
						* tileSize);
		g.setColor(Color.black);
		g.drawRect(camera.x / 32 * tileSize - 1 + location.x, camera.y / 32 * tileSize - 1 + location.y,
				Launcher.getSettings().TILES_ON_SCREEN_X * tileSize + 2,
				Launcher.getSettings().TILES_ON_SCREEN_Y * tileSize + 2);
	}

	@Override
	public void draw(Graphics g, SpriteSheet tiles, Point camera, Color lighting) {
		// Call the non-lighting based draw function
		this.draw(g, tiles, camera);
	}
}
