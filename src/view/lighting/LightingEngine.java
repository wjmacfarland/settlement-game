package view.lighting;

import helpers.HelperFunctions;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import view.GameView;
import model.GameMap;
import model.TerrainTile;
import model.Units.Unit;
import model.Units.UnitCollection;

public class LightingEngine {

	public LightingEngine() {
		lightEmitters = new LinkedList<LightEmitter>();
	}

	public static List<LightEmitter> lightEmitters;

	class Direction {

		int deltaX, deltaY;

		Direction(int deltaX, int deltaY) {
			this.deltaX = deltaX;
			this.deltaY = deltaY;
		}
	}

	// Per-frame changes to apply to lighting
	public void updateLighting(Point camera, UnitCollection units, float sunlight) {
		Point mapSize = GameMap.getInstance().getSize();
		for (int x = 0; x < mapSize.x; x++) {
			for (int y = 0; y < mapSize.y; y++) {
				GameMap.getInstance().get(x, y).setLightValue(sunlight);
			}
		}
		for (LightEmitter light : lightEmitters) {
			calculateFOV(light, camera);
		}
		for (Unit u : units) {
			if (u.getLight() != null) {
				calculateFOV(u.getLight(), camera);
			}
		}
	}

	// Returns a 2D array of float values where 0 represents no light and 1
	// represents full light
	public void calculateFOV(LightEmitter light, Point camera) {

		int cameraTileX = camera.x / 32;
		int cameraTileY = camera.y / 32;

		// If light exists outside of possible view of the camera, skip
		// calculating for it
		if (light.loc.x + light.getRadius() < cameraTileX)
			return;
		if (light.loc.y + light.getRadius() < cameraTileY)
			return;
		if (light.loc.x - light.getRadius() > cameraTileX + GameView.TILES_X)
			return;
		if (light.loc.y - light.getRadius() > cameraTileY + GameView.TILES_Y)
			return;

		Direction[] directions = new Direction[8];
		directions[0] = new Direction(-1, 0);
		directions[1] = new Direction(-1, 1);
		directions[2] = new Direction(0, 1);
		directions[3] = new Direction(1, 1);
		directions[4] = new Direction(1, 0);
		directions[5] = new Direction(1, -1);
		directions[6] = new Direction(0, -1);
		directions[7] = new Direction(-1, -1);

		// Make sure light is in valid range
		if (!(HelperFunctions.valueInRange(light.loc.x, 0, GameMap.getInstance().getSize().x) && HelperFunctions
				.valueInRange(light.loc.y, 0, GameMap.getInstance().getSize().y))) {
			return;
		}

		for (Direction d : directions) {
			castLight(light, 1, 1.0f, 0.0f, 0, d.deltaX, d.deltaY, 0);
			castLight(light, 1, 1.0f, 0.0f, d.deltaX, 0, 0, d.deltaY);
		}
	}

	/**
	 * @Credit Eben Howard http://www.roguebasin.com/index.php?title=
	 *         FOV_using_recursive_shadowcasting_-_improved
	 */
	private void castLight(LightEmitter light, int row, float start, float end, int xx, int xy, int yx, int yy) {
		float newStart = 0.0f;
		if (start < end) {
			return;
		}
		boolean blocked = false;
		for (int distance = row; distance <= light.getRadius() && !blocked; distance++) {
			int deltaY = -distance;
			for (int deltaX = -distance; deltaX <= 0; deltaX++) {
				int currentX = light.loc.x + deltaX * xx + deltaY * xy;
				int currentY = light.loc.y + deltaX * yx + deltaY * yy;
				if (!HelperFunctions.valueInRange(currentX, 0, GameView.TILES_X))
					continue;
				if (!HelperFunctions.valueInRange(currentY, 0, GameView.TILES_Y))
					continue;
				TerrainTile tile = GameMap.getInstance().get(currentX, currentY);
				float leftSlope = (deltaX - 0.5f) / (deltaY + 0.5f);
				float rightSlope = (deltaX + 0.5f) / (deltaY - 0.5f);

				if (!(currentX >= 0 && currentY >= 0 && currentX < GameMap.getInstance().getSize().x && currentY < GameMap
						.getInstance().getSize().y) || start < rightSlope) {
					continue;
				} else if (end > leftSlope) {
					break;
				}

				// check if it's within the lightable area and light if needed
				if (light.getrStrat().radius(deltaX, deltaY) <= light.getRadius()) {
					Float bright = new Float(
							(1 - (light.getrStrat().radius(deltaX, deltaY) / light.getRadius())));
					if (tile.getLightValue() < bright)
						tile.setLightValue(bright);
				}

				if (blocked) { // previous cell was a blocking one
					// hit a wall
					if (tile.blocksVision()) {
						newStart = rightSlope;
						continue;
					} else {
						blocked = false;
						start = newStart;
					}
				} else {
					// hit wall within sight
					if (tile.blocksVision() && distance < light.getRadius()) {
						blocked = true;
						castLight(light, distance + 1, start, leftSlope, xx, xy, yx, yy);
						newStart = rightSlope;
					}
				}
			}
		}
	}
}
