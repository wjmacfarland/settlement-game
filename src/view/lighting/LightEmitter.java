package view.lighting;

import java.awt.Point;
import java.io.Serializable;

public class LightEmitter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Point loc;

	private int radius;

	private LightRadiusStrategy rStrat;

	public LightEmitter(Point location, int radius, LightRadiusStrategy rStrat) {
		loc = location;
		this.radius = radius;
		this.setrStrat(rStrat);
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public LightRadiusStrategy getrStrat() {
		return rStrat;
	}

	public void setrStrat(LightRadiusStrategy rStrat) {
		this.rStrat = rStrat;
	}
}
