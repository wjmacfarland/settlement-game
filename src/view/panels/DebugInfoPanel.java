package view.panels;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import launcher.Launcher;
import launcher.Settings;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class DebugInfoPanel {

	// Private variables
	private static int panelX;
	private static int panelWidth;
	private static int panelHeight;
	private static int infoLine = 0;
	private static int longestString = 0;

	private ArrayList<String> dynamicInformation;

	private Queue<String> log;

	final Settings gameSettings;

	public DebugInfoPanel() {
		gameSettings = Launcher.getSettings();
		DebugInfoPanel.panelWidth = 0;
		DebugInfoPanel.panelHeight = (gameSettings.TILES_ON_SCREEN_Y / 2) * 32;
		DebugInfoPanel.panelX = 16;
		dynamicInformation = new ArrayList<String>();
		log = new LinkedList<String>();
	}

	public void draw(GameContainer container, Graphics g) throws SlickException {
		DebugInfoPanel.infoLine = 0;
		longestString = 0;
		g.setColor(new Color(0, 0, 0, 0.5f));
		g.fillRect(panelX, 16, panelWidth, panelHeight);
		g.setColor(Color.white);
		g.drawRect(panelX, 16, panelWidth, panelHeight);

		// Draw dynamic info
		drawInfo("Debug Info", g);
		drawInfo("", g);
		for (String s : dynamicInformation) {
			int len = drawInfo(s, g);
			if (len > longestString) {
				longestString = len;
			}
		}

		// Draw log
		drawInfo("", g);
		drawInfo("Log", g);
		for (String s : log) {
			int len = drawInfo(s, g);
			if (len > longestString) {
				longestString = len;
			}
		}

		// Clear the info to draw and reset panelWidth
		dynamicInformation.clear();
		DebugInfoPanel.panelWidth = longestString;
		longestString = 0;
	}

	private int drawInfo(String s, Graphics g) {
		int stringLength = s.length() * 9 + 32;
		g.drawString(s, panelX + 16, 32 + (infoLine * 16));
		if (stringLength > DebugInfoPanel.panelWidth) {
			DebugInfoPanel.panelWidth = s.length() * 9 + 32;
		}
		DebugInfoPanel.infoLine++;
		// Resize panel to size of content
		DebugInfoPanel.panelHeight = infoLine * 16 + 32;
		return stringLength;
	}

	public void addInfo(String s) {
		dynamicInformation.add(s);
	}

	public void post(String s) {
		log.add(s);

		while (DebugInfoPanel.panelHeight + (log.size() * 16) > Launcher.getSettings().RESOLUTION_Y - 128) {
			log.remove();
		}
	}
}
