package view.panels;

import java.util.LinkedList;
import java.util.List;

import launcher.Launcher;
import model.Units.UnitTaskCollection;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

import view.Button;
import view.GameGUI;
import view.MiniButton;

public class RolePanel {

	private class RoleRow {
		private String label;
		private int y;

		MiniButton increase;
		MiniButton decrease;

		public RoleRow(int x, int y, String label) {
			this.y = y;
			this.label = label;
			increase = new MiniButton(w - 64, y, "^");
			decrease = new MiniButton(w - 32, y, "v");
		}

		public void draw(Graphics g) {
			String amount = "";
			switch (label) {
			case "Gatherers - ":
				amount = " " + UnitTaskCollection.getInstance().unitsToGather;
				break;
			case "Builders - ":
				amount = " " + UnitTaskCollection.getInstance().unitsToBuild;
				break;
			}
			g.drawString(label + amount, endOfFirstColumnX - label.length() * 9 - 4, y);
			increase.draw(g);
			decrease.draw(g);
		}
	}

	int x, y, w, h;

	private List<RoleRow> rows;

	int minX, minY, minW, minH;

	int endOfFirstColumnX;

	private Button minimizeButton;

	boolean minimized;

	public RolePanel() {
		// Create buttons
		rows = new LinkedList<RoleRow>();
		minimized = true;
		x = 16;
		minX = x;
		y = Launcher.getSettings().RESOLUTION_Y - 48;
		minY = y;
		w = Launcher.getSettings().RESOLUTION_X / 3 - 32;
		minW = w;
		h = 32;
		minH = h;

		endOfFirstColumnX = w / 2;

		minimizeButton = new MiniButton(x + 4, y + 4, "-");

		rows.add(new RoleRow(x + 16, Launcher.getSettings().RESOLUTION_Y - 144 + 32 + 32 * rows.size(),
				"Gatherers - "));
		rows.add(new RoleRow(x + 16, Launcher.getSettings().RESOLUTION_Y - 144 + 32 + 32 * rows.size(),
				"Builders - "));
	}

	public void draw(Graphics g) {
		g.setColor(Color.white);
		g.setColor(new Color(0, 0, 0, 0.5f));
		g.fillRect(x, y, w, h);
		g.setColor(Color.white);
		g.drawRect(x, y, w, h);
		g.drawString("Role Assignments", x + 32, y + 8);
		if (!minimized) {
			for (RoleRow row : rows) {
				row.draw(g);
			}
		}
		minimizeButton.draw(g);
	}

	public boolean sendClick(int absMouseX, int absMouseY) {
		Rectangle mouse = new Rectangle(absMouseX, absMouseY, 1, 1);
		Rectangle minRect = new Rectangle(minimizeButton.x, minimizeButton.y, minimizeButton.w,
				minimizeButton.h);
		if (mouse.intersects(minRect)) {
			if (minimized) {
				minimizeButton.y = Launcher.getSettings().RESOLUTION_Y - 140;
				h = 128;
				y = Launcher.getSettings().RESOLUTION_Y - 144;
				minimized = false;
			} else {
				minimizeButton.y = Launcher.getSettings().RESOLUTION_Y - 44;
				h = minH;
				y = minY;
				minimized = true;
			}
			return true;
		}
		if (!minimized) {
			for (RoleRow row : rows) {
				if (row.increase.doesRectCollide(mouse)) {
					// Increase workers
					if (row.label.equals("Gatherers - ")) {
						UnitTaskCollection.getInstance().adjustUnitsToGather(1);
						GameGUI.log("Gatherers increased (now: "
								+ UnitTaskCollection.getInstance().unitsToGather + ")");
					} else if (row.label.equals("Builders - ")) {
						UnitTaskCollection.getInstance().adjustUnitsToBuild(1);
						GameGUI.log("Builders increased (now: "
								+ UnitTaskCollection.getInstance().unitsToBuild + ")");
					}
				} else if (row.decrease.doesRectCollide(mouse)) {
					// Decrease workers
					if (row.label.equals("Gatherers - ")) {
						UnitTaskCollection.getInstance().adjustUnitsToGather(-1);
						GameGUI.log("Gatherers decreased (now: "
								+ UnitTaskCollection.getInstance().unitsToGather + ")");
					} else if (row.label.equals("Builders - ")) {
						UnitTaskCollection.getInstance().adjustUnitsToBuild(-1);
						GameGUI.log("Builders decreased (now: "
								+ UnitTaskCollection.getInstance().unitsToBuild + ")");
					}
				}
			}
		}
		return false;
	}
}
