package view.panels;

import java.awt.Point;
import java.math.BigDecimal;
import java.math.RoundingMode;

import launcher.Launcher;
import launcher.Settings;
import model.Units.Unit;
import model.resource.Item;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;

public class UnitPanel {

	// Unit to display information on
	private Unit unit;

	// Private variables
	private int panelX;
	private int panelWidth;
	private int panelHeight;
	private int infoLine = 0;
	private int yOffset;

	final Settings gameSettings;

	public UnitPanel(Unit unit) {
		gameSettings = Launcher.getSettings();
		this.unit = unit;
		this.panelWidth = 0;
		this.panelHeight = (Launcher.settings.TILES_ON_SCREEN_Y / 2) * 32;
		this.panelX = gameSettings.RESOLUTION_X - panelWidth - 16;
	}

	public void setDisplayedUnit(Unit unit) {
		this.unit = unit;
	}

	public Unit getDisplayedUnit() {
		return this.unit;
	}

	public void draw(GameContainer container, SpriteSheet tiles, Graphics g) throws SlickException {
		infoLine = 0;
		g.setColor(new Color(0, 0, 0, 0.5f));
		g.fillRect(panelX, yOffset + 16, panelWidth, panelHeight);
		g.setColor(Color.white);
		g.drawRect(panelX, yOffset + 16, panelWidth, panelHeight);
		// Draw Content
		g.setColor(Color.yellow);
		drawInfoCentered("" + unit.getName(), g, yOffset);
		g.setColor(Color.white);
		drawInfo("State: " + unit.getState(), g, yOffset);
		drawInfo("Location: (" + unit.getLocation().x + ", " + unit.getLocation().y + ")", g, yOffset);
		BigDecimal bd = new BigDecimal(unit.getSustenance()).setScale(1, RoundingMode.UP);
		BigDecimal bd2 = new BigDecimal(unit.getFatigue()).setScale(1, RoundingMode.UP);
		BigDecimal bd3 = new BigDecimal(unit.getWarmth()).setScale(1, RoundingMode.UP);
		double sustValue = bd.doubleValue();
		double fatValue = bd2.doubleValue();
		double thirstValue = bd3.doubleValue();
		if (sustValue < 50)
			g.setColor(Color.yellow);
		if (sustValue < 25)
			g.setColor(Color.red);

		drawInfo("Sustenance: " + sustValue + "%", g, yOffset);
		drawInfo("Fatigue: " + fatValue + "%", g, yOffset);
		drawInfo("Warmth: " + thirstValue + "%", g, yOffset);

		if (unit.getInventory().size() > 0)
			drawInventory(g, tiles, yOffset);

		// Resize panel to size of content
		this.panelHeight = infoLine * 16 + 32;
	}

	private void drawInfo(String s, Graphics g, int yOffset) {
		g.drawString(s, panelX + 8, yOffset + (infoLine * 16) + 32);
		if (s.length() * 10 + 8 > this.panelWidth) {
			this.panelWidth = s.length() * 10;
			this.panelX = gameSettings.RESOLUTION_X - panelWidth - 16;
		}
		infoLine++;
	}

	private void drawInventory(Graphics g, SpriteSheet tiles, int yOffset) {
		int itemsPlacedThisRow = 0;
		int maxItemsPerRow = (panelWidth - 64) / 16;
		infoLine += 2;
		for (Item item : unit.getInventory()) {
			int xDrawLoc = panelX + 8 + (itemsPlacedThisRow * 16);
			if (itemsPlacedThisRow > maxItemsPerRow) {
				xDrawLoc = panelX + 8;
				itemsPlacedThisRow = 0;
				infoLine += 2;
			}
			tiles.getSubImage(item.gfx.x, item.gfx.y).draw(xDrawLoc, yOffset + (infoLine * 16));
			itemsPlacedThisRow++;
		}
	}

	private void drawInfoCentered(String s, Graphics g, int yOffset) {
		g.drawString(s, panelX + (panelWidth - 64) / 2, yOffset + (infoLine * 16) + 32);
		infoLine++;
	}

	public void getUnitOfPanelClicked(int mouseX, int mouseY, Point camera) {
		Rectangle mouse = new Rectangle(mouseX, mouseY, 1, 1);
		Rectangle panel = new Rectangle(panelX, yOffset + 16, panelWidth, panelHeight);
		if (mouse.intersects(panel)) {
			camera.x = unit.getLocation().x * 32 - Launcher.getSettings().RESOLUTION_X / 2;
			camera.y = unit.getLocation().y * 32 - Launcher.getSettings().RESOLUTION_Y / 2;
		}
	}

	public int setYOffset(int yOffset) {
		this.yOffset = yOffset;
		return yOffset + this.panelHeight + 16;
	}

	public int getYOffset() {
		return yOffset;
	}
}
