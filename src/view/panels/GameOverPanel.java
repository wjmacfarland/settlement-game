package view.panels;

import java.util.LinkedList;
import java.util.List;

import launcher.Launcher;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

import view.Button;

public class GameOverPanel {

	private List<Button> buttons;

	int x, y, w, h;

	boolean gameWon;

	public GameOverPanel(boolean gameWon) {
		this.gameWon = gameWon;
		// Create buttons
		buttons = new LinkedList<Button>();

		int totalWidth = 16;
		buttons.add(new Button(x + totalWidth, y + 32, "New Game"));
		totalWidth += buttons.get(0).w + 16;
		buttons.add(new Button(x + totalWidth, y + 32, "Load Game"));
		totalWidth += buttons.get(0).w + 16;
		buttons.add(new Button(x + totalWidth, y + 32, "Quit Game"));
		totalWidth += buttons.get(0).w + 16;

		// Determine panel coords/size
		w = totalWidth;
		h = 384;
		x = Launcher.getSettings().RESOLUTION_X / 2 - (w / 2);
		y = Launcher.getSettings().RESOLUTION_Y / 3;

		for (Button button : buttons) {
			button.x += x;
			button.y += y;
		}
	}

	private void drawInfo(String s, Graphics g, int y) {
		g.drawString(s, x + 8, y);
	}

	public void draw(Graphics g) {
		g.setColor(Color.white);
		g.drawString("Escape Menu", x + 32, y + 8);
		g.setColor(new Color(0, 0, 0, 0.5f));
		g.fillRect(x, y, w, h);
		g.setColor(Color.white);
		g.drawRect(x, y, w, h);
		for (Button button : buttons) {
			button.draw(g);
		}
		if (gameWon) {
			drawInfo("After many sleepness and stressful", g, y + 128);
			drawInfo("nights on the planet, the humans", g, y + 128 + 16);
			drawInfo("managed to survive and craft a rocket", g, y + 128 + 32);
			drawInfo("ship. Where will the survivors land", g, y + 128 + 48);
			drawInfo("next? Well, that is the next great", g, y + 128 + 64);
			drawInfo("adventure.", g, y + 128 + 64 + 16);
		} else {
			drawInfo("Alas, the conditions on the planet were", g, y + 128);
			drawInfo("far too perilous for the survivors.", g, y + 128 + 16);
		}
	}

	public String sendClick(int absMouseX, int absMouseY) {
		for (Button button : buttons) {
			Rectangle mouse = new Rectangle(absMouseX, absMouseY, 1, 1);
			Rectangle buttonRect = new Rectangle(button.x, button.y, button.w, button.h);
			if (mouse.intersects(buttonRect)) {
				return button.label;
			}
		}
		return "";
	}
}