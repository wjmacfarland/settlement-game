package view.panels;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import launcher.Launcher;
import model.FoodCollection;
import model.MetalCollection;
import model.WoodCollection;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class ResourcePanel {

	// Private variables
	private int x, y, w, h;

	private ArrayList<String> dynamicInformation;

	private Queue<String> log;

	private final SpriteSheet tiles;

	public ResourcePanel(SpriteSheet tileset) {
		w = Launcher.getSettings().RESOLUTION_X / 3;
		h = 128;
		x = Launcher.getSettings().RESOLUTION_X / 2 - (w / 2);
		y = Launcher.getSettings().RESOLUTION_Y - h - 16;
		tiles = tileset;
		dynamicInformation = new ArrayList<String>();
		log = new LinkedList<String>();
	}

	public void draw(Graphics g) throws SlickException {
		g.setColor(Color.white);
		g.setColor(new Color(0, 0, 0, 0.5f));
		g.fillRect(x, y, w, h);
		g.setColor(Color.white);
		String resStorage = "Resource Storage";
		g.drawString("Resource Storage", x + w / 2 - resStorage.length() * 4.5f, y + 8);
		g.drawRect(x, y, w, h);

		if (Launcher.getSettings().RESOLUTION_X >= 1280) {

			// Draw wood quantity
			tiles.getSprite(0, 123).draw(x + 16, y + 32, 2.0f);
			g.drawString("x " + WoodCollection.getInstance().size(), x + 64 + 16, y + 64);

			// Draw food quantity
			tiles.getSprite(3, 63).draw(x + 16 + 128, y + 32, 2.0f);
			g.drawString("x " + FoodCollection.getInstance().size(), x + 16 + 128 + 64, y + 64);

			tiles.getSprite(3, 5).draw(x + 16 + 256, y + 32, 2.0f);
			g.drawString("x " + MetalCollection.getInstance().size(), x + 16 + 256 + 64, y + 64);

		} else {
			// Draw wood quantity
			tiles.getSprite(0, 123).draw(x + 16, y + 32);
			g.drawString("x " + WoodCollection.getInstance().size(), x + 48, y + 48);

			// Draw food quantity
			tiles.getSprite(3, 63).draw(x + 16 + 96, y + 32);
			g.drawString("x " + FoodCollection.getInstance().size(), x + 48 + 96, y + 48);

			tiles.getSprite(3, 5).draw(x + 16 + 192, y + 32);
			g.drawString("x " + MetalCollection.getInstance().size(), x + 48 + 192, y + 48);

		}
	}
}
