package view.panels;

import java.util.LinkedList;
import java.util.List;

import launcher.Launcher;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

import view.Button;

public class EscapePanel {

	private List<Button> buttons;

	int x, y, w, h;

	public EscapePanel() {
		// Create buttons
		buttons = new LinkedList<Button>();

		int totalWidth = 16;
		buttons.add(new Button(x + totalWidth, y + 32, "Save Game"));
		totalWidth += buttons.get(0).w + 16;
		buttons.add(new Button(x + totalWidth, y + 32, "Load Game"));
		totalWidth += buttons.get(0).w + 16;
		buttons.add(new Button(x + totalWidth, y + 32, "Quit Game"));
		totalWidth += buttons.get(0).w + 16;

		// Determine panel coords/size
		w = totalWidth;
		h = 96;
		x = Launcher.getSettings().RESOLUTION_X / 2 - (w / 2);
		y = Launcher.getSettings().RESOLUTION_Y / 3;

		for (Button button : buttons) {
			button.x += x;
			button.y += y;
		}
	}

	public void draw(Graphics g) {
		g.setColor(Color.white);
		g.drawString("Escape Menu", x + 32, y + 8);
		g.setColor(new Color(0, 0, 0, 0.5f));
		g.fillRect(x, y, w, h);
		g.setColor(Color.white);
		g.drawRect(x, y, w, h);
		for (Button button : buttons) {
			button.draw(g);
		}
	}

	public String sendClick(int absMouseX, int absMouseY) {
		for (Button button : buttons) {
			Rectangle mouse = new Rectangle(absMouseX, absMouseY, 1, 1);
			Rectangle buttonRect = new Rectangle(button.x, button.y, button.w, button.h);
			if (mouse.intersects(buttonRect)) {
				return button.label;
			}
		}
		return "";
	}
}