package helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;

import view.GameView;

public class NameGenerator {

	private static NameGenerator instance = null;
	private ArrayList<String> names;
	private LinkedList<Integer> usedNames;

	public NameGenerator() {
		names = new ArrayList<String>();
		usedNames = new LinkedList<Integer>();

		Path path = Paths.get("res/data/names.txt");
		try (BufferedReader reader = Files.newBufferedReader(path,
				StandardCharsets.UTF_8)) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				names.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static NameGenerator getInstance() {
		if (instance == null) {
			instance = new NameGenerator();
		}
		return instance;
	}

	public String getRandomName() {
		int nameIndex = GameView.RANDOM.nextInt(27607);
		if (!usedNames.contains(nameIndex)) {
			usedNames.add(nameIndex);
			String name = names.get(nameIndex);
			String cappedName = name.substring(0, 1).toUpperCase()
					+ name.substring(1, name.length());
			return cappedName;
		} else
			if(usedNames.size() == names.size()){
				return new String("We ran out of names somehow");
			}
			else{
				while(usedNames.contains(nameIndex)){
					nameIndex = GameView.RANDOM.nextInt(27607);
				}
				usedNames.add(nameIndex);
				String name = names.get(nameIndex);
				String cappedName = name.substring(0, 1).toUpperCase()
						+ name.substring(1, name.length());
				return cappedName;
			}
		
	}

}
