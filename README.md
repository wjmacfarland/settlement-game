# README #

This project requires Slick2D: http://slick.ninjacave.com/

After importing the project into Eclipse, you may need to set the library path via: -Djava.library.path="your-slick-path-here"

Run Launcher.java in order to start the game. The launcher allows you to specify resolution and basic sound options.